import os
import time
import re
import pkg_resources
import shutil
import subprocess
from zipfile import ZipFile

base_path = pkg_resources.resource_filename('netoprmgr', '')
capture_path = pkg_resources.resource_filename('netoprmgr', 'static/')
capture_path = os.path.join(capture_path,'capture/')
data_path = pkg_resources.resource_filename('netoprmgr', 'static/')
data_path = os.path.join(data_path,'data/')
result_path = pkg_resources.resource_filename('netoprmgr', 'static/')
result_path = os.path.join(result_path,'result/')

from netoprmgr.script.create_template_chart import create_data_template

class MainCli:

    def showLog():
        chg_dir = os.chdir(capture_path)
        current_dir=os.getcwd()
        from netoprmgr.script.get_log import get_log
        #from netoprmgr.script.log_docx import log_docx

        num=input('Log for 1 or 3 month?\n')
        month_list = []
        if num == '1':
            month=input('Please input month\n'+'1 = Jan\n'+
                                                    '2 = Feb\n'+
                                                    '3 = Mar\n'+
                                                    '4 = Apr\n'+
                                                    '5 = May\n'+
                                                    '6 = Jun\n'+
                                                    '7 = Jul\n'+
                                                    '8 = Aug\n'+
                                                    '9 = Sep\n'+
                                                    '10 = Oct\n'+
                                                    '11 = Nov\n'+
                                                    '12 = Dec\n')
            if month == '1':
                month = 'Jan'
            elif month == '2':
                month = 'Feb'
            elif month == '3':
                month = 'Mar'
            elif month == '4':
                month = 'Apr'
            elif month == '5':
                month = 'May'
            elif month == '6':
                month = 'Jun'
            elif month == '7':
                month = 'Jul'
            elif month == '8':
                month = 'Aug'
            elif month == '9':
                month = 'Sep'
            elif month == '10':
                month = 'Oct'
            elif month == '11':
                month = 'Nov'
            elif month == '12':
                month = 'Dec'
            month_list.append(month)
        elif num == '3':
            month=input('Please input first month\n'+'1 = Jan\n'+
                                                    '2 = Feb\n'+
                                                    '3 = Mar\n'+
                                                    '4 = Apr\n'+
                                                    '5 = May\n'+
                                                    '6 = Jun\n'+
                                                    '7 = Jul\n'+
                                                    '8 = Aug\n'+
                                                    '9 = Sep\n'+
                                                    '10 = Oct\n'+
                                                    '11 = Nov\n'+
                                                    '12 = Dec\n')
            if month == '1':
                month = 'Jan'
            elif month == '2':
                month = 'Feb'
            elif month == '3':
                month = 'Mar'
            elif month == '4':
                month = 'Apr'
            elif month == '5':
                month = 'May'
            elif month == '6':
                month = 'Jun'
            elif month == '7':
                month = 'Jul'
            elif month == '8':
                month = 'Aug'
            elif month == '9':
                month = 'Sep'
            elif month == '10':
                month = 'Oct'
            elif month == '11':
                month = 'Nov'
            elif month == '12':
                month = 'Dec'
            month_list.append(month)

            month=input('\nPlease input second month\n'+'1 = Jan\n'+
                                                    '2 = Feb\n'+
                                                    '3 = Mar\n'+
                                                    '4 = Apr\n'+
                                                    '5 = May\n'+
                                                    '6 = Jun\n'+
                                                    '7 = Jul\n'+
                                                    '8 = Aug\n'+
                                                    '9 = Sep\n'+
                                                    '10 = Oct\n'+
                                                    '11 = Nov\n'+
                                                    '12 = Dec\n')
            if month == '1':
                month = 'Jan'
            elif month == '2':
                month = 'Feb'
            elif month == '3':
                month = 'Mar'
            elif month == '4':
                month = 'Apr'
            elif month == '5':
                month = 'May'
            elif month == '6':
                month = 'Jun'
            elif month == '7':
                month = 'Jul'
            elif month == '8':
                month = 'Aug'
            elif month == '9':
                month = 'Sep'
            elif month == '10':
                month = 'Oct'
            elif month == '11':
                month = 'Nov'
            elif month == '12':
                month = 'Dec'
            month_list.append(month)

            month=input('\nPlease input third month\n'+'1 = Jan\n'+
                                                    '2 = Feb\n'+
                                                    '3 = Mar\n'+
                                                    '4 = Apr\n'+
                                                    '5 = May\n'+
                                                    '6 = Jun\n'+
                                                    '7 = Jul\n'+
                                                    '8 = Aug\n'+
                                                    '9 = Sep\n'+
                                                    '10 = Oct\n'+
                                                    '11 = Nov\n'+
                                                    '12 = Dec\n')
            if month == '1':
                month = 'Jan'
            elif month == '2':
                month = 'Feb'
            elif month == '3':
                month = 'Mar'
            elif month == '4':
                month = 'Apr'
            elif month == '5':
                month = 'May'
            elif month == '6':
                month = 'Jun'
            elif month == '7':
                month = 'Jul'
            elif month == '8':
                month = 'Aug'
            elif month == '9':
                month = 'Sep'
            elif month == '10':
                month = 'Oct'
            elif month == '11':
                month = 'Nov'
            elif month == '12':
                month = 'Dec'
            month_list.append(month)
        else:
            print('Wrong Input!!! Back To Main')
            menu()

        files = os.listdir(current_dir)
        get_log=get_log(files,month_list)
        get_log.get_log()
        
        time.sleep(3)
        for file in files:
            if file.endswith("db"):
                os.remove(file)
        src_mv = (capture_path+'show_log.docx')
        dst_mv = (result_path+'show_log.docx')
        shutil.move(src_mv,dst_mv)

    def showLogWeb(month_list,year):
        chg_dir = os.chdir(capture_path)
        current_dir=os.getcwd()
        from netoprmgr.script.get_log import get_log

        month_list = month_list
        year = year

        files = os.listdir(current_dir)
        get_log=get_log(files,month_list,year)
        get_log.get_log()
        
        time.sleep(3)
        for file in files:
            if file.endswith("db"):
                os.remove(file)
        #src_mv = (capture_path+'show_log.docx')
        #dst_mv = (result_path+'show_log.docx')
        #shutil.move(src_mv,dst_mv)
        src_mv = (capture_path+'logresult_rs.csv')
        dst_mv = (result_path+'logresult_rs.csv')
        shutil.move(src_mv,dst_mv)

    def createNewReport():
        chg_dir = os.chdir(capture_path)
        current_dir=os.getcwd()
        from netoprmgr.script.file_identification import file_identification
        from netoprmgr.script.convert_docx import convert_docx
        files = os.listdir(current_dir)
        file_identification=file_identification(files)
        file_identification.file_identification()
        convert_docx=convert_docx()
        convert_docx.convert_docx()
        time.sleep(3)
        src_mv = (capture_path+'preventive_maintenance.docx')
        dst_mv = (result_path+'preventive_maintenance.docx')
        shutil.move(src_mv,dst_mv)
        os.remove("pmdb")

    def deleteCapture():
        chg_dir = os.chdir(capture_path)
        current_dir=os.getcwd()
        files = os.listdir(current_dir)
        for file in files:
            if '__init__.py' in file:
                pass
            else:
                try:
                    os.remove(file)
                    print('Deleting '+str(file))
                except:
                    pass
    
    def deleteResult():
        chg_dir = os.chdir(result_path)
        current_dir=os.getcwd()
        files = os.listdir(current_dir)
        for file in files:
            if '__init__.py' in file:
                pass
            else:
                try:
                    os.remove(file)
                    print('Deleting '+str(file))
                except:
                    pass

    def createTemplate():
        chg_dir = os.chdir(data_path)
        function_create_version_template=create_data_template()
        
if __name__ == "__main__":
    answer=input(
    'Press Number for MENU :\n'
    '1. Create Report\n'
    '2. Show Log Report\n'
    'Type "quit" to quit program\n'
    )

    if answer == '1':
        MainCli.createNewReport()
    elif answer == '2':
        MainCli.showLog()