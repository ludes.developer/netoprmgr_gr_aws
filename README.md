# Local Installation

Python 3.7+ is recommended

You can also create virtual environment to use this module.
1.	Clone source code from netoprmgr repository
	```
	https://github.com/ludesdeveloper/netoprmgr.git
	cd netoprmgr
	```
	
2.	Install requirements
	```
	pip install -r requirements.txt
	```
# eosl_ip.py

After clone source code and install requirements, please add new file eosl_ip that used to connect to eosl API

1.	Add new file eosl_ip.py

2.	Paste this command
	```
	def eosl_ip():
    ip_address = '127.0.0.1:5003'
    return ip_address
	```

# Usage

1.	Run python -m netoprmgr__main__

	![run_main_not_folder](https://ibb.co/9v5wTYb)

2.	Or you can run python from your directory that contains netoprmgr packages

	![run_main_folder](https://ibb.co/7GJHsqB)

3.	Open web browser, type localhost:5002

	![open_web]https://ibb.co/dKtZ9X7)

# netoprmgr_eosl

In order to get End of Support/Life information that can be generated to report.
Please consider to clone source code from netoprmgr_eosl repository

# Docker

1.	netoprmgr can be pulled from docker
	```
	docker pull ludesdeveloper/netoprmgr
	```
2.	Then run docker
	```
	docker run -d -p 5000:5000 ludesdeveloper/netoprmgr
	```