#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import requests


def execute_update_ip(directory):
    on_prem_check = False
    write = open(directory+'/ip_variables.py', 'w')
    try:
        requests.get('http://localhost:5012', timeout=1)
        on_prem_check = True
    except:
        on_prem_check = False
    if on_prem_check == True:
        write.write("docx_create_ip = 'http://localhost:5011/'\n")
        write.write("dbcustomer_ip = 'localhost'\n")
        write.write("dbgrtemplate_ip = 'localhost'\n")
        write.write("eosl_var_ip = 'localhost:5012'")
    else:
        write.write("docx_create_ip = 'http://golang-gr:5011/'\n")
        write.write("dbcustomer_ip = 'portal-db'\n")
        write.write("dbgrtemplate_ip = 'portal-db'\n")
        write.write("eosl_var_ip = 'gr-go-soft-hard-eosl:5012'")
    write.close()
