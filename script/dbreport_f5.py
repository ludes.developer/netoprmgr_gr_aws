import sqlite3

class dbreport_f5:
    def __init__(self):
        #DESTROY pmdb_f5
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE tipetable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE swtable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE swsumtable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE hwsumtable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE hwcardtable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE eosltable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE envtable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE cputable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE memtable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE throughputbitstable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE throughputssltable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE throughputpackettable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE connectiontable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE newconnectiontable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE tipetable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE httprequeststable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE ltmtable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE hatable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE synchatable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE traffichatable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE certtable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE lictable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE logtable''')
            db.commit()
            db.close()
        except:
            pass
        
        #OPEN DB PMBD_F5
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE tipetable(id INTEGER PRIMARY KEY, tipe TEXT)
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE swtable(id INTEGER PRIMARY KEY, devicename TEXT,
                model TEXT, iosversion TEXT, uptime TEXT, confreg TEXT, version TEXT, date_eos TEXT, date_last TEXT)                     
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE swsumtable(id INTEGER PRIMARY KEY, version TEXT, date_eos TEXT, date_last TEXT)                    
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE hwsumtable(id INTEGER PRIMARY KEY, model TEXT)                    
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE hwcardtable(id INTEGER PRIMARY KEY, devicename TEXT,
                model TEXT, tipe TEXT, sn TEXT, eosl_date TEXT)                    
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE eosltable(id INTEGER PRIMARY KEY, model TEXT, tipe TEXT, date TEXT)
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE envtable(id INTEGER PRIMARY KEY, devicename TEXT,
                                system TEXT, item TEXT, status TEXT)
            ''')
            db.close()
        except:
            pass       
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE cputable(id INTEGER PRIMARY KEY, devicename TEXT,
                model TEXT, cpu_current TEXT, cpu_3h TEXT, cpu_24h TEXT, cpu_7d TEXT, cpu_30d TEXT, cpu_status TEXT)                    
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE memtable(id INTEGER PRIMARY KEY, devicename TEXT,
                memory_name TEXT, memory_current TEXT, memory_3h TEXT, memory_24h TEXT, memory_7d TEXT, memory_30d TEXT, memory_status TEXT)                    
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE throughputbitstable(id INTEGER PRIMARY KEY, devicename TEXT,
                throughput_bits_name TEXT, throughput_bits_current TEXT, throughput_bits_3h TEXT, throughput_bits_24h TEXT, throughput_bits_7d TEXT, throughput_bits_30d TEXT, throughput_bits_status TEXT)                
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE throughputssltable(id INTEGER PRIMARY KEY, devicename TEXT,
                throughput_ssl_name TEXT, throughput_ssl_current TEXT, throughput_ssl_3h TEXT, throughput_ssl_24h TEXT, throughput_ssl_7d TEXT, throughput_ssl_30d TEXT, throughput_ssl_status TEXT)                
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE throughputpackettable(id INTEGER PRIMARY KEY, devicename TEXT,
                throughput_packet_name TEXT, throughput_packet_current TEXT, throughput_packet_3h TEXT, throughput_packet_24h TEXT, throughput_packet_7d TEXT, throughput_packet_30d TEXT, throughput_packet_status TEXT)                
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE connectiontable(id INTEGER PRIMARY KEY, devicename TEXT,
                connection_name TEXT, connection_current TEXT, connection_3h TEXT, connection_24h TEXT, connection_7d TEXT, connection_30d TEXT, connection_status TEXT)                
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE newconnectiontable(id INTEGER PRIMARY KEY, devicename TEXT,
                newconnection_name TEXT, newconnection_current TEXT, newconnection_3h TEXT, newconnection_24h TEXT, newconnection_7d TEXT, newconnection_30d TEXT, newconnection_status TEXT)                
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE httprequeststable(id INTEGER PRIMARY KEY, devicename TEXT,
                httprequests_name TEXT, httprequests_current TEXT, httprequests_3h TEXT, httprequests_24h TEXT, httprequests_7d TEXT, httprequests_30d TEXT, httprequests_status TEXT)                
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE ltmtable(id INTEGER PRIMARY KEY, devicename TEXT,
                ltm_tipe TEXT, ltm_name TEXT, availability TEXT, state TEXT)                
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE hatable(id INTEGER PRIMARY KEY, devicename TEXT, device TEXT,
                mgmt_ip TEXT, configsync_ip TEXT, ha_state TEXT, ha_status TEXT)                
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE synchatable(id INTEGER PRIMARY KEY, devicename TEXT,
                sync_status TEXT, action TEXT)                
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE traffichatable(id INTEGER PRIMARY KEY, devicename TEXT,
                traffic_group TEXT, device_traffic TEXT, traffic_status TEXT, next_active TEXT, previous_active TEXT, active_reason TEXT)                
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE certtable(id INTEGER PRIMARY KEY, devicename TEXT,
                cert_name TEXT, cn TEXT, expired_date TEXT, cert_status TEXT)                
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE lictable(id INTEGER PRIMARY KEY, devicename TEXT,
                lic_version TEXT, register_key TEXT, active_module TEXT, lic_on TEXT, svc_date TEXT, lic_status TEXT)                
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_f5')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE logtable(id INTEGER PRIMARY KEY, devicename TEXT, model TEXT, script TEXT)
            ''')
            db.close()
        except:
            pass

