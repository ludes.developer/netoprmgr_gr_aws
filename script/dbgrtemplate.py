import os
import sqlite3
import pkg_resources
import pymysql
from netoprmgr.script.ip_variables import dbgrtemplate_ip

capture_path = pkg_resources.resource_filename('netoprmgr', 'static/')
capture_path = os.path.join(capture_path, 'capture/')


class dbgrtemplate:
    def __init__(self, customer):
        self.customer = customer

    def dbgrtemplate(self):
        chg_dir = os.chdir(capture_path)

        # SQL QUERY RS
        db = sqlite3.connect('pmdb_rs')
        cursor = db.cursor()
        cursor.execute('''SELECT model, script FROM logtable''')
        records = cursor.fetchall()
        # connect to remote db for dbgr_template
        dbgrtemplate = pymysql.connect(
            db='db_portal', user='root', passwd='N3t0prmgr!', host=dbgrtemplate_ip, port=3306)
        cursor_grtemplate = dbgrtemplate.cursor()
        for record in records:
            cursor_check = dbgrtemplate.cursor()
            cursor_check.execute(
                """SELECT * from grtemplate_grtemplate where device_pid=%s""", (record[0]))
            check_records = cursor_check.fetchall()
            if len(check_records) == 0:
                sql = "INSERT INTO grtemplate_grtemplate (script_name, device_pid) VALUES (%s, %s)"
                val = (record[1], record[0])
                cursor_grtemplate.execute(sql, val)
            else:
                print('Data Duplicate, Not Writing to Customer DB')
        dbgrtemplate.commit()
        dbgrtemplate.close()

        # CLOSE DB RS
        db.close()

        # SQL QUERY UCM
        dbucm = sqlite3.connect('pmdb_ucm')
        cursor = dbucm.cursor()
        cursor.execute('''SELECT model, script FROM logtable''')
        records = cursor.fetchall()
        # connect to remote db for dbgr_template
        dbgrtemplate = pymysql.connect(
            db='db_portal', user='root', passwd='N3t0prmgr!', host=dbgrtemplate_ip, port=3306)
        cursor_grtemplate = dbgrtemplate.cursor()
        for record in records:
            cursor_check = dbgrtemplate.cursor()
            cursor_check.execute(
                """SELECT * from grtemplate_grtemplate where device_pid=%s""", (record[0]))
            check_records = cursor_check.fetchall()
            if len(check_records) == 0:
                sql = "INSERT INTO grtemplate_grtemplate (script_name, device_pid) VALUES (%s, %s)"
                val = (record[1], record[0])
                cursor_grtemplate.execute(sql, val)
            else:
                print('Data Duplicate, Not Writing to Customer DB')
        dbgrtemplate.commit()
        dbgrtemplate.close()

        # CLOSE DB UCM
        dbucm.close()

        # SQL QUERY F5
        dbf5 = sqlite3.connect('pmdb_f5')
        cursor = dbf5.cursor()
        cursor.execute('''SELECT model, script FROM logtable''')
        records = cursor.fetchall()
        # connect to remote db for dbgr_template
        dbgrtemplate = pymysql.connect(
            db='db_portal', user='root', passwd='N3t0prmgr!', host=dbgrtemplate_ip, port=3306)
        cursor_grtemplate = dbgrtemplate.cursor()
        for record in records:
            cursor_check = dbgrtemplate.cursor()
            cursor_check.execute(
                """SELECT * from grtemplate_grtemplate where device_pid=%s""", (record[0]))
            check_records = cursor_check.fetchall()
            if len(check_records) == 0:
                sql = "INSERT INTO grtemplate_grtemplate (script_name, device_pid) VALUES (%s, %s)"
                val = (record[1], record[0])
                cursor_grtemplate.execute(sql, val)
            else:
                print('Data Duplicate, Not Writing to Customer DB')
        dbgrtemplate.commit()
        dbgrtemplate.close()

        # CLOSE DB F5
        dbf5.close()

        # SQL QUERY FORTIGATE
        dbfortigate = sqlite3.connect('pmdb_fortigate')
        cursor = dbfortigate.cursor()
        cursor.execute('''SELECT model, script FROM logtable''')
        records = cursor.fetchall()
        # connect to remote db for dbgr_template
        dbgrtemplate = pymysql.connect(
            db='db_portal', user='root', passwd='N3t0prmgr!', host=dbgrtemplate_ip, port=3306)
        cursor_grtemplate = dbgrtemplate.cursor()
        for record in records:
            cursor_check = dbgrtemplate.cursor()
            cursor_check.execute(
                """SELECT * from grtemplate_grtemplate where device_pid=%s""", (record[0]))
            check_records = cursor_check.fetchall()
            if len(check_records) == 0:
                sql = "INSERT INTO grtemplate_grtemplate (script_name, device_pid) VALUES (%s, %s)"
                val = (record[1], record[0])
                cursor_grtemplate.execute(sql, val)
            else:
                print('Data Duplicate, Not Writing to Customer DB')
        dbgrtemplate.commit()
        dbgrtemplate.close()

        # CLOSE DB FORTIGATE
        dbfortigate.close()
