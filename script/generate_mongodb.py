from datetime import datetime
import pymongo
import xlsxwriter

current_time = datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ')
myclient = pymongo.MongoClient("mongodb://172.16.104.201:27017")
mydb = myclient["netoprmgr"]
mycol = mydb["users_login"]
# mydict = { "date": current_time, "user": "seski.ramadhan@compnet.co.id" }
# x = mycol.insert_one(mydict)

#create excel
workbook = xlsxwriter.Workbook('login_netoprmgr.xlsx')
worksheet = workbook.add_worksheet()
worksheet.write(0, 0, "date")
worksheet.write(0, 1, "user")
for enum, x in enumerate(mycol.find(),1):
  print(x)
  worksheet.write(enum, 0, x["date"])
  worksheet.write(enum, 1, x["user"])

workbook.close()