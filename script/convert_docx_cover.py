from docx import Document
from docx.oxml.shared import OxmlElement, qn
import sqlite3
import re
from docx.shared import Pt
import pkg_resources
import os
import subprocess
from sys import platform
from netoprmgr.script.convert_docx_create import convert_docx_create
from docx.enum.text import WD_LINE_SPACING
from docx.enum.text import WD_ALIGN_PARAGRAPH
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
from docx.enum.table import WD_ROW_HEIGHT
from docx.enum.table import WD_ALIGN_VERTICAL
from docx.shared import Inches
from docx.shared import Pt
from docx.shared import Cm
from docx.shared import RGBColor
from datetime import datetime

from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw

data_path = pkg_resources.resource_filename('netoprmgr', 'static/')
data_path = os.path.join(data_path, 'data/')
capture_path = pkg_resources.resource_filename('netoprmgr', 'static/')
capture_path = os.path.join(capture_path,'capture/')
main_path = pkg_resources.resource_filename('netoprmgr', '/')

docx_create = convert_docx_create()
def func_get_req(address):
    try:
        os.chdir(main_path)
        files = ['init', 'init.exe']
        for file in files:
            os.chmod(file, 0o0777)
        address = re.findall(".*(qr.*)", address)
        address = address[0]
        if platform == "win32":
            cmd = ("init ")
            qr = subprocess.check_output(cmd+address, shell=True)
        else:
            cmd = ("./init ")
            qr = subprocess.check_output(cmd+address, shell=True)

        qr = str(qr)
        qr = re.findall("b\W(.*)\Wn\W", qr)
        qr = qr[0]
        return qr
    except:
        return

def shade_cells(cells, shade):
    for cell in cells:
        tcPr = cell._tc.get_or_add_tcPr()
        tcVAlign = OxmlElement("w:shd")
        tcVAlign.set(qn("w:fill"), shade)
        tcPr.append(tcVAlign)

def set_repeat_table_header(row):
    tr = row._tr
    trPr = tr.get_or_add_trPr()
    qr45 = func_get_req(docx_create+"qr45")
    tblHeader = OxmlElement(qr45)
    tblHeader.set(qn('w:val'), "true")
    trPr.append(tblHeader)
    return row

class convert_docx_cover:
    # @staticmethod
    def __init__(self, customer, review, prepared, pic, bulan, tahun, report_type):
        # variable constructor
        self.customer = customer
        self.review = review
        self.prepared = prepared
        self.pic = pic
        self.bulan = bulan
        self.tahun = tahun
        self.report_type = report_type

    def convert_docx_cover(self):
        print('')
        print('Processing Document')
        os.chdir(capture_path)

        # using document docx module
        report_template = open(data_path+'template.docx', 'rb')
        document = Document(report_template)
        
        all_file_dir = os.listdir()
        all_tipe = []
        all_count_tipe = []
        all_model = []
        all_version = []
        for file in all_file_dir:
            if 'pmdb' in file:
                db = sqlite3.connect(file)
                cursor = db.cursor()

                # SQL QUERY TIPE TABLE
                cursor.execute(
                    '''SELECT tipe, COUNT(*) FROM tipetable GROUP BY tipe''')
                records = cursor.fetchall()

                for record in records:
                    all_tipe.append(record[0])
                    all_count_tipe.append(record[1])

                # SQL QUERY TIPE TABLE
                cursor.execute(
                    '''SELECT model, version FROM swtable WHERE confreg='-' OR confreg LIKE '0x%' ORDER BY model, version ASC ''')
                records = cursor.fetchall()

                for record in records:
                    all_model.append(record[0])
                    all_version.append(record[1])

                # CLOSE DB
                db.close()

        compnet = 'Nusantara Compnet Integrator, PT'
        address = 'Bekasi, Bogor dan Depok - Indonesia'

        document.add_paragraph()
        # Cover Periode
        p = document.add_paragraph(self.bulan+' '+self.tahun)
        p.style = document.styles['Heading 4']

        document.add_paragraph()
        document.add_paragraph()
        document.add_paragraph()

        # Cover PT Customer
        caps = self.customer.upper()
        p = document.add_paragraph(caps)
        p.style = document.styles['Heading 4']
        p.alignment = WD_ALIGN_PARAGRAPH.RIGHT

        document.add_paragraph()
        document.add_paragraph()
        document.add_paragraph()

        p = document.add_paragraph(address)
        p.style = document.styles['Quote']
        p.alignment = WD_ALIGN_PARAGRAPH.RIGHT

        document.add_paragraph()
        document.add_paragraph()
        document.add_paragraph()
        document.add_paragraph()
        document.add_paragraph()
        document.add_paragraph()
        document.add_paragraph()

        # Cover PT Compnet
        p = document.add_paragraph('PT. Nusantara Compnet Integrator')
        p.style = document.styles['Quote']
        p.alignment = WD_ALIGN_PARAGRAPH.LEFT

        p = document.add_paragraph('Corporate Headquarters')
        p.style = document.styles['Quote']
        p.alignment = WD_ALIGN_PARAGRAPH.LEFT

        p = document.add_paragraph('Gallery West Office Tower, ')
        p.style = document.styles['Quote']
        p.alignment = WD_ALIGN_PARAGRAPH.LEFT

        p = document.add_paragraph('Lantai 8, Jl. Panjang No. 5, Kebun Jeruk')
        p.style = document.styles['Quote']
        p.alignment = WD_ALIGN_PARAGRAPH.LEFT

        p = document.add_paragraph('Jakarta Barat 11530 Indonesia')
        p.style = document.styles['Quote']
        p.alignment = WD_ALIGN_PARAGRAPH.LEFT

        p = document.add_paragraph('http://www.compnet.co.id')
        p.style = document.styles['Quote']
        p.alignment = WD_ALIGN_PARAGRAPH.LEFT

        p = document.add_paragraph('Tel	: 62 021 25676600')
        p.style = document.styles['Quote']
        p.alignment = WD_ALIGN_PARAGRAPH.LEFT

        p = document.add_paragraph('Fax	: 62 021 53650829')
        p.style = document.styles['Quote']
        p.alignment = WD_ALIGN_PARAGRAPH.LEFT

        # Cover Footer
        green = RGBColor(13, 156, 153)
        waktu = datetime.now()
        str_waktu = str(waktu)

        if re.findall('(^\d+-\d+-\d+\s+\d+:\d+:\d+).\d+', str_waktu):
            hasil = re.findall('(^\d+-\d+-\d+\s+\d+:\d+:\d+).\d+', str_waktu)
            hasil = hasil[0]

        # Create Image Watermark
        chg_dir = os.chdir(data_path)
        current_dir = os.getcwd()

        img = Image.open(data_path+'/generated.png')
        draw = ImageDraw.Draw(img)
        font = ImageFont.truetype(data_path+'/Roboto-Regular.ttf', 60)
        draw.text((110, 440), hasil, (0, 0, 0), font=font)

        chg_dir = os.chdir(capture_path)
        current_dir = os.getcwd()

        img.save(capture_path+'/watermark.png')

        imgwater = (capture_path+'/watermark.png')

        footer = document.sections[0].footer
        table_footer = footer.add_table(1, 2, Inches(6.6))
        qr47 = func_get_req(docx_create+"qr47")
        table_footer.style = qr47
        table_footer.allow_autofit = True
        hdr_cells = table_footer.rows[0].cells

        hdr_cells[0].text = self.bulan+' ' + self.tahun
        hdr_cells[1].text = 'Preventive Maintenance'

        row_cells = table_footer.add_row().cells
        row_cells[0].text = ''
        row_cells[1].text = ''

        run = row_cells[1].paragraphs[0].runs[0]
        run.add_picture(imgwater, width=Cm(2))

        # Daftar Isi
        p = document.add_paragraph('Contents')
        p.style = document.styles['Heading 1']

        p = document.add_paragraph()
        run = p.add_run('Buat Manual: References -> Table of Content')
        font = run.font
        font.color.rgb = RGBColor(255, 0, 0)

        document.add_page_break()

        # Record of Document
        p = document.add_paragraph('Record of Document')
        p.style = document.styles['Heading 1']

        document.add_paragraph()

        p = document.add_paragraph()
        p.add_run('Distribution Control').bold = True
        p.style = document.styles['Subtitle']

        data_approval = (
            ('1. Approval Copy', self.customer),
            ('2. Approval Copy', compnet)
        )
        table_approval = document.add_table(rows=1, cols=2)
        qr48 = func_get_req(docx_create+"qr48")
        table_approval.style = qr48
        hdr_cells = table_approval.rows[0].cells
        hdr_cells[0].text = 'Copy No.'
        hdr_cells[1].text = 'Distribution'

        for data_approval1, data_approval2 in data_approval:
            row_cells = table_approval.add_row().cells
            row_cells[0].text = data_approval1
            row_cells[1].text = data_approval2

        document.add_paragraph()

        p = document.add_paragraph()
        p.add_run('Review List').bold = True
        p.style = document.styles['Subtitle']

        table_review = document.add_table(rows=1, cols=2)
        qr48 = func_get_req(docx_create+"qr48")
        table_review.style = qr48
        hdr_cells = table_review.rows[0].cells
        hdr_cells[0].text = 'Reviewed by'
        hdr_cells[1].text = 'Date'

        row_cells = table_review.add_row().cells
        row_cells[0].text = self.review
        row_cells[1].text = self.bulan+' '+self.tahun

        document.add_paragraph()
        document.add_paragraph()
        document.add_paragraph()
        document.add_paragraph()
        document.add_paragraph()
        document.add_paragraph()
        document.add_paragraph()
        document.add_paragraph()
        document.add_paragraph()
        document.add_paragraph()
        document.add_paragraph()
        document.add_paragraph()

        table_ttd = document.add_table(rows=1, cols=3)
        qr49 = func_get_req(docx_create+"qr49")
        table_ttd.style = qr49
        hdr_cells = table_ttd.rows[0].cells
        hdr_cells[0].text = 'Prepared by,\n\n\n\n'
        hdr_cells[1].text = 'Reviewed by,\n\n\n\n'
        hdr_cells[2].text = 'Accepted by,\n\n\n\n'
        table_ttd2 = document.add_table(rows=1, cols=3)
        qr50 = func_get_req(docx_create+"qr50")
        table_ttd2.style = qr50
        hdr_cells = table_ttd2.rows[0].cells
        hdr_cells[0].text = self.prepared
        hdr_cells[1].text = self.review
        hdr_cells[2].text = self.pic
        table_ttd3 = document.add_table(rows=1, cols=3)
        qr51 = func_get_req(docx_create+"qr51")
        table_ttd3.style = qr51
        hdr_cells = table_ttd3.rows[0].cells
        hdr_cells[0].text = 'Compnet'
        hdr_cells[1].text = 'Compnet'
        hdr_cells[2].text = 'PIC'
        table_ttd4 = document.add_table(rows=1, cols=3)
        table_ttd4.style = 'TTD3'
        hdr_cells = table_ttd4.rows[0].cells
        hdr_cells[0].text = 'Network Engineer'
        hdr_cells[1].text = 'Customer Engineer'
        hdr_cells[2].text = self.customer

        document.add_page_break()

        # Executive Summary
        p = document.add_paragraph('Executive Summary')
        p.style = document.styles['Heading 1']

        p = document.add_paragraph()
        p.add_run(
            'Preventive Maintenance is one of services included in Maintenance Contract Agreement between ')
        p.paragraph_format.space_before = Pt(6)
        p.add_run(compnet).bold = True
        p.add_run(' and ')
        p.add_run(self.customer).bold = True
        p.add_run(
            ' Maintenance check all the equipment (which is under Contract), to make sure there are no potential problem.')
        p.style = document.styles['Subtitle']
        p = document.add_paragraph(
            'Preventive Maintenance was done on '+self.bulan+', '+self.tahun+'.')
        p.style = document.styles['Subtitle']

        p = document.add_paragraph('Analysis Summary')
        p.style = document.styles['Heading 2']

        # TIPE SUMMARY
        for enum, tipe in enumerate(all_tipe):
            p = document.add_paragraph(
                'Number of devices which are analyzed, '+tipe+': '+str(all_count_tipe[enum])+' unit.')
            p.style = document.styles['List Paragraph']
        
        if 'F5' in self.report_type or 'Routing and Switching' in self.report_type or 'Collaboration' in self.report_type:

            p = document.add_paragraph(
                'Software Analysis found no problems on all devices.')
            p.style = document.styles['List Paragraph']

            #open db connection F5
            os.chdir(capture_path)
            db_f5 = sqlite3.connect('pmdb_f5')
            cursor_f5 = db_f5.cursor()

            #sql query F5
            cursor_f5.execute('''SELECT model, version FROM swtable ORDER BY model, version ASC''')
            records_f5 = cursor_f5.fetchall()

            if 'F5' in self.report_type:
                p = document.add_paragraph('Bug Summary F5')
                p.style = document.styles['Subtitle']

                # BUG SUMMARY F5
                table = document.add_table(rows=1, cols=10)
                table.allow_autofit = True
                qr46 = func_get_req(docx_create+"qr46")
                table.style = qr46
                hdr_cells = table.rows[0].cells

                table.rows[0].height = Cm(1.4)
                set_repeat_table_header(table.rows[0])

                hdr_cells[0].text = 'Device'
                hdr_cells[1].text = 'Version'
                hdr_cells[2].text = 'Count of Bug Severity'
                hdr_cells[7].text = 'Grand Total'
                hdr_cells[8].text = 'Suggested Release Version'
                hdr_cells[9].text = 'Suggestion'

                row_cells = table.add_row().cells

                my_cell1 = table.cell(1, 2)
                my_cell2 = table.cell(1, 3)
                my_cell3 = table.cell(1, 4)
                my_cell4 = table.cell(1, 5)
                my_cell5 = table.cell(1, 6)

                my_p1 = my_cell1.paragraphs[0]
                my_p2 = my_cell2.paragraphs[0]
                my_p3 = my_cell3.paragraphs[0]
                my_p4 = my_cell4.paragraphs[0]
                my_p5 = my_cell5.paragraphs[0]

                run1 = my_p1.add_run('1')
                run2 = my_p2.add_run('2')
                run3 = my_p3.add_run('3')
                run4 = my_p4.add_run('4')
                run5 = my_p5.add_run('5')

                white = RGBColor(255, 255, 255)

                run1.font.color.rgb = white
                run2.font.color.rgb = white
                run3.font.color.rgb = white
                run4.font.color.rgb = white
                run5.font.color.rgb = white

                run1.bold = True
                run2.bold = True
                run3.bold = True
                run4.bold = True
                run5.bold = True

                shade_cells([table.cell(1, 2), table.cell(1, 3), table.cell(
                    1, 4), table.cell(1, 5), table.cell(1, 6)], "#006699")

                a_countofbug = table.cell(0, 2)
                b_countofbug = table.cell(0, 6)
                A_countofbug = a_countofbug.merge(b_countofbug)

                a_device = table.cell(0, 0)
                b_device = table.cell(1, 0)
                A_device = a_device.merge(b_device)

                a_version = table.cell(0, 1)
                b_version = table.cell(1, 1)
                A_version = a_version.merge(b_version)

                a_total = table.cell(0, 7)
                b_total = table.cell(1, 7)
                A_total = a_total.merge(b_total)

                a_release = table.cell(0, 8)
                b_release = table.cell(1, 8)
                A_release = a_release.merge(b_release)

                a_suggest = table.cell(0, 9)
                b_suggest = table.cell(1, 9)
                A_suggest = a_suggest.merge(b_suggest)

                # data section
                row_check = 'ludesdeveloper'
                row_version = 'ludesdeveloper'
                iteration_check = False
                count_row = 2

                red = RGBColor(255, 0, 0)

                for row in records_f5:
                    if row_check not in row[0] and iteration_check == False:
                        row_cells = table.add_row().cells
                        row_check = row[0]
                        row_version = row[1]
                        iteration_check = False
                        start_row = table.cell(count_row, 0)

                        row_cells[0].text = (row[0])
                        row_cells[1].text = (row[1])
                        row_cells[2].text = 'X'
                        row_cells[3].text = 'X'
                        row_cells[4].text = 'X'
                        row_cells[5].text = 'X'
                        row_cells[6].text = 'X'
                        row_cells[7].text = 'X'
                        row_cells[8].text = 'X'
                        row_cells[9].text = 'X'

                        row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

                        run1 = row_cells[2].paragraphs[0].runs[0]
                        run2 = row_cells[3].paragraphs[0].runs[0]
                        run3 = row_cells[4].paragraphs[0].runs[0]
                        run4 = row_cells[5].paragraphs[0].runs[0]
                        run5 = row_cells[6].paragraphs[0].runs[0]
                        run6 = row_cells[7].paragraphs[0].runs[0]
                        run7 = row_cells[8].paragraphs[0].runs[0]
                        run8 = row_cells[9].paragraphs[0].runs[0]

                        run1.font.color.rgb = red
                        run2.font.color.rgb = red
                        run3.font.color.rgb = red
                        run4.font.color.rgb = red
                        run5.font.color.rgb = red
                        run6.font.color.rgb = red
                        run7.font.color.rgb = red
                        run8.font.color.rgb = red

                        iteration_check == True
                        count_row += 1

                    elif row_check in row[0] and row_version not in row[1] and iteration_check == False:
                        row_cells = table.add_row().cells
                        row_check = row[0]
                        row_version = row[1]

                        row_cells[1].text = (row[1])
                        row_cells[2].text = 'X'
                        row_cells[3].text = 'X'
                        row_cells[4].text = 'X'
                        row_cells[5].text = 'X'
                        row_cells[6].text = 'X'
                        row_cells[7].text = 'X'
                        row_cells[8].text = 'X'
                        row_cells[9].text = 'X'

                        row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

                        run1 = row_cells[2].paragraphs[0].runs[0]
                        run2 = row_cells[3].paragraphs[0].runs[0]
                        run3 = row_cells[4].paragraphs[0].runs[0]
                        run4 = row_cells[5].paragraphs[0].runs[0]
                        run5 = row_cells[6].paragraphs[0].runs[0]
                        run6 = row_cells[7].paragraphs[0].runs[0]
                        run7 = row_cells[8].paragraphs[0].runs[0]
                        run8 = row_cells[9].paragraphs[0].runs[0]

                        run1.font.color.rgb = red
                        run2.font.color.rgb = red
                        run3.font.color.rgb = red
                        run4.font.color.rgb = red
                        run5.font.color.rgb = red
                        run6.font.color.rgb = red
                        run7.font.color.rgb = red
                        run8.font.color.rgb = red

                        end_row = table.cell(count_row, 0)
                        merge_row = start_row.merge(end_row)
                        iteration_check == True
                        count_row += 1

                    row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP

                # Tabel Severity Level
                data_severity = (
                    ('1', 'Blocking', 'Causes a site down, upgrading causes a module that previously worked to fail to load, or blocks development and/or testing work.'),
                    ('2', 'Critical', "Crash, loss of data, severe memory leak, or regression. Users are not able to perform a task or complete a scenario with the design as implemented. Important features are missing, broken, or behaving in a way that users won't be able to understand or remedy. Reasonable user action results in data loss."),
                    ('3', 'Major', 'Major loss of function:\n\u2022 This issue may lead a customer to unknowingly put their site or system into an undesirable state.\n\u2022 Any issue that erroneously reports system status, environmental status, or system telemetry that service providers rely on to accurately report system status, including system chassis indicator lights.\n\u2022 Significant interaction, layout, or other visual defects that most users will notice, and will slow or block users from completing important tasks. Clear negative impact to the perception of quality.'),
                    ('4', 'Minor', 'Minor loss of function or other issue where an easy workaround exists. Moderate interaction, layout, or visual defects that do not block or slow a user from completing a desired task, but are noticeable.'),
                    ('5', 'Cosmetic', 'Minor usability and formatting issues. Layout or visual defects that are likely to go unnoticed by users.'),
                )

                p = document.add_paragraph('')

                table_severity = document.add_table(rows=1, cols=3)
                table_severity.allow_autofit = True
                qr52 = func_get_req(docx_create+"qr52")
                table_severity.style = qr52
                hdr_cells = table_severity.rows[0].cells
                table_severity.rows[0].height = Cm(1.2)

                hdr_cells[0].width = Cm(1.12)
                hdr_cells[1].width = Cm(1.12)
                hdr_cells[2].width = Cm(13.92)
                set_repeat_table_header(table_severity.rows[0])

                hdr_cells[0].text = 'No'
                hdr_cells[1].text = 'Severity Level'
                hdr_cells[2].text = 'Description'

                for data_severity1, data_severity2, data_severity3 in data_severity:
                    row_cells = table_severity.add_row().cells
                    row_cells[0].width = Cm(1.12)
                    row_cells[1].width = Cm(1.12)
                    row_cells[2].width = Cm(13.92)

                    row_cells[0].text = data_severity1
                    row_cells[1].text = data_severity2
                    row_cells[2].text = data_severity3

                p = document.add_paragraph('')

            if 'Routing and Switching' in self.report_type or 'Collaboration' in self.report_type:
                p = document.add_paragraph('Bug Summary')
                p.style = document.styles['Subtitle']

                # BUG SUMMARY CISCO
                table = document.add_table(rows=1, cols=11)
                table.allow_autofit = True
                qr46 = func_get_req(docx_create+"qr46")
                table.style = qr46
                hdr_cells = table.rows[0].cells

                table.rows[0].height = Cm(1.4)
                set_repeat_table_header(table.rows[0])

                hdr_cells[0].text = 'Device'
                hdr_cells[1].text = 'Version'
                hdr_cells[2].text = 'Count of Bug Severity'
                hdr_cells[8].text = 'Grand Total'
                hdr_cells[9].text = 'Suggested Release Version'
                hdr_cells[10].text = 'Suggestion'

                row_cells = table.add_row().cells

                my_cell1 = table.cell(1, 2)
                my_cell2 = table.cell(1, 3)
                my_cell3 = table.cell(1, 4)
                my_cell4 = table.cell(1, 5)
                my_cell5 = table.cell(1, 6)
                my_cell6 = table.cell(1, 7)

                my_p1 = my_cell1.paragraphs[0]
                my_p2 = my_cell2.paragraphs[0]
                my_p3 = my_cell3.paragraphs[0]
                my_p4 = my_cell4.paragraphs[0]
                my_p5 = my_cell5.paragraphs[0]
                my_p6 = my_cell6.paragraphs[0]

                run1 = my_p1.add_run('1')
                run2 = my_p2.add_run('2')
                run3 = my_p3.add_run('3')
                run4 = my_p4.add_run('4')
                run5 = my_p5.add_run('5')
                run6 = my_p6.add_run('6')

                white = RGBColor(255, 255, 255)

                run1.font.color.rgb = white
                run2.font.color.rgb = white
                run3.font.color.rgb = white
                run4.font.color.rgb = white
                run5.font.color.rgb = white
                run6.font.color.rgb = white

                run1.bold = True
                run2.bold = True
                run3.bold = True
                run4.bold = True
                run5.bold = True
                run6.bold = True

                shade_cells([table.cell(1, 2), table.cell(1, 3), table.cell(
                    1, 4), table.cell(1, 5), table.cell(1, 6), table.cell(1, 7)], "#006699")

                a_countofbug = table.cell(0, 2)
                b_countofbug = table.cell(0, 7)
                A_countofbug = a_countofbug.merge(b_countofbug)

                a_device = table.cell(0, 0)
                b_device = table.cell(1, 0)
                A_device = a_device.merge(b_device)

                a_version = table.cell(0, 1)
                b_version = table.cell(1, 1)
                A_version = a_version.merge(b_version)

                a_total = table.cell(0, 8)
                b_total = table.cell(1, 8)
                A_total = a_total.merge(b_total)

                a_release = table.cell(0, 9)
                b_release = table.cell(1, 9)
                A_release = a_release.merge(b_release)

                a_suggest = table.cell(0, 10)
                b_suggest = table.cell(1, 10)
                A_suggest = a_suggest.merge(b_suggest)

                # data section
                row_check = 'ludesdeveloper'
                row_version = 'ludesdeveloper'
                iteration_check = False
                count_row = 2

                red = RGBColor(255, 0, 0)

                for enum, model in enumerate(all_model):
                    if row_check not in model and iteration_check == False:
                        row_cells = table.add_row().cells
                        row_check = model
                        row_version = all_version[enum]
                        iteration_check = False
                        start_row = table.cell(count_row, 0)

                        row_cells[0].text = (model)
                        row_cells[1].text = (all_version[enum])
                        row_cells[2].text = 'X'
                        row_cells[3].text = 'X'
                        row_cells[4].text = 'X'
                        row_cells[5].text = 'X'
                        row_cells[6].text = 'X'
                        row_cells[7].text = 'X'
                        row_cells[8].text = 'X'
                        row_cells[9].text = 'X'
                        row_cells[10].text = 'X'

                        row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

                        run1 = row_cells[2].paragraphs[0].runs[0]
                        run2 = row_cells[3].paragraphs[0].runs[0]
                        run3 = row_cells[4].paragraphs[0].runs[0]
                        run4 = row_cells[5].paragraphs[0].runs[0]
                        run5 = row_cells[6].paragraphs[0].runs[0]
                        run6 = row_cells[7].paragraphs[0].runs[0]
                        run7 = row_cells[8].paragraphs[0].runs[0]
                        run8 = row_cells[9].paragraphs[0].runs[0]
                        run9 = row_cells[10].paragraphs[0].runs[0]

                        run1.font.color.rgb = red
                        run2.font.color.rgb = red
                        run3.font.color.rgb = red
                        run4.font.color.rgb = red
                        run5.font.color.rgb = red
                        run6.font.color.rgb = red
                        run7.font.color.rgb = red
                        run8.font.color.rgb = red
                        run9.font.color.rgb = red

                        iteration_check == True
                        count_row += 1

                    elif row_check in model and row_version not in all_version[enum] and iteration_check == False:
                        row_cells = table.add_row().cells
                        row_check = model
                        row_version = all_version[enum]

                        row_cells[1].text = (all_version[enum])
                        row_cells[2].text = 'X'
                        row_cells[3].text = 'X'
                        row_cells[4].text = 'X'
                        row_cells[5].text = 'X'
                        row_cells[6].text = 'X'
                        row_cells[7].text = 'X'
                        row_cells[8].text = 'X'
                        row_cells[9].text = 'X'
                        row_cells[10].text = 'X'

                        row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

                        run1 = row_cells[2].paragraphs[0].runs[0]
                        run2 = row_cells[3].paragraphs[0].runs[0]
                        run3 = row_cells[4].paragraphs[0].runs[0]
                        run4 = row_cells[5].paragraphs[0].runs[0]
                        run5 = row_cells[6].paragraphs[0].runs[0]
                        run6 = row_cells[7].paragraphs[0].runs[0]
                        run7 = row_cells[8].paragraphs[0].runs[0]
                        run8 = row_cells[9].paragraphs[0].runs[0]
                        run9 = row_cells[10].paragraphs[0].runs[0]

                        run1.font.color.rgb = red
                        run2.font.color.rgb = red
                        run3.font.color.rgb = red
                        run4.font.color.rgb = red
                        run5.font.color.rgb = red
                        run6.font.color.rgb = red
                        run7.font.color.rgb = red
                        run8.font.color.rgb = red
                        run9.font.color.rgb = red

                        end_row = table.cell(count_row, 0)
                        merge_row = start_row.merge(end_row)
                        iteration_check == True
                        count_row += 1

                    row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP

                # Tabel Severity Level
                data_severity = (
                    ('1', 'Catastrophic', 'Reasonably common circumstances cause the entire system to fail, or a major subsystem to stop working, or other devices on the network to be disrupted. No workarounds exist.'),
                    ('2', 'Severe', 'Important functions are unusable and workarounds do not exist. Other functions and the rest of the network is operating normally.'),
                    ('3', 'Moderate', 'Failures occur in unusual circumstances, or minor features do not work at all, or other failures occur but low-impact workarounds exist. This is the highest level for documentation bugs.'),
                    ('4', 'Minor', 'Failures occur under very unusual circumstances, but operation essentially recovers without intervention. Users do not need to install any workarounds and performance impact is tolerable.'),
                    ('5', 'Cosmetic', 'Defects do not cause any detrimental effect on system functionality.'),
                    ('6', 'Enhancement', 'Requests for new functionality or feature improvements.')
                )

                p = document.add_paragraph('')

                table_severity = document.add_table(rows=1, cols=3)
                table_severity.allow_autofit = True
                qr52 = func_get_req(docx_create+"qr52")
                table_severity.style = qr52
                hdr_cells = table_severity.rows[0].cells
                table_severity.rows[0].height = Cm(1.2)

                hdr_cells[0].width = Cm(1.12)
                hdr_cells[1].width = Cm(1.12)
                hdr_cells[2].width = Cm(13.92)
                set_repeat_table_header(table.rows[0])

                hdr_cells[0].text = 'No'
                hdr_cells[1].text = 'Severity Level'
                hdr_cells[2].text = 'Description'

                for data_severity1, data_severity2, data_severity3 in data_severity:
                    row_cells = table_severity.add_row().cells
                    row_cells[0].width = Cm(1.12)
                    row_cells[1].width = Cm(1.12)
                    row_cells[2].width = Cm(13.92)

                    row_cells[0].text = data_severity1
                    row_cells[1].text = data_severity2
                    row_cells[2].text = data_severity3

                p = document.add_paragraph('')

            # HARDWARE SUMMARY TABLE
            # sql query
            os.chdir(capture_path)
            db = sqlite3.connect('pmdb_rs')
            cursor = db.cursor()
            cursor.execute(
                '''SELECT card, date, COUNT(*) FROM eosltable where DATE(date) <= DATE('now') GROUP BY card''')
            records = cursor.fetchall()
            cursor.execute('''SELECT COUNT(card) FROM eosltable''')
            total = cursor.fetchall()
            total = (str(total))
            total = re.sub("\D", "", total)
            total = int(total)

            if len(records) == 0:
                p = document.add_paragraph(
                    'Hardware Analysis found no problems on all devices.')
                p.style = document.styles['List Paragraph']

            else:
                p = document.add_paragraph(
                    'Hardware Analysis found hardware that has exceeded end of support. List of models is explained below:')
                p.style = document.styles['List Paragraph']
                # add to document
                table = document.add_table(rows=1, cols=3)
                table.allow_autofit = True
                qr46 = func_get_req(docx_create+"qr46")
                table.style = qr46
                hdr_cells = table.rows[0].cells
                set_repeat_table_header(table.rows[0])
                table.rows[0].height = Cm(1.2)

                hdr_cells[0].text = 'Model'
                hdr_cells[1].text = 'Total'
                hdr_cells[2].text = 'End of Support'

                hdr_cells[0].width = Cm(4.25)
                hdr_cells[1].width = Cm(1.69)
                hdr_cells[2].width = Cm(4.25)

                for row in records:
                    row_cells = table.add_row().cells
                    row_cells[0].width = Cm(4.25)
                    row_cells[1].width = Cm(1.69)
                    row_cells[2].width = Cm(4.25)

                    row_cells[0].text = (row[0])
                    row_cells[1].text = str(row[2])
                    row_cells[2].text = (row[1])

            # close database
            db.close()

            # CPU SUMMARY TABLE
            # sql query RS
            os.chdir(capture_path)
            db = sqlite3.connect('pmdb_rs')
            cursor = db.cursor()
            cursor.execute(
                '''SELECT devicename, model, total, process, interrupt, topcpu, status FROM cpusumtable WHERE status = 'High' ORDER by devicename ASC''')
            records = cursor.fetchall()

            # sql query UCM
            os.chdir(capture_path)
            db_ucm = sqlite3.connect('pmdb_ucm')
            cursor = db_ucm.cursor()
            cursor.execute(
                '''SELECT devicename, cpu, top_cpu, cpu_status FROM cputable WHERE cpu_status = 'High' ORDER by devicename ASC''')
            records_ucm = cursor.fetchall()

            if (len(records) + len(records_ucm)) == 0:
                p = document.add_paragraph(
                    'Processor Analysis found no problems on all devices.')
                p.style = document.styles['List Paragraph']

            elif len(records) == 0:
                p = document.add_paragraph(
                    'Processor Analysis found '+str(len(records) + len(records_ucm))+' problems on all devices.')
                p.style = document.styles['List Paragraph']

                # Tabel CPU UCM
                table = document.add_table(rows=1, cols=4)
                table.allow_autofit = True
                qr46 = func_get_req(docx_create+"qr46")
                table.style = qr46
                hdr_cells = table.rows[0].cells
                set_repeat_table_header(table.rows[0])
                table.rows[0].height = Cm(1.2)

                hdr_cells[0].width = Cm(6.69)
                hdr_cells[1].width = Cm(2.75)
                hdr_cells[2].width = Cm(5.5)
                hdr_cells[3].width = Cm(2.08)

                hdr_cells[0].text = 'Device Name'
                hdr_cells[1].text = 'CPU Utilization (%)'
                hdr_cells[2].text = 'Top Process'
                hdr_cells[3].text = 'Status'

                for row in records_ucm:

                    row_cells = table.add_row().cells
                    row_cells[0].width = Cm(6.69)
                    row_cells[1].width = Cm(2.75)
                    row_cells[2].width = Cm(5.5)
                    row_cells[3].width = Cm(2.08)

                    convert_float = float(row[1])
                    convert_round = round(convert_float, 2)
                    convert_str = str(convert_round)

                    row_cells[0].text = (row[0])
                    row_cells[1].text = (convert_str+'%')
                    row_cells[2].text = (row[2])
                    row_cells[3].text = (row[3])

            elif len(records_ucm) == 0:
                p = document.add_paragraph(
                    'Processor Analysis found '+str(len(records) + len(records_ucm))+' problems on all devices.')
                p.style = document.styles['List Paragraph']

                # Tabel CPU RS
                table = document.add_table(rows=1, cols=7)
                table.allow_autofit = True
                qr46 = func_get_req(docx_create+"qr46")
                table.style = qr46
                hdr_cells = table.rows[0].cells
                set_repeat_table_header(table.rows[0])
                table.rows[0].height = Cm(1.2)

                hdr_cells[0].width = Cm(3.3)
                hdr_cells[1].width = Cm(3.3)
                hdr_cells[2].width = Cm(1.8)
                hdr_cells[3].width = Cm(1.8)
                hdr_cells[4].width = Cm(1.8)
                hdr_cells[5].width = Cm(3.2)
                hdr_cells[6].width = Cm(1.8)

                hdr_cells[0].text = 'Device Name'
                hdr_cells[1].text = 'Model'
                hdr_cells[2].text = 'Total'
                hdr_cells[3].text = 'Process'
                hdr_cells[4].text = 'Interrupt'
                hdr_cells[5].text = 'Top Process'
                hdr_cells[6].text = 'Status'

                for row in records:

                    row_cells = table.add_row().cells
                    row_cells[0].width = Cm(3.3)
                    row_cells[1].width = Cm(3.3)
                    row_cells[2].width = Cm(1.8)
                    row_cells[3].width = Cm(1.8)
                    row_cells[4].width = Cm(1.8)
                    row_cells[5].width = Cm(3.2)
                    row_cells[6].width = Cm(1.8)

                    row_cells[0].text = (row[0])
                    row_cells[1].text = (row[1])
                    row_cells[2].text = (row[2]+"%")
                    row_cells[3].text = (row[3]+"%")
                    row_cells[4].text = (row[4]+"%")
                    row_cells[5].text = (row[5])
                    row_cells[6].text = (row[6])

                    row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

            else:
                p = document.add_paragraph(
                    'Processor Analysis found '+str(len(records) + len(records_ucm))+' problems on all devices.')
                p.style = document.styles['List Paragraph']

                # Tabel CPU RS
                p = document.add_paragraph(
                    'Routing and Switching Devices.')
                table = document.add_table(rows=1, cols=7)
                table.allow_autofit = True
                qr46 = func_get_req(docx_create+"qr46")
                table.style = qr46
                hdr_cells = table.rows[0].cells
                set_repeat_table_header(table.rows[0])
                table.rows[0].height = Cm(1.2)

                hdr_cells[0].width = Cm(3.3)
                hdr_cells[1].width = Cm(3.3)
                hdr_cells[2].width = Cm(1.8)
                hdr_cells[3].width = Cm(1.8)
                hdr_cells[4].width = Cm(1.8)
                hdr_cells[5].width = Cm(3.2)
                hdr_cells[6].width = Cm(1.8)

                hdr_cells[0].text = 'Device Name'
                hdr_cells[1].text = 'Model'
                hdr_cells[2].text = 'Total'
                hdr_cells[3].text = 'Process'
                hdr_cells[4].text = 'Interrupt'
                hdr_cells[5].text = 'Top Process'
                hdr_cells[6].text = 'Status'

                for row in records:

                    row_cells = table.add_row().cells
                    row_cells[0].width = Cm(3.3)
                    row_cells[1].width = Cm(3.3)
                    row_cells[2].width = Cm(1.8)
                    row_cells[3].width = Cm(1.8)
                    row_cells[4].width = Cm(1.8)
                    row_cells[5].width = Cm(3.2)
                    row_cells[6].width = Cm(1.8)

                    row_cells[0].text = (row[0])
                    row_cells[1].text = (row[1])
                    row_cells[2].text = (row[2]+"%")
                    row_cells[3].text = (row[3]+"%")
                    row_cells[4].text = (row[4]+"%")
                    row_cells[5].text = (row[5])
                    row_cells[6].text = (row[6])

                    row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

                p = document.add_paragraph('')

                # Tabel CPU UCM
                p = document.add_paragraph(
                    'Collaboration Devices.')
                table = document.add_table(rows=1, cols=4)
                table.allow_autofit = True
                qr46 = func_get_req(docx_create+"qr46")
                table.style = qr46
                hdr_cells = table.rows[0].cells
                set_repeat_table_header(table.rows[0])
                table.rows[0].height = Cm(1.2)

                hdr_cells[0].width = Cm(6.69)
                hdr_cells[1].width = Cm(2.75)
                hdr_cells[2].width = Cm(5.5)
                hdr_cells[3].width = Cm(2.08)

                hdr_cells[0].text = 'Device Name'
                hdr_cells[1].text = 'CPU Utilization (%)'
                hdr_cells[2].text = 'Top Process'
                hdr_cells[3].text = 'Status'

                for row in records_ucm:

                    row_cells = table.add_row().cells
                    row_cells[0].width = Cm(6.69)
                    row_cells[1].width = Cm(2.75)
                    row_cells[2].width = Cm(5.5)
                    row_cells[3].width = Cm(2.08)

                    convert_float = float(row[1])
                    convert_round = round(convert_float, 2)
                    convert_str = str(convert_round)

                    row_cells[0].text = (row[0])
                    row_cells[1].text = (convert_str+'%')
                    row_cells[2].text = (row[2])
                    row_cells[3].text = (row[3])
        
            # close database
            db.close()
            db_ucm.close()

            # MEMORY SUMMARY TABLE
            # sql query RS
            os.chdir(capture_path)
            db = sqlite3.connect('pmdb_rs')
            cursor = db.cursor()
            cursor.execute(
                '''SELECT devicename, model, utils, topproc, status FROM memsumtable WHERE status = 'High' ORDER by devicename ASC''')
            records = cursor.fetchall()

            # sql query UCM
            os.chdir(capture_path)
            db_ucm = sqlite3.connect('pmdb_ucm')
            cursor = db_ucm.cursor()
            cursor.execute(
                '''SELECT devicename, memory, top_memory, memory_status FROM memtable WHERE memory_status = 'High' ORDER by devicename ASC''')
            records_ucm = cursor.fetchall()

            if (len(records) + len(records_ucm)) == 0:
                p = document.add_paragraph(
                    'Memory Analysis found no problems on all devices.')
                p.style = document.styles['List Paragraph']

            elif len(records) == 0:
                p = document.add_paragraph(
                    'Memory Analysis found '+str(len(records) + len(records_ucm))+' problems on all devices.')
                p.style = document.styles['List Paragraph']

                # Tabel Memory UCM
                table = document.add_table(rows=1, cols=4)
                table.allow_autofit = True
                qr46 = func_get_req(docx_create+"qr46")
                table.style = qr46
                hdr_cells = table.rows[0].cells
                set_repeat_table_header(table.rows[0])
                table.rows[0].height = Cm(1.2)

                hdr_cells[0].width = Cm(4.25)
                hdr_cells[1].width = Cm(4.25)
                hdr_cells[2].width = Cm(6.42)
                hdr_cells[3].width = Cm(2.08)

                hdr_cells[0].text = 'Device Name'
                hdr_cells[1].text = 'Memory Utilization (%)'
                hdr_cells[2].text = 'Top Process'
                hdr_cells[3].text = 'Status'

                for row in records_ucm:

                    row_cells = table.add_row().cells
                    row_cells[0].width = Cm(4.25)
                    row_cells[1].width = Cm(4.25)
                    row_cells[2].width = Cm(6.42)
                    row_cells[3].width = Cm(2.08)

                    convert_float = float(row[1])
                    convert_round = round(convert_float, 2)
                    convert_str = str(convert_round)

                    row_cells[0].text = (row[0])
                    row_cells[1].text = (convert_str+'%')
                    row_cells[2].text = (row[2])
                    row_cells[3].text = (row[3])

            elif len(records_ucm) == 0:
                p = document.add_paragraph(
                    'Memory Analysis found '+str(len(records) + len(records_ucm))+' problems on all devices.')
                p.style = document.styles['List Paragraph']

                # Tabel Memory RS
                table = document.add_table(rows=1, cols=5)
                table.allow_autofit = True
                qr46 = func_get_req(docx_create+"qr46")
                table.style = qr46
                hdr_cells = table.rows[0].cells
                set_repeat_table_header(table.rows[0])
                table.rows[0].height = Cm(1.2)

                hdr_cells[0].text = 'Device Name'
                hdr_cells[1].text = 'Model'
                hdr_cells[2].text = 'Processor Memory Utilization'
                hdr_cells[3].text = 'Top Process'
                hdr_cells[4].text = 'Status'

                for row in records:

                    row_cells = table.add_row().cells
                    row_cells[0].text = (row[0])
                    row_cells[1].text = (row[1])
                    row_cells[2].text = (row[2]+"%")
                    row_cells[3].text = (row[3])
                    row_cells[4].text = (row[4])

                    row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

            else:
                p = document.add_paragraph(
                    'Memory Analysis found '+str(len(records) + len(records_ucm))+' problems on all devices.')
                p.style = document.styles['List Paragraph']

                # Table Memory RS
                p = document.add_paragraph(
                    'Routing and Switching Devices.')
                table = document.add_table(rows=1, cols=5)
                table.allow_autofit = True
                qr46 = func_get_req(docx_create+"qr46")
                table.style = qr46
                hdr_cells = table.rows[0].cells
                set_repeat_table_header(table.rows[0])
                table.rows[0].height = Cm(1.2)

                hdr_cells[0].text = 'Device Name'
                hdr_cells[1].text = 'Model'
                hdr_cells[2].text = 'Processor Memory Utilization'
                hdr_cells[3].text = 'Top Process'
                hdr_cells[4].text = 'Status'

                for row in records:

                    row_cells = table.add_row().cells
                    row_cells[0].text = (row[0])
                    row_cells[1].text = (row[1])
                    row_cells[2].text = (row[2]+"%")
                    row_cells[3].text = (row[3])
                    row_cells[4].text = (row[4])

                    row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

                p = document.add_paragraph('')

                # Table Memory UCM
                p = document.add_paragraph(
                    'Collaboration Devices.')
                table = document.add_table(rows=1, cols=4)
                table.allow_autofit = True
                qr46 = func_get_req(docx_create+"qr46")
                table.style = qr46
                hdr_cells = table.rows[0].cells
                set_repeat_table_header(table.rows[0])
                table.rows[0].height = Cm(1.2)

                hdr_cells[0].width = Cm(4.25)
                hdr_cells[1].width = Cm(4.25)
                hdr_cells[2].width = Cm(6.42)
                hdr_cells[3].width = Cm(2.08)

                hdr_cells[0].text = 'Device Name'
                hdr_cells[1].text = 'Memory Utilization (%)'
                hdr_cells[2].text = 'Top Process'
                hdr_cells[3].text = 'Status'

                for row in records_ucm:

                    row_cells = table.add_row().cells
                    row_cells[0].width = Cm(4.25)
                    row_cells[1].width = Cm(4.25)
                    row_cells[2].width = Cm(6.42)
                    row_cells[3].width = Cm(2.08)

                    convert_float = float(row[1])
                    convert_round = round(convert_float, 2)
                    convert_str = str(convert_round)

                    row_cells[0].text = (row[0])
                    row_cells[1].text = (convert_str+'%')
                    row_cells[2].text = (row[2])
                    row_cells[3].text = (row[3])

            # close database
            db.close()
            db_ucm.close()

            p = document.add_paragraph(
                'Log Analysis found no problems on all devices.')
            p.style = document.styles['List Paragraph']

            data_log = (
                ('Device', 'Hostname'),
                ('Log', '<Copy Paste Log Medium Severity> '),
                ('Explanation', '<Explanation> '),
                ('Workaround', '<Workaround> ')
            )
            table_log = document.add_table(rows=1, cols=2)
            table_log.allow_autofit = True
            qr53 = func_get_req(docx_create+"qr53")
            table_log.style = qr53
            hdr_cells = table_log.rows[0].cells
            table_log.rows[0].height = Cm(1.2)
            set_repeat_table_header(table_log.rows[0])

            red = RGBColor(255, 0, 0)

            hdr_cells[0].width = Cm(2.69)
            hdr_cells[1].width = Cm(14.31)

            hdr_cells[0].text = 'Log'
            hdr_cells[1].text = '<Isi Name Log jika Medium dan High, jika tidak ada, hapus tabel ini>'

            run_hdr = hdr_cells[1].paragraphs[0].runs[0]
            run_hdr.font.color.rgb = red

            for data_log1, data_log2 in data_log:
                row_cells = table_log.add_row().cells

                row_cells[0].width = Cm(2.69)
                row_cells[1].width = Cm(14.31)

                row_cells[0].text = data_log1
                row_cells[1].text = data_log2

                run = row_cells[1].paragraphs[0].runs[0]
                run.font.color.rgb = red

            p = document.add_paragraph('')
            db.close()

        if 'FortiGate' in self.report_type:
            p = document.add_paragraph(
                'System Analysis FortiGate:')
            p.style = document.styles['List Paragraph']

            p = document.add_paragraph(
                'System Analysis found no problems on all devices.')
            p.style = document.styles['List Child Paragraph']

            p = document.add_paragraph(
                'High Availability Analysis found no problems on all devices.')
            p.style = document.styles['List Child Paragraph']

            p = document.add_paragraph(
                'System Performance Analysis FortiGate:')
            p.style = document.styles['List Paragraph']

            p = document.add_paragraph(
                'CPU Utilization Analysis found no problems on all devices.')
            p.style = document.styles['List Child Paragraph']

            p = document.add_paragraph(
                'Memory Utilization Analysis found no problems on all devices.')
            p.style = document.styles['List Child Paragraph']

            p = document.add_paragraph(
                'Session Utilization Analysis found no problems on all devices.')
            p.style = document.styles['List Child Paragraph']

            p = document.add_paragraph(
                'License and Certificate Analysis FortiGate:')
            p.style = document.styles['List Paragraph']
            
            p = document.add_paragraph(
                'License Analysis found no problems on all devices.')
            p.style = document.styles['List Child Paragraph']

            p = document.add_paragraph(
                'Certificate Analysis found no problems on all devices.')
            p.style = document.styles['List Child Paragraph']

        document.add_page_break()

        # save document
        os.chdir(capture_path)
        print('')
        print('Saving Document')
        document.save('pm_cover.docx')
        print('Document has been saved to pm_cover.docx')
