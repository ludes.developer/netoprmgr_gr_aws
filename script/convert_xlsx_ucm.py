import sqlite3
import re
import pkg_resources
import os
import xlsxwriter

class convert_xlsx_ucm:
    @staticmethod
    def convert_xlsx_ucm():
        result_path = pkg_resources.resource_filename('netoprmgr', 'static/')
        result_path = os.path.join(result_path,'result/')

        print('')
        print('Processing Document')

        #open db connection
        db = sqlite3.connect('pmdb_ucm')
        cursor = db.cursor()

        #BUG SUMMARY
        #sql query
        cursor.execute('''SELECT model, version FROM swtable ORDER BY model, version ASC''')
        records = cursor.fetchall()
        #define workbook
        workbook = xlsxwriter.Workbook('collaboration.xlsx')

        #format title
        format1 = workbook.add_format({
                                'font_name' : 'Arial',
                                'font_size' : 12,
                                'bold' : 1,
                                'text_wrap': True
                                })
        #format header table
        format2 = workbook.add_format({
                                'font_name' : 'Arial',
                                'font_size' : 11,
                                'bold' : 1,
                                'border': 1,
                                'align' : 'center',
                                'valign' : 'vcenter',
                                'fg_color' : '#00659C',
                                'font_color': 'white',
                                'text_wrap': True
                                })
        #format number header
        format3 = workbook.add_format({
                                'font_name' : 'Arial',
                                'font_size' : 10,
                                'bold' : 1,
                                'border': 1,
                                'align' : 'center',
                                'valign' : 'vcenter',
                                'fg_color' : '#00659C',
                                'font_color': 'white',
                                'text_wrap': True
                                })
        #format data table
        format4 = workbook.add_format({
                                'font_name' : 'Arial',
                                'font_size' : 10,
                                'border': 1,
                                'align' : 'center',
                                'valign' : 'vcenter',
                                'text_wrap': True
                                })
        #format total bold table
        format5 = workbook.add_format({
                                'font_name' : 'Arial',
                                'font_size' : 10,
                                'bold' : 1,
                                'border': 1,
                                'align' : 'center',
                                'valign' : 'vcenter',
                                'text_wrap': True
                                })
        #format for merge
        merge_format = workbook.add_format({
                                'font_name' : 'Arial',
                                'font_size' : 11,
                                'bold': 1,
                                'border': 1,
                                'align': 'center',
                                'valign': 'vcenter',
                                'fg_color': '#00659C',
                                'font_color': 'white',
                                'text_wrap': True
                                })
        #create sheet
        worksheet = workbook.add_worksheet('Bug Summary')
        #header section
        worksheet.merge_range(0, 0, 0, 10, 'Bug Summary',format1)
        worksheet.merge_range(1, 0, 2, 0, 'Device', merge_format)
        worksheet.merge_range(1, 1, 2, 1, 'Version', merge_format)
        worksheet.merge_range(1, 2, 1, 7, 'Count of Bug Severity', merge_format)
        worksheet.merge_range(1, 8, 2, 8, 'Grand Total', merge_format)
        worksheet.merge_range(1, 9, 2, 9, 'Suggested Release Version', merge_format)
        worksheet.merge_range(1, 10, 2, 10, 'Suggestion', merge_format)

        worksheet.write(2,2,'1',format3)
        worksheet.write(2,3,'2',format3)
        worksheet.write(2,4,'3',format3)
        worksheet.write(2,5,'4',format3)
        worksheet.write(2,6,'5',format3)
        worksheet.write(2,7,'6',format3)

        #data section
        row_check = 'ludesdeveloper'
        row_version = 'ludesdeveloper'
        iteration_check = False
        count_row = 3
        for row in records:
            if row_check not in row[0] and iteration_check == False:

                row_check = row[0]
                row_version = row[1]
                iteration_check = False
                start_row = count_row

                worksheet.write(count_row,0,row[0],format4)
                worksheet.write(count_row,1,row[1],format4)
                worksheet.write(count_row,2,'',format4)
                worksheet.write(count_row,3,'',format4)
                worksheet.write(count_row,4,'',format4)
                worksheet.write(count_row,5,'',format4)
                worksheet.write(count_row,6,'',format4)
                worksheet.write(count_row,7,'',format4)
                worksheet.write(count_row,8,'',format4)
                worksheet.write(count_row,9,'',format4)
                worksheet.write(count_row,10,'',format4)
                iteration_check == True
                count_row+=1

            elif row_check in row[0] and row_version not in row[1] and iteration_check == False:
                row_check = row[0]
                row_version = row[1]

                worksheet.write(count_row,1,row[1],format4)
                worksheet.write(count_row,2,'',format4)
                worksheet.write(count_row,3,'',format4)
                worksheet.write(count_row,4,'',format4)
                worksheet.write(count_row,5,'',format4)
                worksheet.write(count_row,6,'',format4)
                worksheet.write(count_row,7,'',format4)
                worksheet.write(count_row,8,'',format4)
                worksheet.write(count_row,9,'',format4)
                worksheet.write(count_row,10,'',format4)
                end_row = count_row
                worksheet.merge_range(start_row, 0, end_row, 0, row[0], format4)
                iteration_check == True
                count_row+=1

        #SOFTWARE SUMMARY TABLE
        #sql query
        cursor.execute('''SELECT version, COUNT(*) FROM swsumtable GROUP BY version''')
        records = cursor.fetchall()
        cursor.execute('''SELECT COUNT(version) FROM swsumtable''')
        total = cursor.fetchall()
        total=(str(total))
        total=re.sub("\D", "", total)
        total=int(total)
        #create sheet
        worksheet = workbook.add_worksheet('Software Summary Table')
        
        #header section
        worksheet.merge_range(0, 0, 0, 4, 'Software Summary Table',format1)
        worksheet.write(1,0,'Version',format2)
        worksheet.write(1,1,'Total',format2)
        worksheet.write(1,2,'Percentage',format2)
        worksheet.write(1,3,'End of Software Maintenance',format2)
        worksheet.write(1,4,'Last Date of Support',format2)
        #header for chart
        worksheet.merge_range(0, 6, 0, 7, 'For Chart',format1)
        worksheet.write(1,6,'Version',format4)
        worksheet.write(1,7,'Percentage',format4)

        #data section
        count_row = 2
        for row in records:
            worksheet.write(count_row,0,row[0],format4)
            worksheet.write(count_row,1,str(row[1]),format4)
            percentage = (str((row[1]/total)*100))
            percentage = re.findall('\d+[.]\d',percentage)
            percentage = percentage[0]
            worksheet.write(count_row,2,(percentage+'%'),format4)
            worksheet.write(count_row,3,'',format4)
            worksheet.write(count_row,4,'',format4)
            worksheet.write(count_row,6,row[0],format4)
            try:
                worksheet.write(count_row,7,float(percentage),format4)
            except:
                worksheet.write(count_row,7,"-",format4)
            count_row += 1

        #chart section
        software_chart = workbook.add_chart({'type': 'pie'})
        software_chart.add_series({
            "categories": ["Software Summary Table", 2, 6, count_row-1, 6],
            "values":     ["Software Summary Table", 2, 7, count_row-1, 7],
            "data_labels": {"value": True},
        })
        worksheet.insert_chart('J2', software_chart)

        #SOFTWARE TABLE
        #sql query
        cursor.execute('''SELECT devicename, model, version, uptime FROM swtable ORDER by devicename ASC''')
        records = cursor.fetchall()
        #create sheet
        worksheet = workbook.add_worksheet('Software Table')
        
        #header section
        worksheet.merge_range(0, 0, 0, 4, 'Software Table',format1)
        worksheet.write(1,0,'Device Name',format2)
        worksheet.write(1,1,'Model',format2)
        worksheet.write(1,2,'OS Version',format2)
        worksheet.write(1,3,'Uptime',format2)
        worksheet.write(1,4,'End of Support',format2)

        #data section
        count_row = 2
        for row in records:
            worksheet.write(count_row,0,row[0],format4)
            worksheet.write(count_row,1,row[1],format4)
            worksheet.write(count_row,2,row[2],format4)
            worksheet.write(count_row,3,row[3],format4)
            worksheet.write(count_row,4,'-',format4)
            count_row += 1

        #CPU SUMMARY TABLE
        #sql query
        cursor.execute('''SELECT devicename, cpu, top_cpu, cpu_status FROM cputable ORDER by devicename ASC''')
        records = cursor.fetchall()
        #create sheet
        worksheet = workbook.add_worksheet('CPU Summary Table')
        
        #header section
        worksheet.merge_range(0, 0, 0, 3, 'CPU Summary Table',format1)
        worksheet.write(1,0,'Device Name',format2)
        worksheet.write(1,1,'CPU Utilization (%)',format2)
        worksheet.write(1,2,'Top Process',format2)
        worksheet.write(1,3,'Status',format2)
        #header for chart
        worksheet.merge_range(0, 5, 0, 6, 'For Chart',format1)
        worksheet.write(1,5,'Device Name',format4)
        worksheet.write(1,6,'CPU Utilization (%)',format4)

        #data section
        count_row = 2
        for row in records:
            worksheet.write(count_row,0,row[0],format4)
            worksheet.write(count_row,1,(row[1]+"%"),format4)
            worksheet.write(count_row,2,row[2],format4)
            worksheet.write(count_row,3,row[3],format4)
            worksheet.write(count_row,5,row[0],format4)
            try:
                worksheet.write(count_row,6,float(row[1]),format4)
            except:
                worksheet.write(count_row,6,"-",format4)
            count_row += 1

        #chart section
        cpu_chart = workbook.add_chart({'type': 'column'})
        for i in range(3,count_row+1):
            cpu_chart.add_series(
                {
                    "name" : "='CPU Summary Table'!F"+str(i),
                    "values": "='CPU Summary Table'!G"+str(i),
                    "data_labels": {"value": True},
                    }
                )
        worksheet.insert_chart('L2', cpu_chart)

        #MEMORY SUMMARY TABLE
        #sql query
        cursor.execute('''SELECT devicename, memory, top_memory, memory_status FROM memtable ORDER by devicename ASC''')
        records = cursor.fetchall()
        #create sheet
        worksheet = workbook.add_worksheet('Memory Summary Table')
        
        #header section
        worksheet.merge_range(0, 0, 0, 4, 'Memory Summary Table',format1)
        worksheet.write(1,0,'Device Name',format2)
        worksheet.write(1,1,'Memory Utilization (%)',format2)
        worksheet.write(1,2,'Top Process',format2)
        worksheet.write(1,3,'Status',format2)
        #header for chart
        worksheet.merge_range(0, 5, 0, 6, 'For Chart',format1)
        worksheet.write(1,5,'Device Name',format4)
        worksheet.write(1,6,'Memory Utilization (%)',format4)

        #data section
        count_row = 2
        for row in records:
            worksheet.write(count_row,0,row[0],format4)
            worksheet.write(count_row,1,(row[1]+"%"),format4)
            worksheet.write(count_row,2,row[2],format4)
            worksheet.write(count_row,3,row[3],format4)
            worksheet.write(count_row,5,row[0],format4)
            #solving if data is not float issue
            try:
                worksheet.write(count_row,6,float(row[1]),format4)
            except:
                worksheet.write(count_row,6,"-",format4)
            count_row += 1

        #chart section
        memory_chart = workbook.add_chart({'type': 'column'})
        for i in range(3,count_row+1):
            memory_chart.add_series(
                {
                    "name" : "='Memory Summary Table'!F"+str(i),
                    "values": "='Memory Summary Table'!G"+str(i),
                    "data_labels": {"value": True},
                    }
                )
        worksheet.insert_chart('J2', memory_chart)

        #Disk Summary Table
        #sql query
        cursor.execute('''SELECT devicename, disk_name, disk_total, disk_free, disk_used, disk_percentage, disk_status FROM disktable ORDER by devicename ASC''')
        records = cursor.fetchall()
        #create sheet
        worksheet = workbook.add_worksheet('Disk Summary Table')
        
        #header section
        worksheet.merge_range(0, 0, 0, 6, 'Disk Summary Table',format1)
        worksheet.write(1,0,'Device Name',format2)
        worksheet.write(1,1,'Disk Name',format2)
        worksheet.write(1,2,'Disk Total',format2)
        worksheet.write(1,3,'Disk Free',format2)
        worksheet.write(1,4,'Disk Used',format2)
        worksheet.write(1,5,'Disk Percentage',format2)
        worksheet.write(1,5,'Disk Status',format2)

        #Belum Merge
        #data section
        count_row = 2
        for row in records:
            worksheet.write(count_row,0,row[0],format4)
            worksheet.write(count_row,1,row[1],format4)
            worksheet.write(count_row,2,row[2],format4)
            worksheet.write(count_row,3,row[3],format4)
            worksheet.write(count_row,4,row[4],format4)
            worksheet.write(count_row,5,(row[5]+'%'),format4)
            worksheet.write(count_row,6,row[6],format4)
            count_row += 1
        
        #Scheduler Backup Summary Table
        #sql query
        cursor.execute('''SELECT devicename, schedule_backup FROM backuptable ORDER by devicename ASC''')
        records = cursor.fetchall()
        #create sheet
        worksheet = workbook.add_worksheet('Scheduler Backup Summary Table')
        
        #header section
        worksheet.merge_range(0, 0, 0, 3, 'Scheduler Backup Summary Table',format1)
        worksheet.write(1,0,'Device Name',format2)
        worksheet.write(1,1,'Scheduled Backup',format2)
        worksheet.write(1,2,'Manual Backup During PM',format2)
        worksheet.write(1,3,'Manual Export CDR During PM',format2)

        #data section
        count_row = 2
        for row in records:
            worksheet.write(count_row,0,row[0],format4)
            worksheet.write(count_row,1,row[1],format4)
            worksheet.write(count_row,2,'Isi Yes/No',format4)
            worksheet.write(count_row,3,'Isi Yes/No',format4)
            count_row += 1

        #Summary of Last Backup
        #sql query
        cursor.execute('''SELECT devicename, last_date_backup, type_backup, status_backup, fitur_backup, failed_backup FROM backuptable ORDER by devicename ASC''')
        records = cursor.fetchall()
        #create sheet
        worksheet = workbook.add_worksheet('Summary of Last Backup')
        
        #header section
        worksheet.merge_range(0, 0, 0, 5, 'Summary of Last Backup',format1)
        worksheet.write(1,0,'Device Name',format2)
        worksheet.write(1,1,'Last Date Backup',format2)
        worksheet.write(1,2,'Type Backup',format2)
        worksheet.write(1,3,'Status Backup',format2)
        worksheet.write(1,4,'Fitur(s) Backup',format2)
        worksheet.write(1,5,'Failed Backup',format2)

        #data section
        count_row = 2
        for row in records:
            worksheet.write(count_row,0,row[0],format4)
            worksheet.write(count_row,1,row[1],format4)
            worksheet.write(count_row,2,row[2],format4)
            worksheet.write(count_row,3,row[3],format4)
            worksheet.write(count_row,4,row[4],format4)
            worksheet.write(count_row,5,row[5],format4)
            count_row += 1
        
        #Ceritificate Summary Table
        #sql query
        cursor.execute('''SELECT devicename, certificate_name, from_date, to_date, cert_method, cert_status FROM certtable ORDER by devicename ASC''')
        records = cursor.fetchall()
        #create sheet
        worksheet = workbook.add_worksheet('Ceritificate Summary Table')
        
        #header section
        worksheet.merge_range(0, 0, 0, 5, 'Ceritificate Summary Table',format1)
        worksheet.write(1,0,'Device Name',format2)
        worksheet.write(1,1,'Certificate Name',format2)
        worksheet.write(1,2,'From Date',format2)
        worksheet.write(1,3,'To Date',format2)
        worksheet.write(1,4,'Method',format2)
        worksheet.write(1,5,'Status',format2)

        #Belum Merge
        #data section
        count_row = 2
        for row in records:
            worksheet.write(count_row,0,row[0],format4)
            worksheet.write(count_row,1,row[1],format4)
            worksheet.write(count_row,2,row[2],format4)
            worksheet.write(count_row,3,row[3],format4)
            worksheet.write(count_row,4,row[4],format4)
            worksheet.write(count_row,5,row[5],format4)
            count_row += 1
        
        #Database Replication Summary Table
        #sql query
        cursor.execute('''SELECT devicename, node, replication, rep_status FROM reptable ORDER by devicename ASC''')
        records = cursor.fetchall()
        #create sheet
        worksheet = workbook.add_worksheet('Database Replication')
        
        #header section
        worksheet.merge_range(0, 0, 0, 3, 'Database Replication Summary Table',format1)
        worksheet.write(1,0,'Device Name',format2)
        worksheet.write(1,1,'Node',format2)
        worksheet.write(1,2,'Replication Code',format2)
        worksheet.write(1,3,'Status',format2)

        #Belum Merge
        #data section
        count_row = 2
        for row in records:
            worksheet.write(count_row,0,row[0],format4)
            worksheet.write(count_row,1,row[1],format4)
            worksheet.write(count_row,2,row[2],format4)
            worksheet.write(count_row,3,row[3],format4)
            count_row += 1
        
        #kondisi unutk license ccx tidak ada
        cursor.execute('''SELECT count(lic_name) FROM lictable''')
        records = cursor.fetchall()

        for row in records:
            if row[0] == 0:
                pass
            else:
                #License of Cisco Unified Contact Center Express
                #sql query
                cursor.execute('''SELECT devicename, lic_name, lic_detail, lic_count FROM lictable ORDER by devicename ASC''')
                records = cursor.fetchall()
                #create sheet
                worksheet = workbook.add_worksheet('License of CCX')
                
                #header section
                worksheet.merge_range(0, 0, 0, 3, 'License of Cisco Unified Contact Center Express',format1)
                worksheet.write(1,0,'Device Name',format2)
                worksheet.write(1,1,'License Name',format2)
                worksheet.write(1,2,'License Details',format2)
                worksheet.write(1,3,'License Count',format2)

                #Belum Merge
                #data section
                count_row = 2
                for row in records:
                    worksheet.write(count_row,0,row[0],format4)
                    worksheet.write(count_row,1,row[1],format4)
                    worksheet.write(count_row,2,row[2],format4)
                    worksheet.write(count_row,3,row[3],format4)
                    count_row += 1
        
        #Kondisi jika CUCM tidak ada
        cursor.execute('''SELECT count(devicename) FROM phonetable''')
        records = cursor.fetchall()

        for row in records:
            if row[0] == 0:
                pass

            else:
                #Device Count Summary Phone
                #sql query
                cursor.execute('''SELECT devicetype,
                                sum(case when status_phone = 'reg' then 1 else 0 end) AS Register,
                                sum(case when status_phone = 'unr' then 1 else 0 end) AS Unregister,
                                sum(case when status_phone = 'unk' then 1 else 0 end) AS Unknown,
                                sum(case when status_phone = 'rej' then 1 else 0 end) AS Rejected,
                                count(*) AS Total
                            FROM phonetable
                            GROUP BY devicetype
                            ORDER BY devicetype ASC''')
                records = cursor.fetchall()
                #create sheet
                worksheet = workbook.add_worksheet('Device Count Summary')
                
                #header section
                worksheet.merge_range(0, 0, 0, 5, 'Device Count Summary',format1)
                worksheet.write(1,0,'Device Type',format2)
                worksheet.write(1,1,'Total Registered Device',format2)
                worksheet.write(1,2,'Total Unregistered Device',format2)
                worksheet.write(1,3,'Total Unknown Device',format2)
                worksheet.write(1,4,'Total Rejected Device',format2)
                worksheet.write(1,5,'Total Device',format2)

                #data section
                list_regis = []
                list_unreg= []
                list_unk = []
                list_rej = []
                litt_total = []
                count_row = 2
                for row in records:
                    worksheet.write(count_row,0,row[0],format4)
                    worksheet.write(count_row,1,row[1],format4)
                    worksheet.write(count_row,2,row[2],format4)
                    worksheet.write(count_row,3,row[3],format4)
                    worksheet.write(count_row,4,row[4],format4)
                    worksheet.write(count_row,5,row[5],format4)

                    regis = row[1]
                    list_regis.append(regis)
                    unreg = row[2]
                    list_unreg.append(unreg)
                    unk = row[3]
                    list_unk.append(unk)
                    rej = row[4]
                    list_rej.append(rej)
                    total = row[5]
                    litt_total.append(total)

                    count_row += 1
                
                all_regis = sum(list_regis)
                all_unreg = sum(list_unreg)
                all_unk = sum(list_unk)
                all_rej = sum(list_rej)
                all_total = sum(litt_total)

                worksheet.write(count_row,0,'SUB TOTAL',format5)
                worksheet.write(count_row,1,all_regis,format5)
                worksheet.write(count_row,2,all_unreg,format5)
                worksheet.write(count_row,3,all_unk,format5)
                worksheet.write(count_row,4,all_rej,format5)
                worksheet.write(count_row,5,all_total,format5)

                #Device Firmware Information
                #sql query
                cursor.execute('''SELECT DISTINCT devicetype, firmware, latest, eos_phone FROM firmwaretable WHERE firmware != "" ORDER by devicetype ASC''')
                records = cursor.fetchall()
                #create sheet
                worksheet = workbook.add_worksheet('Device Firmware')
                
                #header section
                worksheet.merge_range(0, 0, 0, 3, 'Device Firmware Information',format1)
                worksheet.write(1,0,'Device Name',format2)
                worksheet.write(1,1,'Device Firmware',format2)
                worksheet.write(1,2,'Cisco Latest Firmware',format2)
                worksheet.write(1,3,'End of Support',format2)

                #data section
                count_row = 2
                for row in records:
                    worksheet.write(count_row,0,row[0],format4)
                    worksheet.write(count_row,1,row[1],format4)
                    worksheet.write(count_row,2,'Isi Manual',format4)
                    worksheet.write(count_row,3,'Isi Manual',format4)
                    count_row += 1
                
                #Directory Number Information Summary
                #sql query
                cursor.execute('''SELECT count(ext_phone) as DirectoryNumber,
                            sum(case when status_ext = 'Registered' then 1 else 0 end) AS Register,
                            sum(case when status_ext = 'Unregistered' then 1 else 0 end) AS Unregister
                            FROM exttable''')
                records = cursor.fetchall()
                #create sheet
                worksheet = workbook.add_worksheet('Directory Number')
                
                #header section
                worksheet.merge_range(0, 0, 0, 2, 'Directory Number Information Summary',format1)
                worksheet.write(1,0,'Total Directory Number',format2)
                worksheet.write(1,1,'Total Registered Directory Number',format2)
                worksheet.write(1,2,'Total Unregistered Directory Number',format2)

                #data section
                count_row = 2
                for row in records:
                    worksheet.write(count_row,0,row[0],format4)
                    worksheet.write(count_row,1,row[1],format4)
                    worksheet.write(count_row,2,row[2],format4)
                    count_row += 1
                
                #Device Phone Information Summary
                #sql query
                cursor.execute('''SELECT exttable.devicename, exttable.ext_phone, phonetable.desc_phone, phonetable.devicetype, exttable.status_ext
                            FROM exttable
                            INNER JOIN phonetable ON exttable.devicename=phonetable.devicename
                            ORDER BY ext_phone ASC''')
                records = cursor.fetchall()
                #create sheet
                worksheet = workbook.add_worksheet('Device Phone')
                
                #header section
                worksheet.merge_range(0, 0, 0, 4, 'Device Phone Information Summary',format1)
                worksheet.write(1,0,'Device Name',format2)
                worksheet.write(1,1,'Directoy Number',format2)
                worksheet.write(1,2,'Description',format2)
                worksheet.write(1,3,'Device Type',format2)
                worksheet.write(1,4,'Status',format2)

                #data section
                count_row = 2
                for row in records:
                    worksheet.write(count_row,0,row[0],format4)
                    worksheet.write(count_row,1,row[1],format4)
                    worksheet.write(count_row,2,row[2],format4)
                    worksheet.write(count_row,3,row[3],format4)
                    worksheet.write(count_row,4,row[4],format4)
                    count_row += 1
        
        #Kondisi jika CCX tidak ada
        cursor.execute('''SELECT count(triger_no) FROM ccxtrigertable''')
        records = cursor.fetchall()

        for row in records:
            if row[0] == 0:
                pass
            else:
                #Application Count Summary
                #sql query
                cursor.execute('''SELECT DISTINCT devicename, count(DISTINCT app_name) as Total FROM ccxapptable''')
                records = cursor.fetchall()
                #create sheet
                worksheet = workbook.add_worksheet('Application Count Summary')
                
                #header section
                worksheet.merge_range(0, 0, 0, 1, 'Application Count Summary',format1)
                worksheet.write(1,0,'Device Name',format2)
                worksheet.write(1,1,'Total Aplication Management',format2)

                #Belum Merge
                #data section
                count_row = 2
                for row in records:
                    worksheet.write(count_row,0,row[0],format4)
                    worksheet.write(count_row,1,row[1],format4)
                    count_row += 1
                
                #Application Information
                #sql query
                cursor.execute('''SELECT DISTINCT devicename, app_name, app_enable, app_ses FROM ccxapptable ORDER by devicename, app_name ASC''')
                records = cursor.fetchall()
                #create sheet
                worksheet = workbook.add_worksheet('Application Information')
                
                #header section
                worksheet.merge_range(0, 0, 0, 3, 'Application Information',format1)
                worksheet.write(1,0,'Device Name',format2)
                worksheet.write(1,1,'Application Name',format2)
                worksheet.write(1,2,'Status Enable',format2)
                worksheet.write(1,3,'Number of Session',format2)

                #Belum Merge
                #data section
                count_row = 2
                for row in records:
                    worksheet.write(count_row,0,row[0],format4)
                    worksheet.write(count_row,1,row[1],format4)
                    worksheet.write(count_row,2,row[2],format4)
                    worksheet.write(count_row,3,row[3],format4)
                    count_row += 1
                
                #Prompt Count Summary
                #sql query
                cursor.execute('''SELECT DISTINCT devicename, count(DISTINCT promp_name) as Total FROM ccxpromptable''')
                records = cursor.fetchall()
                #create sheet
                worksheet = workbook.add_worksheet('Prompt Count Summary')
                
                #header section
                worksheet.merge_range(0, 0, 0, 1, 'Prompt Count Summary',format1)
                worksheet.write(1,0,'Device Name',format2)
                worksheet.write(1,1,'Total Prompt Management',format2)

                #Belum Merge
                #data section
                count_row = 2
                for row in records:
                    worksheet.write(count_row,0,row[0],format4)
                    worksheet.write(count_row,1,row[1],format4)
                    count_row += 1
                
                #Prompt Information Summary
                #sql query
                cursor.execute('''SELECT DISTINCT devicename, promp_name FROM ccxpromptable ORDER by devicename, promp_name ASC''')
                records = cursor.fetchall()
                #create sheet
                worksheet = workbook.add_worksheet('Prompt Information Summary')
                
                #header section
                worksheet.merge_range(0, 0, 0, 1, 'Prompt Information Summary',format1)
                worksheet.write(1,0,'Device Name',format2)
                worksheet.write(1,1,'Prompt Name',format2)

                #Belum Merge
                #data section
                count_row = 2
                for row in records:
                    worksheet.write(count_row,0,row[0],format4)
                    worksheet.write(count_row,1,row[1],format4)
                    count_row += 1
                
                #Script Count Summary
                #sql query
                cursor.execute('''SELECT DISTINCT devicename, count(DISTINCT script_name) as Total FROM ccxscripttable''')
                records = cursor.fetchall()
                #create sheet
                worksheet = workbook.add_worksheet('Script Count Summary')
                
                #header section
                worksheet.merge_range(0, 0, 0, 1, 'Script Count Summary',format1)
                worksheet.write(1,0,'Device Name',format2)
                worksheet.write(1,1,'Total Script Management',format2)

                #Belum Merge
                #data section
                count_row = 2
                for row in records:
                    worksheet.write(count_row,0,row[0],format4)
                    worksheet.write(count_row,1,row[1],format4)
                    count_row += 1
                
                #Script Information
                #sql query
                cursor.execute('''SELECT DISTINCT devicename, script_name FROM ccxscripttable ORDER by devicename, script_name ASC''')
                records = cursor.fetchall()
                #create sheet
                worksheet = workbook.add_worksheet('Script Information')
                
                #header section
                worksheet.merge_range(0, 0, 0, 1, 'Script Information',format1)
                worksheet.write(1,0,'Device Name',format2)
                worksheet.write(1,1,'Script Namet',format2)

                #Belum Merge
                #data section
                count_row = 2
                for row in records:
                    worksheet.write(count_row,0,row[0],format4)
                    worksheet.write(count_row,1,row[1],format4)
                    count_row += 1
                
                #Triger Count Summary
                #sql query
                cursor.execute('''SELECT DISTINCT devicename, count(DISTINCT triger_name) as Total FROM ccxtrigertable''')
                records = cursor.fetchall()
                #create sheet
                worksheet = workbook.add_worksheet('Triger Count Summary')
                
                #header section
                worksheet.merge_range(0, 0, 0, 1, 'Triger Count Summary',format1)
                worksheet.write(1,0,'Device Name',format2)
                worksheet.write(1,1,'Total Tiger Management',format2)

                #Belum Merge
                #data section
                count_row = 2
                for row in records:
                    worksheet.write(count_row,0,row[0],format4)
                    worksheet.write(count_row,1,row[1],format4)
                    count_row += 1
                
                #Triger Information
                #sql query
                cursor.execute('''SELECT DISTINCT devicename, triger_no, triger_name, triger_enable, triger_ses FROM ccxtrigertable ORDER by devicename, triger_no ASC''')
                records = cursor.fetchall()
                #create sheet
                worksheet = workbook.add_worksheet('Triger Information')
                
                #header section
                worksheet.merge_range(0, 0, 0, 4, 'Triger Information',format1)
                worksheet.write(1,0,'Device Name',format2)
                worksheet.write(1,1,'Triger Number',format2)
                worksheet.write(1,2,'Application Name',format2)
                worksheet.write(1,3,'Status Enable',format2)
                worksheet.write(1,4,'Number of Session',format2)

                #Belum Merge
                #data section
                count_row = 2
                for row in records:
                    worksheet.write(count_row,0,row[0],format4)
                    worksheet.write(count_row,1,row[1],format4)
                    worksheet.write(count_row,2,row[2],format4)
                    worksheet.write(count_row,3,row[3],format4)
                    worksheet.write(count_row,4,row[4],format4)
                    count_row += 1
        
        #Kondisi jika CUCN Tidak ada
        cursor.execute('''SELECT count(message_user) FROM cucnmessagetable''')
        records = cursor.fetchall()

        for row in records:
            if row[0] == 0:
                pass
            else:
                #Mailbox Count Summary
                #sql query
                cursor.execute('''SELECT devicename, COUNT(message_user) as user, SUM(message_inbox) as Inbox, SUM(message_delete) as Deleted, SUM(message) as Message FROM cucnmessagetable''')
                records = cursor.fetchall()
                #create sheet
                worksheet = workbook.add_worksheet('Mailbox Count Summary')
                
                #header section
                worksheet.merge_range(0, 0, 0, 4, 'Mailbox Count Summary',format1)
                worksheet.write(1,0,'Device Name',format2)
                worksheet.write(1,1,'Total User Mailbox',format2)
                worksheet.write(1,2,'Total Message Inbox',format2)
                worksheet.write(1,3,'Total Message Deleted',format2)
                worksheet.write(1,4,'Total All Message',format2)

                #Belum Merge
                #data section
                count_row = 2
                for row in records:
                    worksheet.write(count_row,0,row[0],format4)
                    worksheet.write(count_row,1,row[1],format4)
                    worksheet.write(count_row,2,row[2],format4)
                    worksheet.write(count_row,3,row[3],format4)
                    worksheet.write(count_row,4,row[4],format4)
                    count_row += 1
                
                #Mailbox Information Summary
                #sql query
                cursor.execute('''SELECT devicename, message_user, message_inbox, message_delete, message FROM cucnmessagetable ORDER by devicename, message_user ASC''')
                records = cursor.fetchall()
                #create sheet
                worksheet = workbook.add_worksheet('Mailbox Information Summary')
                
                #header section
                worksheet.merge_range(0, 0, 0, 4, 'Mailbox Information Summary',format1)
                worksheet.write(1,0,'Device Name',format2)
                worksheet.write(1,1,'User ID',format2)
                worksheet.write(1,2,'Message Inbox',format2)
                worksheet.write(1,3,'Message Deleted',format2)
                worksheet.write(1,4,'Total Message',format2)

                #Belum Merge
                #data section
                count_row = 2
                for row in records:
                    worksheet.write(count_row,0,row[0],format4)
                    worksheet.write(count_row,1,row[1],format4)
                    worksheet.write(count_row,2,row[2],format4)
                    worksheet.write(count_row,3,row[3],format4)
                    worksheet.write(count_row,4,row[4],format4)
                    count_row += 1
                
                #Non Mailbox User ID Summary
                #sql query
                cursor.execute('''SELECT devicename, userid FROM cucnuseridtable WHERE userid NOT in (SELECT message_user FROM cucnmessagetable)
                                ORDER BY devicename, userid ASC''')
                records = cursor.fetchall()
                #create sheet
                worksheet = workbook.add_worksheet('Non Mailbox User ID Summary')
                
                #header section
                worksheet.merge_range(0, 0, 0, 1, 'Non Mailbox User ID Summary',format1)
                worksheet.write(1,0,'Device Name',format2)
                worksheet.write(1,1,'User ID',format2)

                #Belum Merge
                #data section
                count_row = 2
                for row in records:
                    worksheet.write(count_row,0,row[0],format4)
                    worksheet.write(count_row,1,row[1],format4)
                    count_row += 1
                
                #Non Mailbox Count User ID Summary
                #sql query
                cursor.execute('''SELECT devicename, count(userid) FROM cucnuseridtable WHERE userid NOT in (SELECT message_user FROM cucnmessagetable)
                        GROUP by devicename ORDER by devicename ASC''')
                records = cursor.fetchall()
                #create sheet
                worksheet = workbook.add_worksheet('Non Mailbox Count User ID')
                
                #header section
                worksheet.merge_range(0, 0, 0, 1, 'Non Mailbox Count User ID Summary',format1)
                worksheet.write(1,0,'Device Name',format2)
                worksheet.write(1,1,'Total Non Mailbox User ID',format2)

                #User Count ID Summary
                #sql query
                cursor.execute('''SELECT devicename,
                        (SELECT count (message_user) FROM cucnmessagetable) as mailbox,
                        (SELECT count (userid) FROM cucnuseridtable) as total_userid,
                        (SELECT count(userid) FROM cucnuseridtable
                        WHERE userid NOT in (SELECT message_user FROM cucnmessagetable)) as nonmailboxuser
                        FROM cucnuseridtable
                GROUP by devicename''')
                records = cursor.fetchall()
                #create sheet
                worksheet = workbook.add_worksheet('User Count ID Summary')
                
                #header section
                worksheet.merge_range(0, 0, 0, 3, 'User Count ID Summary',format1)
                worksheet.write(1,0,'Device Name',format2)
                worksheet.write(1,1,'Total Mailbox User ID',format2)
                worksheet.write(1,2,'Total Non Mailbox User ID',format2)
                worksheet.write(1,3,'Total All User ID',format2)

                #Belum Merge
                #data section
                count_row = 2
                for row in records:
                    worksheet.write(count_row,0,row[0],format4)
                    worksheet.write(count_row,1,row[1],format4)
                    worksheet.write(count_row,2,row[3],format4)
                    worksheet.write(count_row,3,row[2],format4)
                    count_row += 1
        
        #Alert and Log Summary
        #sql query
        cursor.execute('''SELECT * FROM alerttable ORDER by devicename ASC''')
        records = cursor.fetchall()
        #create sheet
        worksheet = workbook.add_worksheet('Alert and Log Summary')
        
        #header section
        worksheet.merge_range(0, 0, 0, 2, 'Alert and Log Summary',format1)
        worksheet.write(1,0,'Device Name',format2)
        worksheet.write(1,1,'Alert and Log Events',format2)
        worksheet.write(1,2,'Severity',format2)

        #Belum Merge
        #data section
        count_row = 2
        for row in records:
            worksheet.write(count_row,0,row[1],format4)
            worksheet.write(count_row,1,row[2],format4)
            worksheet.write(count_row,2,'Isi Manual',format4)
            count_row += 1

        #CLOSE WORKBOOK
        workbook.close()

        #close database
        db.close()
        
        #save document
        print('')
        print('Saving Document')
        print('Document has been saved to collaboration.xlsx')