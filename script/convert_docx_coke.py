from docx import Document
from docx.oxml.shared import OxmlElement, qn
import sqlite3
import re
from docx.shared import Pt
import pkg_resources
import os
import subprocess
from sys import platform
from netoprmgr.script.convert_docx_create import convert_docx_create
from docx.enum.text import WD_LINE_SPACING
from docx.enum.text import WD_ALIGN_PARAGRAPH
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
from docx.enum.table import WD_ROW_HEIGHT
from docx.enum.table import WD_ALIGN_VERTICAL
from docx.enum.table import WD_TABLE_ALIGNMENT
from docx.shared import Inches
from docx.shared import Pt
from docx.shared import Cm
from docx.shared import RGBColor


data_path = pkg_resources.resource_filename('netoprmgr', 'static/')
data_path = os.path.join(data_path,'data/')
capture_path = pkg_resources.resource_filename('netoprmgr', 'static/')
capture_path = os.path.join(capture_path,'capture/')
main_path = pkg_resources.resource_filename('netoprmgr', '/')

docx_create = convert_docx_create()
def func_get_req(address):
    try:
        os.chdir(main_path)
        files = ['init', 'init.exe']
        for file in files:
            os.chmod(file, 0o0777)
        address = re.findall(".*(qr.*)", address)
        address = address[0]
        if platform == "win32":
            cmd = ("init ")
            qr = subprocess.check_output(cmd+address, shell=True)
        else:
            cmd = ("./init ")
            qr = subprocess.check_output(cmd+address, shell=True)

        qr = str(qr)
        qr = re.findall("b\W(.*)\Wn\W", qr)
        qr = qr[0]
        return qr
    except:
        return

def shade_cells(cells, shade):
    for cell in cells:
        tcPr = cell._tc.get_or_add_tcPr()
        tcVAlign = OxmlElement("w:shd")
        tcVAlign.set(qn("w:fill"), shade)
        tcPr.append(tcVAlign)

def set_repeat_table_header(row):
    tr = row._tr
    trPr = tr.get_or_add_trPr()
    qr45 = func_get_req(docx_create+"qr45")
    tblHeader = OxmlElement(qr45)
    tblHeader.set(qn('w:val'), "true")
    trPr.append(tblHeader)
    return row

class convert_docx_coke:
    #@staticmethod
    def __init__(self, customer, review, prepared, pic, bulan, tahun):
        #variable constructor
        self.customer = customer
        self.review = review
        self.prepared = prepared
        self.pic = pic
        self.bulan = bulan
        self.tahun = tahun

    def convert_docx_coke(self):
        print('')
        print('Processing Document')

        #open db connection
        os.chdir(capture_path)
        db = sqlite3.connect('pmdb_rs')
        cursor = db.cursor()
        #using document docx module
        report_template = open(data_path+'template_content.docx', 'rb')
        document = Document(report_template)

        red = RGBColor(255, 0, 0)

        #Software Analysis
        p = document.add_paragraph('Software Analysis')
        p.style = document.styles['Heading 1']

        p = document.add_paragraph('Software Summary')
        p.style = document.styles['Heading 2']
        p.paragraph_format.space_before = Pt(6)
        
        chg_dir = os.chdir(capture_path)
        current_dir=os.getcwd()
        img_software = (capture_path+'/Software Summary.png')
        document.add_picture(img_software, width=Cm(15))
        last_paragraph = document.paragraphs[-1] 
        last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER

        document.add_paragraph()
        p = document.add_paragraph('Software Summary Table')
        p.style = document.styles['Heading 2']

        #SOFTWARE SUMMARY TABLE
        #sql query
        cursor.execute('''SELECT version, COUNT(*), date_eos, date_last FROM swsumtable GROUP BY version''')
        records = cursor.fetchall()
        cursor.execute('''SELECT COUNT(version) FROM swsumtable''')
        total = cursor.fetchall()
        total=(str(total))
        total=re.sub("\D", "", total)
        total=int(total)
        #add to document
        table = document.add_table(rows=1, cols=5)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].width = Cm(3.4)
        hdr_cells[1].width = Cm(1.29)
        hdr_cells[2].width = Cm(2.75)
        hdr_cells[3].width = Cm(5.25)
        hdr_cells[4].width = Cm(4.31)

        hdr_cells[0].text = 'Version'
        hdr_cells[1].text = 'Total'
        hdr_cells[2].text = 'Percentage'    
        hdr_cells[3].text = 'End of Software Maintenance'
        hdr_cells[4].text = 'Last Date of Support'    
        for row in records:
            row_cells = table.add_row().cells

            row_cells[0].width = Cm(3.4)
            row_cells[1].width = Cm(1.29)
            row_cells[2].width = Cm(2.75)
            row_cells[3].width = Cm(5.25)
            row_cells[4].width = Cm(4.31)

            row_cells[0].text = (row[0])
            row_cells[1].text = str(row[1]) 
            percentage_str = (str((row[1]/total)*100))

            convert_float = float(percentage_str)
            convert_round = round(convert_float, 1)
            percentage = str(convert_round)

            row_cells[2].text = (percentage+'%')

            if row[2] == 'Not Announced':
                row_cells[3].text = 'Isi Manual (YYYY-MM-DD) - Input ke Portal Netoprmgr'
                run1 = row_cells[3].paragraphs[0].runs[0]
                run1.font.color.rgb = red
            else:
                row_cells[3].text = (row[2])

            if row[3] == 'Not Announced':
                row_cells[4].text = 'Isi Manual (YYYY-MM-DD) - Input ke Portal Netoprmgr'
                run2 = row_cells[4].paragraphs[0].runs[0]
                run2.font.color.rgb = red
            else:
                row_cells[4].text = (row[3])
        
        document.add_paragraph()
        p = document.add_paragraph('Software Table')
        p.style = document.styles['Heading 2']

        #SOFTWARE TABLE
        #sql query
        qr3 = func_get_req(docx_create+"qr3")
        cursor.execute(qr3)
        records = cursor.fetchall()
        #add to document
        table = document.add_table(rows=1, cols=5)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].width = Cm(3.44)
        hdr_cells[1].width = Cm(3.19)
        hdr_cells[2].width = Cm(6.31)
        hdr_cells[3].width = Cm(2.25)
        hdr_cells[4].width = Cm(1.75)

        hdr_cells[0].text = 'Device Name'
        hdr_cells[1].text = 'Model'
        hdr_cells[2].text = 'IOS Version'
        hdr_cells[3].text = 'Uptime'
        hdr_cells[4].text = 'Config Register'
        for row in records:
            row_cells = table.add_row().cells
            
            row_cells[0].width = Cm(3.44)
            row_cells[1].width = Cm(3.19)
            row_cells[2].width = Cm(6.31)
            row_cells[3].width = Cm(2.25)
            row_cells[4].width = Cm(1.75)

            row_cells[0].text = (row[0])
            row_cells[1].text = (row[1])
            row_cells[2].text = (row[2])
            row_cells[3].text = (row[3])
            row_cells[4].text = (row[4])

            row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

        #Software Summary Table and Software Table Explanation   
        p = document.add_paragraph('Version')
        p.style = document.styles['Title']
        p.paragraph_format.space_before = Pt(6)
        p = document.add_paragraph('Software version used by device.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Total')
        p.style = document.styles['Title']
        p = document.add_paragraph('Total count of software version.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Percentage')
        p.style = document.styles['Title']
        p = document.add_paragraph('Percentage of software version.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('End of Software Maintenance')
        p.style = document.styles['Title']
        p = document.add_paragraph('The last date that Cisco Engineering may release any software maintenance releases or bug fixes to the software product. After this date, Cisco Engineering will no longer develop, repair, maintain, or test the product software.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Last Date of Support')
        p.style = document.styles['Title']
        p = document.add_paragraph('The last date to receive service and support for the product. After this date, all support services for the product are unavailable, and the product becomes obsolete.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Device Name')
        p.style = document.styles['Title']
        p = document.add_paragraph('The device name column refers to the DNS or host name of the analyzed switch. Device names may be represented in either uppercase or lowercase, which may affect sorting.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Model')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Model column refers to the switch model type. The table is sorted by switch type, making comparisons a simple process.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('IOS Version')
        p.style = document.styles['Title']
        p = document.add_paragraph('Many customers attempt to standardize on IOS for like router platforms. Rather than attempting to do this across the network, identify key functions and features with each router and plan IOS accordingly (e.g. Core – Distribution-DLSw peering, etc.).')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Uptime')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Uptime column displays the uptime in increments of days. The information included in this column allows the customer to review routers that may have been reloaded outside of the normally scheduled change control window. It also identifies routers that are possible candidates for scheduled reloads as a means of re-capturing memory, especially if the router currently exhibits low memory conditions.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Config Register')
        p.style = document.styles['Title']
        p = document.add_paragraph('Configuration Registers should be set the same from platform to platform. This value is often times misconfigured and overlooked. The table is sorted by router platform so this value should be standardized on each platform with few exceptions. The standard Routers Config Register values are 0x10F or 0x2102. The standard Switches Config Register values are 0xF.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Software Analysis Summary')
        p.style = document.styles['Title']
        p = document.add_paragraph()
        p.style = document.styles['No Spacing']
        run = p.add_run('<Software analysis must be summarized in here>')
        font = run.font
        font.color.rgb = RGBColor(255, 0, 0)
        
        document.add_page_break()
        
        #Hardware Analysis
        p = document.add_paragraph('Hardware Analysis')
        p.style = document.styles['Heading 1']

        p = document.add_paragraph('Hardware Summary')
        p.style = document.styles['Heading 2']
        p.paragraph_format.space_before = Pt(6)

        chg_dir = os.chdir(capture_path)
        current_dir=os.getcwd()
        img_hardware = (capture_path+'/Hardware Summary.png')
        document.add_picture(img_hardware, width=Cm(15))
        last_paragraph = document.paragraphs[-1] 
        last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER

        document.add_paragraph()
        p = document.add_paragraph('Hardware Summary Table')
        p.style = document.styles['Heading 2']

        #HARDWARE SUMMARY TABLE
        #sql query
        qr4 = func_get_req(docx_create+"qr4")
        cursor.execute(qr4)
        records = cursor.fetchall()
        qr5 = func_get_req(docx_create+"qr5")
        cursor.execute(qr5)
        total = cursor.fetchall()
        total=(str(total))
        total=re.sub("\D", "", total)
        total=int(total)
        #add to document
        table = document.add_table(rows=1, cols=4)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].text = 'Model'
        hdr_cells[1].text = 'Total'
        hdr_cells[2].text = 'Percentage'
        hdr_cells[3].text = 'End of Support'

        hdr_cells[0].width = Cm(4.25)
        hdr_cells[1].width = Cm(1.69)
        hdr_cells[2].width = Cm(2.5)
        hdr_cells[3].width = Cm(4.25)

        for row in records:
            row_cells = table.add_row().cells
            row_cells[0].width = Cm(4.25)
            row_cells[1].width = Cm(1.69)
            row_cells[2].width = Cm(2.5)
            row_cells[3].width = Cm(4.25)

            row_cells[0].text = (row[0])
            row_cells[1].text = str(row[2])

            percentage_str = (str((row[2]/total)*100))

            convert_float = float(percentage_str)
            convert_round = round(convert_float, 1)
            percentage = str(convert_round)

            row_cells[2].text = (percentage+'%')

            if row[1] == 'Not Announced':
                row_cells[3].text = 'Isi Manual (YYYY-MM-DD) - Input ke Portal Netoprmgr'
                run1 = row_cells[3].paragraphs[0].runs[0]
                run1.font.color.rgb = red

            else:
                row_cells[3].text = (row[1])

        document.add_paragraph()
        
        p = document.add_paragraph('Hardware Condition Analysis')
        p.style = document.styles['Heading 2']
        #SHOW ENVIRONMENT
        #sql query
        qr6 = func_get_req(docx_create+"qr6")
        cursor.execute(qr6)
        records = cursor.fetchall()
        #add to document
        table = document.add_table(rows=1, cols=4)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)
        
        hdr_cells[0].text = 'Device Name'
        hdr_cells[1].text = 'System'
        hdr_cells[2].text = 'Item'   
        hdr_cells[3].text = 'Status'

        hdr_cells[0].width = Cm(4.25)
        hdr_cells[1].width = Cm(2.44)
        hdr_cells[2].width = Cm(7)
        hdr_cells[3].width = Cm(3.3)

        iteration_check = False
        row_check = 'ludesdeveloper'
        count_row = 1       
        for row in records:
            row_cells = table.add_row().cells
            row_cells[0].width = Cm(4.25)
            row_cells[1].width = Cm(2.44)
            row_cells[2].width = Cm(7)
            row_cells[3].width = Cm(3.3)

            if row_check == row[1]:
                pass
            else:
                row_check = row[1]
                iteration_check = False
                start_row = table.cell(count_row, 0)
            if row_check == row[1] and iteration_check == False:
                row_cells[0].text = (row[1])
                iteration_check = True
            else:
                end_row = table.cell(count_row, 0)
                merge_row = start_row.merge(end_row)

            row_cells[1].text = (row[2]) 
            row_cells[2].text = (row[3])
            row_cells[3].text = (row[4])

            row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
            row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT
            row_cells[2].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

            count_row+=1

        document.add_paragraph()
        p = document.add_paragraph('Hardware Card Table')
        p.style = document.styles['Heading 2']

        #HARDWARE CARD TABLE
        #sql query
        qr7 = func_get_req(docx_create+"qr7")
        cursor.execute(qr7)
        records = cursor.fetchall()
        table = document.add_table(rows=1, cols=6)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].text = 'Device Name'
        hdr_cells[1].text = 'Model'
        hdr_cells[2].text = 'Card'
        hdr_cells[3].text = 'Description'
        hdr_cells[4].text = 'Serial Number'
        hdr_cells[5].text = 'End of Support'
        iteration_check = False
        row_check = 'ludesdeveloper'
        count_row = 1
        for row in records:
            if row_check != row[0] and iteration_check == False:
                row_cells = table.add_row().cells
                row_cells[0].width = Cm(4.25)

                row_check = row[0]
                row_detail = row[1]
                iteration_check = False
                start_row = table.cell(count_row, 0)
                start_row1 = table.cell(count_row, 1)

                row_cells[0].text = (row[0])
                row_cells[1].text = (row[1])
                row_cells[2].text = (row[2])
                row_cells[3].text = (row[3])
                row_cells[4].text = (row[4])
                
                if row[5] == 'Not Announced':
                    row_cells[5].text = 'Isi Manual (YYYY-MM-DD) - Input ke Portal Netoprmgr'
                    run1 = row_cells[5].paragraphs[0].runs[0]
                    run1.font.color.rgb = red
                else:
                    row_cells[5].text = (row[5])

                iteration_check == True
                count_row+=1

            elif row_check in row[0] and row_detail in row[1] and iteration_check == False:
                row_cells = table.add_row().cells
                row_check = row[0]
                row_detail = row[1]
                
                row_cells[2].text = (row[2])
                row_cells[3].text = (row[3])
                row_cells[4].text = (row[4])

                if row[5] == 'Not Announced':
                    row_cells[5].text = 'Isi Manual (YYYY-MM-DD) - Input ke Portal Netoprmgr'
                    run1 = row_cells[5].paragraphs[0].runs[0]
                    run1.font.color.rgb = red
                else:
                    row_cells[5].text = (row[5])

                end_row = table.cell(count_row, 0)
                merge_row = start_row.merge(end_row)
                end_row1 = table.cell(count_row, 1)
                merge_row1 = start_row1.merge(end_row1)
                iteration_check == True
                count_row+=1

            # row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
            # row_cells[1].vertical_alignment = WD_ALIGN_VERTICAL.TOP

            row_cells[0].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT
            row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT
        
        #Hardware Summary Table, Hardware Condition Analysis and Hardware Card Table Explanation
        p = document.add_paragraph('Device Name')
        p.style = document.styles['Title']
        p.paragraph_format.space_before = Pt(6)
        p = document.add_paragraph('The device name column refers to the DNS or host name of the analyzed switch. Device names may be represented in either uppercase or lowercase, which may affect sorting.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Model')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Model column refers to the switch model type. The table is sorted by switch type, making comparisons a simple process.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Total')
        p.style = document.styles['Title']
        p = document.add_paragraph('Total count of hardware model.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Percentage')
        p.style = document.styles['Title']
        p = document.add_paragraph('Percentage of hardware model.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('End of Support')
        p.style = document.styles['Title']
        p = document.add_paragraph('The last date to receive service and support for the product. After this date, all support services for the product are unavailable, and the product becomes obsolete.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('System')
        p.style = document.styles['Title']
        p = document.add_paragraph('System refers to power supply, fan, and temperature of hardware.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Item')
        p.style = document.styles['Title']
        p = document.add_paragraph('Item refers to type or model of power supply, fan, and temperature.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Status')
        p.style = document.styles['Title']
        p = document.add_paragraph('Status refers to condition or status of power supply, fan, and temperature.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Card')
        p.style = document.styles['Title']
        p = document.add_paragraph('The card column refers to the router cards.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Description')
        p.style = document.styles['Title']
        p = document.add_paragraph('Detail description about model and card.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Serial Number')
        p.style = document.styles['Title']
        p = document.add_paragraph('The serial number column refers to the unique identifier for each card in the switch.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Hardware Analysis Summary')
        p.style = document.styles['Title']  
        
        #Konidis Hardware Analis
        qr8 = func_get_req(docx_create+"qr8")
        cursor.execute(qr8)
        records = cursor.fetchall()
        qr9 = func_get_req(docx_create+"qr9")
        cursor.execute(qr9)
        total = cursor.fetchall()
        total = (str(total))
        total = re.sub("\D", "", total)
        total = int(total)

        if len(records) == 0:
            p = document.add_paragraph()
            p.style = document.styles['No Spacing']
            run = p.add_run('<Hardware analysis must be summarized in here>')
            font = run.font
            font.color.rgb = RGBColor(255, 0, 0)

        else:
            p = document.add_paragraph('Hardware Analysis found hardware that has exceeded end of support. List of models is explained below:')
            p.style = document.styles['No Spacing']
            # add to document
            table = document.add_table(rows=1, cols=3)
            table.alignment = WD_TABLE_ALIGNMENT.CENTER
            table.allow_autofit = True
            qr46 = func_get_req(docx_create+"qr46")
            table.style = qr46
            hdr_cells = table.rows[0].cells
            set_repeat_table_header(table.rows[0])
            table.rows[0].height = Cm(1.2)

            hdr_cells[0].text = 'Model'
            hdr_cells[1].text = 'Total'
            hdr_cells[2].text = 'End of Support'

            hdr_cells[0].width = Cm(4.25)
            hdr_cells[1].width = Cm(1.69)
            hdr_cells[2].width = Cm(4.25)

            for row in records:
                row_cells = table.add_row().cells
                row_cells[0].width = Cm(4.25)
                row_cells[1].width = Cm(1.69)
                row_cells[2].width = Cm(4.25)

                row_cells[0].text = (row[0])
                row_cells[1].text = str(row[2])
                row_cells[2].text = (row[1])

        document.add_page_break()
        
        #Processor Analysis
        p = document.add_paragraph('Processor Analysis')
        p.style = document.styles['Heading 1']

        p = document.add_paragraph('CPU Summary')
        p.style = document.styles['Heading 2']
        p.paragraph_format.space_before = Pt(6)

        chg_dir = os.chdir(capture_path)
        current_dir=os.getcwd()
        img_cpu = (capture_path+'/CPU Summary.png')
        document.add_picture(img_cpu, width=Cm(15))
        last_paragraph = document.paragraphs[-1] 
        last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER

        document.add_paragraph()
        p = document.add_paragraph('CPU Summary Table')
        p.style = document.styles['Heading 2']

        #CPU SUMMARY TABLE
        #sql query

        cursor.execute('''SELECT devicename, model, topcpu FROM cpusumtable''')
        records = cursor.fetchall()
        cursor.execute('''SELECT Node, [Average CPU Load], [Peak CPU Load] from cpucoke''')
        cpusum = cursor.fetchall()
        table = document.add_table(rows=1, cols=6)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)
        

        hdr_cells[0].text = 'Device Name'
        hdr_cells[1].text = 'Model'
        hdr_cells[2].text = 'Average CPU'
        hdr_cells[3].text = 'Peak CPU'
        hdr_cells[4].text = 'Top Process'
        hdr_cells[5].text = 'Status'

        iteration_check = False
        row_check = 'ludesdeveloper'
        count_row = 1
        for row in records:
            if row_check != row[0] and iteration_check == False:
                row_cells = table.add_row().cells
                row_cells[0].width = Cm(4.25)
                # print(row[0])
                # input()
                row_check = row[0]
                row_detail = row[1]
                iteration_check = False
                start_row = table.cell(count_row, 0)
                start_row1 = table.cell(count_row, 1)

                row_cells[0].text = (row[0])
                row_cells[1].text = (row[1])
                for row2 in cpusum:
                    if row[0] in row2[0]:
                        row_cells[2].text = (row2[1])
                        row_cells[3].text = (row2[2])
                        x = row2[1].split(" ")
                        if float(x[0]) < 21:
                            row_cells[5].text = "Low"
                            
                        elif float(x[0]) < 81:
                            row_cells[5].text = "Medium"
                            
                        elif float(x[0]) > 81:
                            row_cells[5].text = "High"
                            
                        break
                    if row2[0] == 'Error':
                        row_cells[2].text = "Please upload file Report_CPU_Load.xlsx"
                        row_cells[3].text = "Please upload file Report_CPU_Load.xlsx"
                        row_cells[5].text = "Please upload file Report_CPU_Load.xlsx"
                        run1 = row_cells[2].paragraphs[0].runs[0]
                        run2 = row_cells[3].paragraphs[0].runs[0]
                        run3 = row_cells[5].paragraphs[0].runs[0]
                        run1.font.color.rgb = red
                        run2.font.color.rgb = red
                        run3.font.color.rgb = red
                    else:
                        row_cells[2].text = "Hostname Not Found in Report_CPU_Load.xlsx"
                        row_cells[3].text = "Hostname Not Found in Report_CPU_Load.xlsx"
                        row_cells[5].text = "Hostname Not Found in Report_CPU_Load.xlsx"
                        run1 = row_cells[2].paragraphs[0].runs[0]
                        run2 = row_cells[3].paragraphs[0].runs[0]
                        run3 = row_cells[5].paragraphs[0].runs[0]
                        run1.font.color.rgb = red
                        run2.font.color.rgb = red
                        run3.font.color.rgb = red
                     
                row_cells[4].text = (row[2])
                

                iteration_check == True
                count_row+=1

            # elif row_check in row[0] and row_detail in row[1] and iteration_check == False:
            #     row_cells = table.add_row().cells
            #     row_check = row[0]
            #     row_detail = row[1]
                
            #     row_cells[2].text = str(round(float(row[2]),2))+'%'

            #     try:
            #         row[3] + 'String'
            #         row_cells[3].text = str(row[3])+'%'

            #     except:
            #         row_cells[3].text = str(round(float(row[3]),2))+'%'

            #     try:
            #         row[4] + 'String'
            #         row_cells[4].text = str(row[4])+'%'

            #     except:
            #         row_cells[4].text = str(round(float(row[4]),2))+'%'
                    
            #     # if 'Member ID' in row[3] or '-' in row[3]:
            #     #     row_cells[3].text = str(row[3])
            #     # else:
            #     #     row_cells[3].text = str(round(float(row[3]),2))+'%'

            #     # if '-' in row[4]:
            #     #     row_cells[4].text = str(row[4])
            #     # else:
            #     #     row_cells[4].text = str(round(float(row[4]),2))+'%'

            #     row_cells[5].text = (row[5])
            #     row_cells[6].text = (row[6])

            #     end_row = table.cell(count_row, 0)
            #     merge_row = start_row.merge(end_row)
            #     end_row1 = table.cell(count_row, 1)
            #     merge_row1 = start_row1.merge(end_row1)
            #     iteration_check == True
            #     count_row+=1

            # row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
            # row_cells[1].vertical_alignment = WD_ALIGN_VERTICAL.TOP

            row_cells[0].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT
            row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

        # for row in records:

        #     row_cells = table.add_row().cells
        #     row_cells[0].width = Cm(3.3)
        #     row_cells[1].width = Cm(3.3)
        #     row_cells[2].width = Cm(1.8)
        #     row_cells[3].width = Cm(1.8)
        #     row_cells[4].width = Cm(1.8)
        #     row_cells[5].width = Cm(3.2)
        #     row_cells[6].width = Cm(1.8)

        #     row_cells[0].text = (row[0])
        #     row_cells[1].text = (row[1])
        #     row_cells[2].text = str(round(float(row[2]),2))+'%'

        #     if 'Member ID' in row[3]:
        #         row_cells[3].text = str(row[3])
        #     else:
        #         row_cells[3].text = str(round(float(row[3]),2))+'%'

        #     if '-' in row[4]:
        #         row_cells[4].text = str(row[4])
        #     else:
        #         row_cells[4].text = str(round(float(row[4]),2))+'%'
                
        #     row_cells[5].text = (row[5])
        #     row_cells[6].text = (row[6])

        #     row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

        #CPU Summary Table Explanation
        p = document.add_paragraph('Device Name')
        p.style = document.styles['Title']
        p.paragraph_format.space_before = Pt(6)
        p = document.add_paragraph('The device name column refers to the DNS or host name of the analyzed switch. Device names may be represented in either uppercase or lowercase, which may affect sorting.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Model')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Model column refers to the switch model type. The table is sorted by switch type, making comparisons a simple process.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Total')
        p.style = document.styles['Title']
        p = document.add_paragraph('Refers to the total CPU based upon the 5-second, and 3 days average.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Process')
        p.style = document.styles['Title']
        p = document.add_paragraph('Display number of running CPU process.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Interrupt')
        p.style = document.styles['Title']
        p = document.add_paragraph('Display number of interrupt CPU process.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Top Process')
        p.style = document.styles['Title']
        p = document.add_paragraph('Display top 3 process in descending order.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Status')
        p.style = document.styles['Title']
        p = document.add_paragraph('Display status or severity of CPU Process.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Status threshold: ')
        p.style = document.styles['No Spacing']
        p = document.add_paragraph('Low = 0-20%')
        p.style = document.styles['List Nilai']     
        p = document.add_paragraph('Medium = 21-80%')
        p.style = document.styles['List Nilai']
        p = document.add_paragraph('High = 81-100%')
        p.style = document.styles['List Nilai']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('CPU Analysis Summary')
        p.style = document.styles['Title']

        #Kondisi CPU Analis
        qr11 = func_get_req(docx_create+"qr11")
        cursor.execute(qr11)
        records = cursor.fetchall()
        if len(records) == 0:
            p = document.add_paragraph()
            p.style = document.styles['No Spacing']
            run = p.add_run('<CPU analysis summary must be analyzed in here>')
            font = run.font
            font.color.rgb = RGBColor(255, 0, 0)

        else:
            p = document.add_paragraph('Processor Analysis found '+str(len(records))+' problems on all devices.')
            p.style = document.styles['No Spacing']

            # Tabel CPU RS
            table = document.add_table(rows=1, cols=7)
            table.alignment = WD_TABLE_ALIGNMENT.CENTER
            table.allow_autofit = True
            qr46 = func_get_req(docx_create+"qr46")
            table.style = qr46
            hdr_cells = table.rows[0].cells
            set_repeat_table_header(table.rows[0])
            table.rows[0].height = Cm(1.2)

            hdr_cells[0].width = Cm(3.3)
            hdr_cells[1].width = Cm(3.3)
            hdr_cells[2].width = Cm(1.8)
            hdr_cells[3].width = Cm(1.8)
            hdr_cells[4].width = Cm(1.8)
            hdr_cells[5].width = Cm(3.2)
            hdr_cells[6].width = Cm(1.8)

            hdr_cells[0].text = 'Device Name'
            hdr_cells[1].text = 'Model'
            hdr_cells[2].text = 'Total'
            hdr_cells[3].text = 'Process'
            hdr_cells[4].text = 'Interrupt'
            hdr_cells[5].text = 'Top Process'
            hdr_cells[6].text = 'Status'

            iteration_check = False
            row_check = 'ludesdeveloper'
            count_row = 1
            for row in records:
                if row_check != row[0] and iteration_check == False:
                    row_cells = table.add_row().cells
                    row_cells[0].width = Cm(4.25)

                    row_check = row[0]
                    row_detail = row[1]
                    iteration_check = False
                    start_row = table.cell(count_row, 0)
                    start_row1 = table.cell(count_row, 1)

                    row_cells[0].text = (row[0])
                    row_cells[1].text = (row[1])
                    row_cells[2].text = str(round(float(row[2]),2))+'%'

                    if 'Member ID' in row[3] or '-' in row[3] or 'Slot' in row[3]:
                        row_cells[3].text = str(row[3])
                    else:
                        row_cells[3].text = str(round(float(row[3]),2))+'%'

                    if '-' in row[4]:
                        row_cells[4].text = str(row[4])
                    else:
                        row_cells[4].text = str(round(float(row[4]),2))+'%'
                        
                    row_cells[5].text = (row[5])
                    row_cells[6].text = (row[6])

                    iteration_check == True
                    count_row+=1

                elif row_check in row[0] and row_detail in row[1] and iteration_check == False:
                    row_cells = table.add_row().cells
                    row_check = row[0]
                    row_detail = row[1]
                    
                    row_cells[2].text = str(round(float(row[2]),2))+'%'

                    if 'Member ID' in row[3] or '-' in row[3] or 'Slot' in row[3]:
                        row_cells[3].text = str(row[3])
                    else:
                        row_cells[3].text = str(round(float(row[3]),2))+'%'

                    if '-' in row[4]:
                        row_cells[4].text = str(row[4])
                    else:
                        row_cells[4].text = str(round(float(row[4]),2))+'%'

                    row_cells[5].text = (row[5])
                    row_cells[6].text = (row[6])

                    end_row = table.cell(count_row, 0)
                    merge_row = start_row.merge(end_row)
                    end_row1 = table.cell(count_row, 1)
                    merge_row1 = start_row1.merge(end_row1)
                    iteration_check == True
                    count_row+=1

                # row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
                # row_cells[1].vertical_alignment = WD_ALIGN_VERTICAL.TOP

                row_cells[0].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT
                row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

        document.add_page_break()
        
        #Memory Analysis
        p = document.add_paragraph('Memory Analysis')
        p.style = document.styles['Heading 1']

        p = document.add_paragraph('Memory Summary')
        p.style = document.styles['Heading 2']
        p.paragraph_format.space_before = Pt(6)

        chg_dir = os.chdir(capture_path)
        current_dir=os.getcwd()
        img_memory = (capture_path+'/Memory Summary.png')
        document.add_picture(img_memory, width=Cm(15))
        last_paragraph = document.paragraphs[-1] 
        last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER

        document.add_paragraph()
        p = document.add_paragraph('Memory Summary Table')
        p.style = document.styles['Heading 2']

        #MEMORY SUMMARY TABLE
        #sql query
        qr12 = func_get_req(docx_create+"qr12")
        cursor.execute(qr12)
        records = cursor.fetchall()
        table = document.add_table(rows=1, cols=5)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].text = 'Device Name'
        hdr_cells[1].text = 'Model'
        hdr_cells[2].text = 'Processor Memory Utilization'
        hdr_cells[3].text = 'Top Process'
        hdr_cells[4].text = 'Status'

        iteration_check = False
        row_check = 'ludesdeveloper'
        count_row = 1
        for row in records:
            if row_check != row[0] and iteration_check == False:
                row_cells = table.add_row().cells
                row_cells[0].width = Cm(4.25)

                row_check = row[0]
                row_detail = row[1]
                iteration_check = False
                start_row = table.cell(count_row, 0)
                start_row1 = table.cell(count_row, 1)

                row_cells[0].text = (row[0])
                row_cells[1].text = (row[1])

                if '-' in row[2]:
                    row_cells[2].text = str(row[2])
                else:
                    row_cells[2].text = str(round(float(row[2]),2))+'%'

                    row_cells[3].text = (row[3])
                    row_cells[4].text = (row[4])

                iteration_check == True
                count_row+=1

            elif row_check in row[0] and row_detail in row[1] and iteration_check == False:
                row_cells = table.add_row().cells
                row_check = row[0]
                row_detail = row[1]

                if '-' in row[2]:
                    row_cells[2].text = str(row[2])
                else:
                    row_cells[2].text = str(round(float(row[2]),2))+'%'

                    row_cells[3].text = (row[3])
                    row_cells[4].text = (row[4])

                end_row = table.cell(count_row, 0)
                merge_row = start_row.merge(end_row)
                end_row1 = table.cell(count_row, 1)
                merge_row1 = start_row1.merge(end_row1)
                iteration_check == True
                count_row+=1

            # row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
            # row_cells[1].vertical_alignment = WD_ALIGN_VERTICAL.TOP

            row_cells[0].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT
            row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

        # for row in records:
        #     if row[2] == '-':
        #         row_cells = table.add_row().cells
        #         row_cells[0].text = (row[0])
        #         row_cells[1].text = (row[1])
        #         row_cells[2].text = (row[2])
        #         row_cells[3].text = (row[3])
        #         row_cells[4].text = (row[4])
        #     else:
        #         row_cells = table.add_row().cells
        #         row_cells[0].text = (row[0])
        #         row_cells[1].text = (row[1])
        #         row_cells[2].text = str(round(float(row[2]),2))+'%'
        #         row_cells[3].text = (row[3])
        #         row_cells[4].text = (row[4])

        #     row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT
        
        #Memory Summary Table Explanation
        p = document.add_paragraph('Device Name')
        p.style = document.styles['Title']
        p.paragraph_format.space_before = Pt(6)
        p = document.add_paragraph('The device name column refers to the DNS or host name of the analyzed switch. Device names may be represented in either uppercase or lowercase, which may affect sorting.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Model')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Model column refers to the switch model type. The table is sorted by switch type, making comparisons a simple process.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Processor Memory Utilization')
        p.style = document.styles['Title']
        p = document.add_paragraph('Processor memory or main memory stores the running configuration and routing tables. The Cisco IOS software executes from main memory.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Top Process')
        p.style = document.styles['Title']
        p = document.add_paragraph('Display top 3 process in descending order')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Status')
        p.style = document.styles['Title']
        p = document.add_paragraph('Display status or severity of Memory Process.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Status threshold: ')
        p.style = document.styles['No Spacing']
        p = document.add_paragraph('Low = 0-20%')
        p.style = document.styles['List Nilai']     
        p = document.add_paragraph('Medium = 21-80%')
        p.style = document.styles['List Nilai']
        p = document.add_paragraph('High = 81-100%')
        p.style = document.styles['List Nilai']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Memory Analysis Summary')
        p.style = document.styles['Title']

        #Kondisi Memori Analis
        qr13 = func_get_req(docx_create+"qr13")
        cursor.execute(qr13)
        records = cursor.fetchall()

        if len(records) == 0:
            p = document.add_paragraph()
            p.style = document.styles['No Spacing']
            run = p.add_run('<Memory analysis summary must be summarized in here>')
            font = run.font
            font.color.rgb = RGBColor(255, 0, 0)
        
        else:
            p = document.add_paragraph('Memory Analysis found '+str(len(records))+' problems on all devices.')
            p.style = document.styles['No Spacing']

            # Tabel Memory RS
            table = document.add_table(rows=1, cols=5)
            table.alignment = WD_TABLE_ALIGNMENT.CENTER
            table.allow_autofit = True
            qr46 = func_get_req(docx_create+"qr46")
            table.style = qr46
            hdr_cells = table.rows[0].cells
            set_repeat_table_header(table.rows[0])
            table.rows[0].height = Cm(1.2)

            hdr_cells[0].text = 'Device Name'
            hdr_cells[1].text = 'Model'
            hdr_cells[2].text = 'Processor Memory Utilization'
            hdr_cells[3].text = 'Top Process'
            hdr_cells[4].text = 'Status'

            iteration_check = False
            row_check = 'ludesdeveloper'
            count_row = 1
            for row in records:
                if row_check != row[0] and iteration_check == False:
                    row_cells = table.add_row().cells
                    row_cells[0].width = Cm(4.25)

                    row_check = row[0]
                    row_detail = row[1]
                    iteration_check = False
                    start_row = table.cell(count_row, 0)
                    start_row1 = table.cell(count_row, 1)

                    row_cells[0].text = (row[0])
                    row_cells[1].text = (row[1])

                    if '-' in row[2]:
                        row_cells[2].text = str(row[2])
                    else:
                        row_cells[2].text = str(round(float(row[2]),2))+'%'

                        row_cells[3].text = (row[3])
                        row_cells[4].text = (row[4])

                    iteration_check == True
                    count_row+=1

                elif row_check in row[0] and row_detail in row[1] and iteration_check == False:
                    row_cells = table.add_row().cells
                    row_check = row[0]
                    row_detail = row[1]

                    if '-' in row[2]:
                        row_cells[2].text = str(row[2])
                    else:
                        row_cells[2].text = str(round(float(row[2]),2))+'%'

                        row_cells[3].text = (row[3])
                        row_cells[4].text = (row[4])

                    end_row = table.cell(count_row, 0)
                    merge_row = start_row.merge(end_row)
                    end_row1 = table.cell(count_row, 1)
                    merge_row1 = start_row1.merge(end_row1)
                    iteration_check == True
                    count_row+=1

                # row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
                # row_cells[1].vertical_alignment = WD_ALIGN_VERTICAL.TOP

                row_cells[0].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT
                row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT
        
        document.add_page_break()
        
        #Log Analysis
        p = document.add_paragraph('Log Analysis')
        p.style = document.styles['Heading 1']

        p = document.add_paragraph('Log Summary Table')
        p.style = document.styles['Heading 2']
        p.paragraph_format.space_before = Pt(6)
        
        #Log Summary Table
        #sql query
        qr14 = func_get_req(docx_create+"qr14")
        cursor.execute(qr14)
        records = cursor.fetchall()
        #add to document
        table = document.add_table(rows=1, cols=3)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].width = Cm(3.7)
        hdr_cells[1].width = Cm(11.3)
        hdr_cells[2].width = Cm(2)

        hdr_cells[0].text = 'Device Name'
        hdr_cells[1].text = 'Log Event'
        hdr_cells[2].text = 'Severity'

        for row in records:
            row_cells = table.add_row().cells

            row_cells[0].width = Cm(3.7)
            row_cells[1].width = Cm(11.3)
            row_cells[2].width = Cm(2)

            row_cells[0].text = (row[0])
            row_cells[1].text = 'Isi Manual dari logresult_rs.csv'
            row_cells[2].text = 'Isi Manual - Input ke Portal Netoprmgr'

            row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
            row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

            run1 = row_cells[1].paragraphs[0].runs[0]
            run2 = row_cells[2].paragraphs[0].runs[0]

            run1.font.color.rgb = red
            run2.font.color.rgb = red

        #Log Analysis Summary Table Explanation
        p = document.add_paragraph('Device Name')
        p.style = document.styles['Title']
        p.paragraph_format.space_before = Pt(6)
        p = document.add_paragraph('The device name column refers to the DNS or host name of the analyzed switch. Device names may be represented in either uppercase or lowercase, which may affect sorting.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Log Event')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Log events refer to the logging process of the device which indicates incident or informational information. This log could be an alert for the administrator to mitigate problem.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Severity')
        p.style = document.styles['Title']
        p = document.add_paragraph('The severity tables indicate the importance of the log. High severity means it would cause downtime or disruption to network. Medium severity means this log may lead to network disruption in the future. Low severity means the logs are for information only.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Log Analysis Summary')
        p.style = document.styles['Title']
        p = document.add_paragraph()
        p.style = document.styles['No Spacing']
        run = p.add_run('<Log analysis summary must be summarized in here>')
        font = run.font
        font.color.rgb = RGBColor(255, 0, 0)
        
        #close database
        db.close()
        
        #save document
        os.chdir(capture_path)
        print('')
        print('Saving Document')
        document.save('pm_rs.docx')
        print('Document has been saved to pm_rs.docx')