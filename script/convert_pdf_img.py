from pdf2image import convert_from_path
import pkg_resources
import re
import os

class convert_pdf_img:
    @staticmethod
    def convert_pdf_img():

        capture_path = pkg_resources.resource_filename('netoprmgr', 'static/')
        capture_path = os.path.join(capture_path,'capture/')

        files = os.listdir(capture_path)
        list_name = []
        tnpa_pdf = []
        list_png = []
        for i in files:
            fel = re.findall('.*.pdf', i)
            for file in fel:
                # print(f'file {file}')
                list_name.append(file)
            fep = re.findall('(.*).pdf', i)
            for namef in fep:
                tnpa_pdf.append(namef)

        tot = len(list_name)
        for p in range(tot):
            pathpdf = capture_path+list_name[p]
            pathno = capture_path+tnpa_pdf[p]
            print('===============')
            print('Precessing file '+list_name[p])
            print('===============')
            images = convert_from_path(pathpdf)
            
            for index, image in enumerate(images):
                image.save(f'{pathno}-{index}.png', size=10)
                print(f'PDF to PNG {tnpa_pdf[p]}-{index}.png Done')
                # print(f'{tnpa_pdf[p]}{index}.png')
                list_png.append(f'{tnpa_pdf[p]}{index}.png')