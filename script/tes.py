import os
import pkg_resources
import xlrd

data_path = pkg_resources.resource_filename('netoprmgr', 'static/')
data_path = os.path.join(data_path,'data/')

chg_dir = os.chdir(data_path)
current_dir=os.getcwd()
support_dir = (data_path+'/support_devices.xlsx')
book = xlrd.open_workbook(support_dir)
first_sheet = book.sheet_by_index(0)
cell = first_sheet.cell(0,0)

PID = []
Script = []
count = 1
for i in range(first_sheet.nrows):
    try:
        PID.append(first_sheet.row_values(count)[0])
        Script.append(first_sheet.row_values(count)[1])
        count +=1
    except:
        pass

print (PID)
print (Script)