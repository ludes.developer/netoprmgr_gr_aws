import re
from docx import Document

class get_log:
    def __init__(self,file,month_list,year):
        self.file=file
        self.month_list=month_list
        self.year = year

    def get_log(self):
        list_hostname = []
        list_log = []
        list_severity_value = []
        check_log = 'False'

        try:
            print('')
            print('Processing File :')
            print(self.file)
            read_file = open(self.file, 'r')
            read_file_list = read_file.readlines()
            
            for file in read_file_list:
                if re.findall('^hostname (.*)', file):
                    hostname = re.findall('^hostname (.*)', file)
                    hostname = hostname[0]
                    
                elif re.findall('^switchname (.*)', file):
                    hostname = re.findall('^switchname (.*)', file)
                    hostname = hostname[0]
                    break
            
            log_break = False
            for file in read_file_list:
                #Check if log has severity below 4
                for month in self.month_list:
                    #condition has month only
                    if re.findall('^.*('+month+'\s+\d+\s+\d+:.*-[0,1,2,3,4]-.*)', file):
                        log_break = True
                        log = re.findall('^.*('+month+'\s+\d+\s+\d+:.*-[0,1,2,3,4]-.*)', file)
                        log = log[0]
                        severity_value = re.findall('-(\d+)-', file)
                        severity_value = severity_value[0]
                        list_hostname.append(hostname)
                        list_log.append(log)
                        list_severity_value.append(severity_value)
                        check_log = 'True'
                    #condition has both month and year
                    elif re.findall('^.*('+month+'\s+\d+\s+'+self.year+'\s+.*-[0,1,2,3,4]-.*)',file):
                        log_break = True
                        log = re.findall('^.*('+month+'\s+\d+\s+'+self.year+'\s+.*-[0,1,2,3,4]-.*)', file)
                        log = log[0]
                        severity_value = re.findall('-(\d+)-', file)
                        severity_value = severity_value[0]
                        list_hostname.append(hostname)
                        list_log.append(log)
                        list_severity_value.append(severity_value)
                        check_log = 'True'
                    elif re.findall('\S+\s+\d+\s+.*%.*-\d+-',file):
                        log_break = True
                    #break loop
                    elif log_break == True and re.findall('.*#',file):
                        break
                    elif log_break == True and re.findall('^\s*$',file):
                        break
            
            #Check if log has not severity below 4
            if check_log == 'False':
                list_hostname.append(hostname)
                list_log.append('-')
                list_severity_value.append('-')
            
            check_log = 'False'    

        #except NameError:
            #raise
        except:
            pass
        return list_hostname, list_log, list_severity_value

