import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt; plt.rcdefaults()
from matplotlib import pyplot as plt
import matplotlib.cm as cm
from matplotlib.colors import Normalize
import matplotlib.patches as mpatches
from numpy.random import rand
import numpy as np
import sqlite3
import re
import pkg_resources
import os
import xlrd

class convert_chart_f5:
    @staticmethod
    def convert_chart_f5():
        #open db connection
        db = sqlite3.connect('pmdb_f5')
        cursor = db.cursor()
        
        #SOFTWARE SUMMARY
        #sql query
        cursor.execute('''SELECT version, COUNT(*) FROM swsumtable GROUP BY version''')
        count_version = cursor.fetchall()
        cursor.execute('''SELECT COUNT(version) FROM swsumtable''')
        total = cursor.fetchall()
        total=(str(total))
        total=re.sub("\D", "", total)
        total=int(total)
        #create list
        percentage=[]
        labels=[]
        for row in count_version:
            try:
                percentage.append((row[1]/int(total))*100)
                labels.append(row[0])
            except:
                pass

        plt.style.use("fivethirtyeight")

        if len(percentage) == 1:
            font = {'family' : 'DejaVu Sans',
                    'weight' : 'bold',
                    'size'   : 12}
            plt.rc('font', **font)

            wedges, text, autotexts = plt.pie(percentage, labels=labels, pctdistance=0, labeldistance=-0.2, textprops=dict(color="w"), autopct='%1.1f%%', shadow=True, startangle=90, radius=1)
            plt.setp(autotexts, size=12)
            for t in text:
                t.set_horizontalalignment('center')
            plt.title('Software Utilization')
            plt.axis('equal')
            plt.tight_layout()

        elif len(percentage) < 25:
            font = {'family' : 'DejaVu Sans',
                    'weight' : 'normal',
                    'size'   : 12}
            plt.rc('font', **font)

            wedges, text, autotexts = plt.pie(percentage, pctdistance=0.9, textprops=dict(color="w"), autopct='%1.1f%%', shadow=True, startangle=90, radius=2)
            plt.setp(autotexts, size=10)
            plt.legend(labels, bbox_to_anchor=(1.6,1.3), loc="upper center")
            plt.title('Software Utilization', pad=100.0)
        else:
            font = {'family' : 'DejaVu Sans',
                    'weight' : 'normal',
                    'size'   : 12}
            plt.rc('font', **font)

            wedges, text, autotexts = plt.pie(percentage, pctdistance=0.9, textprops=dict(color="w"), autopct='%1.1f%%', shadow=True, startangle=90, radius=3)
            plt.setp(autotexts, size=10)
            plt.legend(labels, bbox_to_anchor=(0.5,-1.1), loc="lower center", ncol=5)
            plt.title('Software Utilization', pad=220.0)
        #save image
        print('Saving Image')
        plt.savefig('Software Summary F5.png', bbox_inches='tight')
        plt.close()
        print('Image saved to Software Summary F5.png')

        #HARDWARE SUMMARY
        #sql query
        cursor.execute('''SELECT model, COUNT(*) FROM hwsumtable GROUP BY model''')
        count_model = cursor.fetchall()
        cursor.execute('''SELECT COUNT(model) FROM hwsumtable''')
        total = cursor.fetchall()
        total=(str(total))
        total=re.sub("\D", "", total)
        total=int(total)
        #create list
        percentage=[]
        labels=[]
        for row in count_model:
            try:
                percentage.append((row[1]/int(total))*100)
                labels.append(row[0])
            except:
                pass

        plt.style.use("fivethirtyeight")

        if len(percentage) == 1:
            font = {'family' : 'DejaVu Sans',
                    'weight' : 'bold',
                    'size'   : 12}
            plt.rc('font', **font)

            wedges, text, autotexts = plt.pie(percentage, labels=labels, pctdistance=0, labeldistance=-0.2, textprops=dict(color="w"), autopct='%1.1f%%', shadow=True, startangle=90, radius=1)
            plt.setp(autotexts, size=12)
            for t in text:
                t.set_horizontalalignment('center')
            plt.title('Hardware Utilization')
            plt.axis('equal')
            plt.tight_layout()

        elif len(percentage) < 25:
            font = {'family' : 'DejaVu Sans',
                    'weight' : 'normal',
                    'size'   : 12}
            plt.rc('font', **font)

            wedges, text, autotexts = plt.pie(percentage, pctdistance=0.9, textprops=dict(color="w"), autopct='%1.1f%%', shadow=True, startangle=90, radius=2)
            plt.setp(autotexts, size=10)
            plt.legend(labels, bbox_to_anchor=(1.6,1.3), loc="upper center")
            plt.title('Hardware Utilization', pad=100.0)
        else:
            font = {'family' : 'DejaVu Sans',
                    'weight' : 'normal',
                    'size'   : 12}
            plt.rc('font', **font)

            wedges, text, autotexts = plt.pie(percentage, pctdistance=0.9, textprops=dict(color="w"), autopct='%1.1f%%', shadow=True, startangle=90, radius=3)
            plt.setp(autotexts, size=10)
            plt.legend(labels, bbox_to_anchor=(0.5,-1.1), loc="lower center", ncol=5)
            plt.title('Hardware Utilization', pad=220.0)
        #save image
        print('Saving Image')
        plt.savefig('Hardware Summary F5.png', bbox_inches='tight')
        plt.close()
        print('Image saved to Hardware Summary F5.png')


        #CPU Summary
        #sql query
        cursor.execute('''SELECT devicename, cpu_current FROM cputable''')
        cpu = cursor.fetchall()
        cursor.execute('''SELECT COUNT(devicename) FROM cputable''')
        total = cursor.fetchall()
        total = total[0]
        total = total [0]
        #create list
        devicename=[]
        cpu_total=[]
        for row in cpu:
            try:
                cpu_total.append(float(row[1]))
                devicename.append(row[0])
            except:
                pass
        #create chart
        data = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple']

        my_cmap = cm.get_cmap('jet')
        my_norm = Normalize(vmin=0, vmax=int(total))
        plt.style.use('ggplot')
        plt.ylim(0, 100)
        x_pos = [i for i, _ in enumerate(devicename)]
        if len(devicename) == 1:
            plt.bar(x_pos, cpu_total, color=data)
            plt.title("CPU Utilization")
            plt.xticks([])
        else:
            plt.bar(x_pos, cpu_total, color=data, width=(x_pos[1]-x_pos[0])*1)
            plt.title("CPU Utilization")
            # plt.xticks(x_pos, devicename, rotation='vertical')
            plt.xticks([])

        #new
        list_patch = []
        count_color = 0
        for name in devicename:
            list_patch.append(mpatches.Patch(color=data[count_color], label=name))
            count_color += 1
            if count_color > 4:
                count_color = 0

        plt.legend(handles=list_patch,bbox_to_anchor=(1, 1), loc=2,)

        #save image
        print('Saving Image')
        plt.savefig('CPU Summary F5.png', bbox_inches='tight')
        plt.close()
        print('Image saved to CPU Summary F5.png')

        #Memory Summary
        #sql query
        cursor.execute('''SELECT memory_name, memory_current FROM memtable''')
        memory = cursor.fetchall()
        cursor.execute('''SELECT COUNT(memory_name) FROM memtable''')
        total = cursor.fetchall()
        total = total[0]
        total = total [0]
        #create list
        devicename=[]
        memory_total=[]
        for row in memory:
            try:
                memory_total.append(float(row[1]))
                devicename.append(row[0])
            except:
                pass
        #create chart
        data = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple']

        my_cmap = cm.get_cmap('jet')
        my_norm = Normalize(vmin=0, vmax=int(total))
        plt.style.use('ggplot')
        plt.ylim(0, 100)
        x_pos = [i for i, _ in enumerate(devicename)]
        if len(devicename) == 1:
            plt.bar(x_pos, memory_total, color=data)
            plt.title("Memory Utilization")
            plt.xticks([])
        else:
            plt.bar(x_pos, memory_total, color=data, width=(x_pos[1]-x_pos[0])*1)
            plt.title("Memory Utilization")
            # plt.xticks(x_pos, devicename, rotation='vertical')
            plt.xticks([])

        #new
        list_patch = []
        count_color = 0
        for name in devicename:
            list_patch.append(mpatches.Patch(color=data[count_color], label=name))
            count_color += 1
            if count_color > 4:
                count_color = 0

        plt.legend(handles=list_patch,bbox_to_anchor=(1, 1), loc=2,)

        #save image
        print('Saving Image')
        plt.savefig('Memory Summary F5.png', bbox_inches='tight')
        plt.close()
        print('Image saved to Memory Summary F5.png')

        #close database
        db.close()


