import sqlite3

class dbreport_cover:
    def __init__(self):
        #destroy table summarytable
        try:
            db = sqlite3.connect('pmdb_cover')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE swtable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_cover')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE tipetable''')
            db.commit()
            db.close()
        except:
            pass
        
        #open db connection to table summary table
        try:
            db = sqlite3.connect('pmdb_cover')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE swtable(id INTEGER PRIMARY KEY, devicename TEXT,
                model TEXT, iosversion TEXT, uptime TEXT, confreg TEXT, version TEXT)                    
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_cover')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE tipetable(id INTEGER PRIMARY KEY, tipe TEXT)
            ''')
            db.close()
        except:
            pass
