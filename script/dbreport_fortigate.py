import sqlite3

class dbreport_fortigate:
    def __init__(self):
        #DESTROY pmdb_fortigate
        try:
            db = sqlite3.connect('pmdb_fortigate')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE swtable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_fortigate')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE swsumtable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_fortigate')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE systematable ''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_fortigate')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE hatable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_fortigate')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE cputable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_fortigate')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE memtable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_fortigate')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE sessiontable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_fortigate')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE lictable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_fortigate')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE certtable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_fortigate')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE tipetable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_fortigate')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE logtable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_fortigate')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE portaltable''')
            db.commit()
            db.close()
        except:
            pass
        
        #OPEN DB PMDB_FORTIGATE
        try:
            db = sqlite3.connect('pmdb_fortigate')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE swtable(id INTEGER PRIMARY KEY, devicename TEXT,
                model TEXT, iosversion TEXT, uptime TEXT, confreg TEXT, version TEXT, date_eos TEXT, date_last TEXT)                     
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_fortigate')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE swsumtable(id INTEGER PRIMARY KEY, version TEXT, date_eos TEXT, date_last TEXT)                    
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_fortigate')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE systematable(id INTEGER PRIMARY KEY, devicename TEXT, system TEXT, system_value TEXT)                    
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_fortigate')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE hatable(id INTEGER PRIMARY KEY, devicename TEXT, ha TEXT, ha_value TEXT)                 
            ''')
            db.close()
        except:
            pass      
        try:
            db = sqlite3.connect('pmdb_fortigate')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE cputable(id INTEGER PRIMARY KEY, devicename TEXT, cpu_state TEXT, cpu_user TEXT,
                cpu_system TEXT, cpu_nice TEXT, cpu_idle TEXT, cpu_iowait TEXT, cpu_irq TEXT, cpu_softirq TEXT, cpu_status TEXT)                    
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_fortigate')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE memtable(id INTEGER PRIMARY KEY, devicename TEXT, memory_used TEXT,
                memory_free TEXT, memory_freeable TEXT, memory_status TEXT)                    
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_fortigate')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE sessiontable(id INTEGER PRIMARY KEY, devicename TEXT, session_type TEXT,
                session_1 TEXT, session_10 TEXT, session_30 TEXT)                
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_fortigate')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE lictable(id INTEGER PRIMARY KEY, devicename TEXT, lic_entitlement TEXT,
                lic_contract_exp TEXT, lic_last_update_schedule TEXT, lic_last_update_attempt TEXT, lic_result TEXT, lic_status TEXT)                
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_fortigate')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE certtable(id INTEGER PRIMARY KEY, devicename TEXT, cert_name TEXT,
                cert_subject TEXT, cert_issuer TEXT, cert_validto TEXT, cert_status TEXT)                
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_fortigate')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE tipetable(id INTEGER PRIMARY KEY, tipe TEXT)
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_fortigate')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE logtable(id INTEGER PRIMARY KEY, devicename TEXT, model TEXT, script TEXT)
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_fortigate')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE portaltable(id INTEGER PRIMARY KEY, devicename TEXT, model TEXT, card TEXT, sn TEXT, version TEXT)
            ''')
            db.close()
        except:
            pass

