import sqlite3

class insert_dbreport_cover:
    def __init__(self, devicename, model, iosversion, uptime, confreg, version,
                tipe_list,
                file_name
    ):
        self.devicename = devicename
        self.model = model
        self.iosversion = iosversion
        self.uptime = uptime
        self.confreg = confreg
        self.version = version
        self.tipe_list = tipe_list
        self.file_name = file_name
        
    def insert_dbreport_cover(self):
        #open db connection
        db = sqlite3.connect('pmdb_cover')
        cursor = db.cursor()
        #db software
        try:
            cursor.execute('''INSERT INTO swtable(devicename, model, iosversion, uptime, confreg, version)
                    VALUES(?,?,?,?,?,?)''', (self.devicename, self.model, self.iosversion, self.uptime, self.confreg, self.version))
        except:
            cursor.execute('''INSERT INTO swtable(devicename, model, iosversion, uptime, confreg, version)
                    VALUES(?,?,?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))
        
        #db tipe
        try:
            for enum, tipe in enumerate(self.tipe_list):
                cursor.execute('''INSERT INTO tipetable(tipe)
                        VALUES(?)''', (tipe,))
        except:
            cursor.execute('''INSERT INTO tipetable(tipe)
                    VALUES(?)''', (self.file_name+'-'+'error',))
        db.commit()             
        db.close()