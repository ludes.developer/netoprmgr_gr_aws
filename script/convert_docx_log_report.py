from docx import Document
from docx.oxml.shared import OxmlElement, qn
import sqlite3
import re
from docx.shared import Pt
import pkg_resources
import os
import subprocess
from sys import platform
from netoprmgr.script.convert_docx_create import convert_docx_create
from docx.enum.text import WD_LINE_SPACING
from docx.enum.text import WD_ALIGN_PARAGRAPH
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
from docx.enum.table import WD_ROW_HEIGHT
from docx.shared import Inches
from docx.shared import Pt
from docx.shared import Cm
from docx.shared import RGBColor

data_path = pkg_resources.resource_filename('netoprmgr', 'static/')
data_path = os.path.join(data_path, 'data/')
capture_path = pkg_resources.resource_filename('netoprmgr', 'static/')
capture_path = os.path.join(capture_path,'capture/')
main_path = pkg_resources.resource_filename('netoprmgr', '/')

docx_create = convert_docx_create()
def func_get_req(address):
    try:
        os.chdir(main_path)
        files = ['init', 'init.exe']
        for file in files:
            os.chmod(file, 0o0777)
        address = re.findall(".*(qr.*)", address)
        address = address[0]
        if platform == "win32":
            cmd = ("init ")
            qr = subprocess.check_output(cmd+address, shell=True)
        else:
            cmd = ("./init ")
            qr = subprocess.check_output(cmd+address, shell=True)

        qr = str(qr)
        qr = re.findall("b\W(.*)\Wn\W", qr)
        qr = qr[0]
        return qr
    except:
        return

def shade_cells(cells, shade):
    for cell in cells:
        tcPr = cell._tc.get_or_add_tcPr()
        tcVAlign = OxmlElement("w:shd")
        tcVAlign.set(qn("w:fill"), shade)
        tcPr.append(tcVAlign)

def set_repeat_table_header(row):
    tr = row._tr
    trPr = tr.get_or_add_trPr()
    qr45 = func_get_req(docx_create+"qr45")
    tblHeader = OxmlElement(qr45)
    tblHeader.set(qn('w:val'), "true")
    trPr.append(tblHeader)
    return row

class convert_docx_log_report:
    # @staticmethod
    # def __init__(self):
    #     #variable constructor

    def convert_docx_log_report(self):
        print('')
        print('Processing Document')
        os.chdir(capture_path)

        # using document docx module
        report_template = open(data_path+'template_content.docx', 'rb')
        document = Document(report_template)

        all_file_dir = os.listdir()
        all_devicename = []
        all_model = []
        all_script = []
        for file in all_file_dir:
            if 'pmdb' in file:
                db = sqlite3.connect(file)
                cursor = db.cursor()

                # SQL QUERY TIPE TABLE
                cursor.execute(
                    '''SELECT devicename, model, script FROM logtable ORDER by devicename ASC''')
                records = cursor.fetchall()

                for record in records:
                    all_devicename.append(record[0])
                    all_model.append(record[1])
                    all_script.append(record[2])

                # CLOSE DB
                db.close()

        # tulis = input(all_devicename)
        # tulis = input(all_model)
        # tulis = input(all_script)

        # Log Generate Report
        p = document.add_paragraph('Log Generate Report')
        p.style
        p.style.name
        'Normal'
        p.style = document.styles['Heading 1']
        p.style.name
        'Heading 1'
        p = document.add_paragraph('')

        p = document.add_paragraph('Log Generate Report Checking Table')
        p.style
        p.style.name
        'Normal'
        p.style = document.styles['Heading 2']
        p.style.name
        'Heading 2'
        # Log Generate Report Table
        # add to document
        table = document.add_table(rows=1, cols=4)
        table.allow_autofit = True
        qr52 = func_get_req(docx_create+"qr52")
        table.style = qr52
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].text = 'No'
        hdr_cells[1].text = 'Device Name'
        hdr_cells[2].text = 'Model'
        hdr_cells[3].text = 'Script'

        hdr_cells[0].width = Cm(1.20)
        hdr_cells[1].width = Cm(7.30)
        hdr_cells[2].width = Cm(4.25)
        hdr_cells[3].width = Cm(4.25)

        for enum, name in enumerate(all_devicename):
            row_cells = table.add_row().cells
            row_cells[0].width = Cm(1.20)
            row_cells[1].width = Cm(7.30)
            row_cells[2].width = Cm(4.25)
            row_cells[3].width = Cm(4.25)

            row_cells[0].text = (str(enum+1))
            row_cells[1].text = (name)
            row_cells[2].text = (all_model[enum])
            row_cells[3].text = (all_script[enum])

        # save document
        os.chdir(capture_path)
        print('')
        print('Saving Document')
        document.save('log_generate_report.docx')
        print('Document has been saved to log_generate_report.docx')
