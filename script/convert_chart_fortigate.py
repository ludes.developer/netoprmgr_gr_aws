import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt; plt.rcdefaults()
from matplotlib import pyplot as plt
import matplotlib.cm as cm
from matplotlib.colors import Normalize
import matplotlib.patches as mpatches
from numpy.random import rand
import numpy as np
import sqlite3
import re
import pkg_resources
import os
import xlrd

class convert_chart_fortigate:
    @staticmethod
    def convert_chart_fortigate():
        #open db connection
        db = sqlite3.connect('pmdb_fortigate')
        cursor = db.cursor()

        #CPU Summary
        #sql query
        cursor.execute('''SELECT devicename, cpu_state, cpu_idle FROM cputable''')
        cpu = cursor.fetchall()
        cursor.execute('''SELECT COUNT(cpu_state) FROM cputable''')
        total = cursor.fetchall()
        total = total[0]
        total = total [0]
        #create list
        devicename=[]
        cpu_total=[]
        for row in cpu:
            try:
                cpu_total.append(100-float(row[2]))
                devicename.append(row[0]+"-"+row[1])
            except:
                pass
        #create chart
        data = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple']

        my_cmap = cm.get_cmap('jet')
        my_norm = Normalize(vmin=0, vmax=int(total))
        plt.style.use('ggplot')
        plt.ylim(0, 100)
        x_pos = [i for i, _ in enumerate(devicename)]
        if len(devicename) == 1:
            plt.bar(x_pos, cpu_total, color=data)
            plt.title("CPU Utilization")
            plt.xticks([])
        else:
            plt.bar(x_pos, cpu_total, color=data, width=(x_pos[1]-x_pos[0])*1)
            plt.title("CPU Utilization")
            # plt.xticks(x_pos, devicename, rotation='vertical')
            plt.xticks([])

        #new
        list_patch = []
        count_color = 0
        for name in devicename:
            list_patch.append(mpatches.Patch(color=data[count_color], label=name))
            count_color += 1
            if count_color > 4:
                count_color = 0

        plt.legend(handles=list_patch,bbox_to_anchor=(1, 1), loc=2,)

        #save image
        print('Saving Image')
        plt.savefig('CPU Summary FortiGate.png', bbox_inches='tight')
        plt.close()
        print('Image saved to CPU Summary FortiGate.png')

        #Memory Summary
        #sql query
        cursor.execute('''SELECT devicename, memory_used FROM memtable''')
        memory = cursor.fetchall()
        cursor.execute('''SELECT COUNT(devicename) FROM memtable''')
        total = cursor.fetchall()
        total = total[0]
        total = total [0]
        #create list
        devicename=[]
        memory_total=[]
        for row in memory:
            try:
                memory_total.append(float(row[1]))
                devicename.append(row[0])
            except:
                pass
        #create chart
        data = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple']

        my_cmap = cm.get_cmap('jet')
        my_norm = Normalize(vmin=0, vmax=int(total))
        plt.style.use('ggplot')
        plt.ylim(0, 100)
        x_pos = [i for i, _ in enumerate(devicename)]
        if len(devicename) == 1:
            plt.bar(x_pos, memory_total, color=data)
            plt.title("Memory Utilization")
            plt.xticks([])
        else:
            plt.bar(x_pos, memory_total, color=data, width=(x_pos[1]-x_pos[0])*1)
            plt.title("Memory Utilization")
            # plt.xticks(x_pos, devicename, rotation='vertical')
            plt.xticks([])

        #new
        list_patch = []
        count_color = 0
        for name in devicename:
            list_patch.append(mpatches.Patch(color=data[count_color], label=name))
            count_color += 1
            if count_color > 4:
                count_color = 0

        plt.legend(handles=list_patch,bbox_to_anchor=(1, 1), loc=2,)

        #save image
        print('Saving Image')
        plt.savefig('Memory Summary FortiGate.png', bbox_inches='tight')
        plt.close()
        print('Image saved to Memory Summary FortiGate.png')

        #close database
        db.close()


