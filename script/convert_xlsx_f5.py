import sqlite3
import re
import pkg_resources
import os
import xlsxwriter

class convert_xlsx_f5:
    @staticmethod
    def convert_xlsx_f5():
        result_path = pkg_resources.resource_filename('netoprmgr', 'static/')
        result_path = os.path.join(result_path,'result/')

        print('')
        print('Processing Document')

        #open db connection
        db = sqlite3.connect('pmdb_f5')
        cursor = db.cursor()

        #BUG SUMMARY
        #sql query
        cursor.execute('''SELECT model, version FROM swtable ORDER BY model, version ASC''')
        records = cursor.fetchall()
        #define workbook
        workbook = xlsxwriter.Workbook('f5.xlsx')

        #format title
        format1 = workbook.add_format({
                                'font_name' : 'Arial',
                                'font_size' : 12,
                                'bold' : 1,
                                'text_wrap': True
                                })
        #format header table
        format2 = workbook.add_format({
                                'font_name' : 'Arial',
                                'font_size' : 11,
                                'bold' : 1,
                                'border': 1,
                                'align' : 'center',
                                'valign' : 'vcenter',
                                'fg_color' : '#00659C',
                                'font_color': 'white',
                                'text_wrap': True
                                })
        #format number header
        format3 = workbook.add_format({
                                'font_name' : 'Arial',
                                'font_size' : 10,
                                'bold' : 1,
                                'border': 1,
                                'align' : 'center',
                                'valign' : 'vcenter',
                                'fg_color' : '#00659C',
                                'font_color': 'white',
                                'text_wrap': True
                                })
        #format data table
        format4 = workbook.add_format({
                                'font_name' : 'Arial',
                                'font_size' : 10,
                                'border': 1,
                                'align' : 'center',
                                'valign' : 'vcenter',
                                'text_wrap': True
                                })
        #format total bold table
        format5 = workbook.add_format({
                                'font_name' : 'Arial',
                                'font_size' : 10,
                                'bold' : 1,
                                'border': 1,
                                'align' : 'center',
                                'valign' : 'vcenter',
                                'text_wrap': True
                                })
        #format for merge
        merge_format = workbook.add_format({
                                'font_name' : 'Arial',
                                'font_size' : 11,
                                'bold': 1,
                                'border': 1,
                                'align': 'center',
                                'valign': 'vcenter',
                                'fg_color': '#00659C',
                                'font_color': 'white',
                                'text_wrap': True
                                })
        #create sheet
        worksheet = workbook.add_worksheet('Bug Summary')
        #header section
        worksheet.merge_range(0, 0, 0, 9, 'Bug Summary',format1)
        worksheet.merge_range(1, 0, 2, 0, 'Device', merge_format)
        worksheet.merge_range(1, 1, 2, 1, 'Version', merge_format)
        worksheet.merge_range(1, 2, 1, 6, 'Count of Bug Severity', merge_format)
        worksheet.merge_range(1, 7, 2, 7, 'Grand Total', merge_format)
        worksheet.merge_range(1, 8, 2, 8, 'Suggested Release Version', merge_format)
        worksheet.merge_range(1, 9, 2, 9, 'Suggestion', merge_format)

        worksheet.write(2,2,'1',format3)
        worksheet.write(2,3,'2',format3)
        worksheet.write(2,4,'3',format3)
        worksheet.write(2,5,'4',format3)
        worksheet.write(2,6,'5',format3)

        #data section
        row_check = 'ludesdeveloper'
        row_version = 'ludesdeveloper'
        iteration_check = False
        count_row = 3
        for row in records:
            if row_check not in row[0] and iteration_check == False:

                row_check = row[0]
                row_version = row[1]
                iteration_check = False
                start_row = count_row

                worksheet.write(count_row,0,row[0],format4)
                worksheet.write(count_row,1,row[1],format4)
                worksheet.write(count_row,2,'',format4)
                worksheet.write(count_row,3,'',format4)
                worksheet.write(count_row,4,'',format4)
                worksheet.write(count_row,5,'',format4)
                worksheet.write(count_row,6,'',format4)
                worksheet.write(count_row,7,'',format4)
                worksheet.write(count_row,8,'',format4)
                worksheet.write(count_row,9,'',format4)
                iteration_check == True
                count_row+=1

            elif row_check in row[0] and row_version not in row[1] and iteration_check == False:
                row_check = row[0]
                row_version = row[1]

                worksheet.write(count_row,1,row[1],format4)
                worksheet.write(count_row,2,'',format4)
                worksheet.write(count_row,3,'',format4)
                worksheet.write(count_row,4,'',format4)
                worksheet.write(count_row,5,'',format4)
                worksheet.write(count_row,6,'',format4)
                worksheet.write(count_row,7,'',format4)
                worksheet.write(count_row,8,'',format4)
                worksheet.write(count_row,9,'',format4)
                end_row = count_row
                worksheet.merge_range(start_row, 0, end_row, 0, row[0], format4)
                iteration_check == True
                count_row+=1

        #SOFTWARE SUMMARY TABLE
        #sql query
        cursor.execute('''SELECT version, COUNT(*) FROM swsumtable GROUP BY version''')
        records = cursor.fetchall()
        cursor.execute('''SELECT COUNT(version) FROM swsumtable''')
        total = cursor.fetchall()
        total=(str(total))
        total=re.sub("\D", "", total)
        total=int(total)
        #create sheet
        worksheet = workbook.add_worksheet('Software Summary Table')
        
        #header section
        worksheet.merge_range(0, 0, 0, 4, 'Software Summary Table',format1)
        worksheet.write(1,0,'Version',format2)
        worksheet.write(1,1,'Total',format2)
        worksheet.write(1,2,'Percentage',format2)
        worksheet.write(1,3,'End of Software Maintenance',format2)
        worksheet.write(1,4,'Last Date of Support',format2)
        #header for chart
        worksheet.merge_range(0, 6, 0, 7, 'For Chart',format1)
        worksheet.write(1,6,'Version',format4)
        worksheet.write(1,7,'Percentage',format4)

        #data section
        count_row = 2
        for row in records:
            worksheet.write(count_row,0,row[0],format4)
            worksheet.write(count_row,1,str(row[1]),format4)
            percentage = (str((row[1]/total)*100))
            percentage = re.findall('\d+[.]\d',percentage)
            percentage = percentage[0]
            worksheet.write(count_row,2,(percentage+'%'),format4)
            worksheet.write(count_row,3,'',format4)
            worksheet.write(count_row,4,'',format4)
            worksheet.write(count_row,6,row[0],format4)
            try:
                worksheet.write(count_row,7,float(percentage),format4)
            except:
                worksheet.write(count_row,7,"-",format4)
            count_row += 1

        #chart section
        software_chart = workbook.add_chart({'type': 'pie'})
        software_chart.add_series({
            "categories": ["Software Summary Table", 2, 6, count_row-1, 6],
            "values":     ["Software Summary Table", 2, 7, count_row-1, 7],
            "data_labels": {"value": True},
        })
        worksheet.insert_chart('J2', software_chart)

        #SOFTWARE TABLE
        #sql query
        cursor.execute('''SELECT devicename, model, version, confreg, uptime FROM swtable ORDER by devicename ASC''')
        records = cursor.fetchall()
        #create sheet
        worksheet = workbook.add_worksheet('Software Table')
        
        #header section
        worksheet.merge_range(0, 0, 0, 4, 'Software Table',format1)
        worksheet.write(1,0,'Device Name',format2)
        worksheet.write(1,1,'Model',format2)
        worksheet.write(1,2,'Version',format2)
        worksheet.write(1,3,'Build',format2)
        worksheet.write(1,4,'Uptime',format2)

        #data section
        count_row = 2
        for row in records:
            worksheet.write(count_row,0,row[0],format4)
            worksheet.write(count_row,1,row[1],format4)
            worksheet.write(count_row,2,row[2],format4)
            worksheet.write(count_row,3,row[3],format4)
            worksheet.write(count_row,4,row[4],format4)
            count_row += 1
        
        #HARDWARE SUMMARY TABLE
        #sql query
        cursor.execute('''SELECT tipe, date, COUNT(*) FROM eosltable GROUP BY tipe''')
        records = cursor.fetchall()
        cursor.execute('''SELECT COUNT(tipe) FROM eosltable''')
        total = cursor.fetchall()
        total=(str(total))
        total=re.sub("\D", "", total)
        total=int(total)
        #create sheet
        worksheet = workbook.add_worksheet('Hardware Summary Table')
        
        #header section
        worksheet.merge_range(0, 0, 0, 3, 'Hardware Summary Table',format1)
        worksheet.write(1,0,'Model',format2)
        worksheet.write(1,1,'Total',format2)
        worksheet.write(1,2,'Percentage',format2)
        worksheet.write(1,3,'End of Support',format2)
        #header for chart
        worksheet.merge_range(0, 5, 0, 6, 'For Chart',format1)
        worksheet.write(1,5,'Model',format4)
        worksheet.write(1,6,'Percentage',format4)

        #data section
        count_row = 2
        for row in records:
            worksheet.write(count_row,0,row[0],format4)
            worksheet.write(count_row,1,str(row[2]),format4)
            percentage = (str((row[2]/total)*100))
            percentage = re.findall('\d+[.]\d',percentage)
            percentage = percentage[0]
            worksheet.write(count_row,2,(percentage+'%'),format4)
            worksheet.write(count_row,3,row[1],format4)
            worksheet.write(count_row,5,row[0],format4)
            try:
                worksheet.write(count_row,6,float(percentage),format4)
            except:
                worksheet.write(count_row,6,"-",format4)
            count_row += 1

        #chart section
        hardware_chart = workbook.add_chart({'type': 'pie'})
        hardware_chart.add_series({
            "categories": ["Hardware Summary Table", 2, 5, count_row-1, 5],
            "values":     ["Hardware Summary Table", 2, 6, count_row-1, 6],
            "data_labels": {"value": True},
        })
        worksheet.insert_chart('I2', hardware_chart)

        #HARDWARE CARD TABLE
        #sql query
        cursor.execute('''SELECT devicename, model, tipe, sn, eosl_date FROM hwcardtable''')
        records = cursor.fetchall()
        #create sheet
        worksheet = workbook.add_worksheet('Hardware Card Table')
        
        #header section
        worksheet.merge_range(0, 0, 0, 4, 'Hardware Card Table',format1)
        worksheet.write(1,0,'Device Name',format2)
        worksheet.write(1,1,'Model',format2)
        worksheet.write(1,2,'Tipe',format2)
        worksheet.write(1,3,'Serial Number',format2)
        worksheet.write(1,4,'End of Support',format2)

        #data section
        iteration_check = False
        row_check = 'ludesdeveloper'
        count_row = 2
        for row in records:
            if row_check not in row[0]:
                row_check = row[0]
                iteration_check = False
                start_row = count_row
            else:
                pass
            if row_check == row[0] and iteration_check == False:
                worksheet.write(count_row,0,row[0],format4)
                worksheet.write(count_row,1,row[1],format4)
                worksheet.write(count_row,2,row[2],format4)
                worksheet.write(count_row,3,row[4],format4)
                worksheet.write(count_row,4,row[3],format4)
                iteration_check = True
            else:
                worksheet.write(count_row,0,'',format4)
                worksheet.write(count_row,1,row[1],format4)
                worksheet.write(count_row,2,row[2],format4)
                worksheet.write(count_row,3,row[4],format4)
                worksheet.write(count_row,4,row[3],format4)
                iteration_check = True
                end_row = count_row
                worksheet.merge_range(start_row, 0, end_row, 0, row[0], format4)
            count_row+=1
        
        #HARDWARE CONDITION ANALYSIS
        #sql query
        cursor.execute('''SELECT * FROM envtable''')
        records = cursor.fetchall()
        #create sheet
        worksheet = workbook.add_worksheet('Hardware Condition Analysis')
        
        #header section
        worksheet.merge_range(0, 0, 0, 3, 'Hardware Condition Analysis',format1)
        worksheet.write(1,0,'Device Name',format2)
        worksheet.write(1,1,'System',format2)
        worksheet.write(1,2,'Item',format2)
        worksheet.write(1,3,'Status',format2)

        #data section
        iteration_check = False
        row_check = 'ludesdeveloper'
        count_row = 2
        for row in records:
            if row_check not in row[1]:
                row_check = row[1]
                iteration_check = False
                start_row = count_row
            else:
                pass
            if row_check == row[1] and iteration_check == False:
                worksheet.write(count_row,0,row[1],format4)
                worksheet.write(count_row,1,row[2],format4)
                worksheet.write(count_row,2,row[3],format4)
                worksheet.write(count_row,3,row[4],format4)
                iteration_check = True
            else:
                worksheet.write(count_row,0,'',format4)
                worksheet.write(count_row,1,row[2],format4)
                worksheet.write(count_row,2,row[3],format4)
                worksheet.write(count_row,3,row[4],format4)
                iteration_check = True
                end_row = count_row
                worksheet.merge_range(start_row, 0, end_row, 0, row[1], format4)
            count_row+=1

        #CPU SUMMARY TABLE
        #sql query
        cursor.execute('''SELECT devicename, model, cpu_current, cpu_30d, cpu_status FROM cputable ORDER by devicename ASC''')
        records = cursor.fetchall()
        #create sheet
        worksheet = workbook.add_worksheet('CPU Summary Table')
        
        #header section
        worksheet.merge_range(0, 0, 0, 4, 'CPU Summary Table',format1)
        worksheet.write(1,0,'Device Name',format2)
        worksheet.write(1,1,'Model',format2)
        worksheet.write(1,2,'Current (%)',format2)
        worksheet.write(1,3,'30 Days (%)',format2)
        worksheet.write(1,4,'Status',format2)
        #header for chart
        worksheet.merge_range(0, 6, 0, 7, 'For Chart',format1)
        worksheet.write(1,6,'Device Name',format4)
        worksheet.write(1,7,'Current CPU (%)',format4)

        #data section
        count_row = 2
        for row in records:
            worksheet.write(count_row,0,row[0],format4)
            worksheet.write(count_row,1,(row[1]+"%"),format4)
            worksheet.write(count_row,2,(row[2]+"%"),format4)
            worksheet.write(count_row,3,(row[3]+"%"),format4)
            worksheet.write(count_row,4,row[4],format4)
            worksheet.write(count_row,6,row[0],format4)
            try:
                worksheet.write(count_row,7,float(row[2]),format4)
            except:
                worksheet.write(count_row,7,"-",format4)
            count_row += 1

        #chart section
        cpu_chart = workbook.add_chart({'type': 'column'})
        for i in range(3,count_row+1):
            cpu_chart.add_series(
                {
                    "name" : "='CPU Summary Table'!G"+str(i),
                    "values": "='CPU Summary Table'!H"+str(i),
                    "data_labels": {"value": True},
                    }
                )
        worksheet.insert_chart('J2', cpu_chart)

        #MEMORY SUMMARY TABLE
        #sql query
        cursor.execute('''SELECT devicename, memory_name, memory_current, memory_30d, memory_status FROM memtable ORDER by devicename ASC''')
        records = cursor.fetchall()
        #create sheet
        worksheet = workbook.add_worksheet('Memory Summary Table')
        
        #header section
        worksheet.merge_range(0, 0, 0, 4, 'Memory Summary Table',format1)
        worksheet.write(1,0,'Device Name',format2)
        worksheet.write(1,0,'Device Name',format2)
        worksheet.write(1,1,'Model',format2)
        worksheet.write(1,2,'Current (%)',format2)
        worksheet.write(1,3,'30 Days (%)',format2)
        worksheet.write(1,4,'Status',format2)
        #header for chart
        #header for chart
        worksheet.merge_range(0, 6, 0, 7, 'For Chart',format1)
        worksheet.write(1,6,'Device Name',format4)
        worksheet.write(1,7,'Current Memory (%)',format4)

        #data section
        count_row = 2
        for row in records:
            worksheet.write(count_row,0,row[0],format4)
            worksheet.write(count_row,1,(row[1]+"%"),format4)
            worksheet.write(count_row,2,(row[2]+"%"),format4)
            worksheet.write(count_row,3,(row[3]+"%"),format4)
            worksheet.write(count_row,4,row[4],format4)
            worksheet.write(count_row,6,row[0],format4)
            try:
                worksheet.write(count_row,7,float(row[2]),format4)
            except:
                worksheet.write(count_row,7,"-",format4)
            count_row += 1

        #chart section
        memory_chart = workbook.add_chart({'type': 'column'})
        for i in range(3,count_row+1):
            memory_chart.add_series(
                {
                    "name" : "='Memory Summary Table'!G"+str(i),
                    "values": "='Memory Summary Table'!H"+str(i),
                    "data_labels": {"value": True},
                    }
                )
        worksheet.insert_chart('J2', memory_chart)

        #THROUGHPUT SUMMARY TABLE
        #sql query throughputbitstable
        cursor.execute('''SELECT devicename, throughput_bits_name, throughput_bits_current, throughput_bits_30d, throughput_bits_status FROM throughputbitstable ORDER by devicename ASC''')
        records_bits = cursor.fetchall()

        #create sheet
        worksheet = workbook.add_worksheet('Throughput Summary Table 1')
        
        #header section throughputbitstable
        worksheet.merge_range(0, 0, 0, 4, 'Throughput Summary Table',format1)
        worksheet.write(1,0,'Device Name',format2)
        worksheet.write(1,1,'Throughput (bits)',format2)
        worksheet.write(1,2,'Current',format2)
        worksheet.write(1,3,'30 Days',format2)
        worksheet.write(1,4,'Status',format2)

        #data section throughputbitstable
        count_row_bits = 2
        for row in records_bits:
            worksheet.write(count_row_bits,0,row[0],format4)
            worksheet.write(count_row_bits,1,row[1],format4)
            worksheet.write(count_row_bits,2,row[2],format4)
            worksheet.write(count_row_bits,3,row[3],format4)
            worksheet.write(count_row_bits,4,row[4],format4)
            count_row_bits += 1
        
        #sql query throughputssltable
        cursor.execute('''SELECT devicename, throughput_ssl_name, throughput_ssl_current, throughput_ssl_30d, throughput_ssl_status FROM throughputssltable ORDER by devicename ASC''')
        records_ssl = cursor.fetchall()

        #create sheet
        worksheet = workbook.add_worksheet('Throughput Summary Table 2')

        #header section throughputssltable
        worksheet.merge_range(0, 0, 0, 4, 'SSL Transcation Table',format1)
        worksheet.write(1,0,'Device Name',format2)
        worksheet.write(1,1,'SSL Transcation',format2)
        worksheet.write(1,2,'Current',format2)
        worksheet.write(1,3,'30 Days',format2)
        worksheet.write(1,4,'Status',format2)

        #data section throughputssltable
        count_row_ssl = 2
        for row in records_ssl:
            worksheet.write(count_row_ssl,0,row[0],format4)
            worksheet.write(count_row_ssl,1,row[1],format4)
            worksheet.write(count_row_ssl,2,row[2],format4)
            worksheet.write(count_row_ssl,3,row[3],format4)
            worksheet.write(count_row_ssl,4,row[4],format4)
            count_row_ssl += 1

        #sql query throughputpackettable
        cursor.execute('''SELECT devicename, throughput_packet_name, throughput_packet_current, throughput_packet_30d, throughput_packet_status FROM throughputpackettable ORDER by devicename ASC''')
        records_packet = cursor.fetchall()
        
        #create sheet
        worksheet = workbook.add_worksheet('Throughput Summary Table 3')

        #header section throughputpackettable
        worksheet.merge_range(0, 0, 0, 4, 'Throughput (packets) Table',format1)
        worksheet.write(1,0,'Device Name',format2)
        worksheet.write(1,1,'Throughput (packets)',format2)
        worksheet.write(1,2,'Current',format2)
        worksheet.write(1,3,'30 Days',format2)
        worksheet.write(1,4,'Status',format2)

        #data section throughputbitstable
        count_row_packet = 2
        for row in records_packet:
            worksheet.write(count_row_packet,0,row[0],format4)
            worksheet.write(count_row_packet,1,row[1],format4)
            worksheet.write(count_row_packet,2,row[2],format4)
            worksheet.write(count_row_packet,3,row[3],format4)
            worksheet.write(count_row_packet,4,row[4],format4)
            count_row_packet += 1
        
        #CONNECTION SUMMARY TABLE
        #sql query connectiontable
        cursor.execute('''SELECT devicename, connection_name, connection_current, connection_30d, connection_status FROM connectiontable ORDER by devicename ASC''')
        records_connection = cursor.fetchall()

        #create sheet
        worksheet = workbook.add_worksheet('Connection Summary Table 1')
        
        #header section connectiontable
        worksheet.merge_range(0, 0, 0, 4, 'Active Connection Table',format1)
        worksheet.write(1,0,'Device Name',format2)
        worksheet.write(1,1,'Active Connection',format2)
        worksheet.write(1,2,'Current',format2)
        worksheet.write(1,3,'30 Days',format2)
        worksheet.write(1,4,'Status',format2)

        #data section connectiontable
        count_row_connection = 2
        for row in records_connection:
            worksheet.write(count_row_connection,0,row[0],format4)
            worksheet.write(count_row_connection,1,row[1],format4)
            worksheet.write(count_row_connection,2,row[2],format4)
            worksheet.write(count_row_connection,3,row[3],format4)
            worksheet.write(count_row_connection,4,row[4],format4)
            count_row_connection += 1

        #sql query newconnectiontable
        cursor.execute('''SELECT devicename, newconnection_name, newconnection_current, newconnection_30d, newconnection_status FROM newconnectiontable ORDER by devicename ASC''')
        records_new = cursor.fetchall()

        #create sheet
        worksheet = workbook.add_worksheet('Connection Summary Table 2')

        #header section newconnectiontable
        worksheet.merge_range(0, 0, 0, 4, 'Total New Connection (/sec) Table',format1)
        worksheet.write(1,0,'Device Name',format2)
        worksheet.write(1,1,'Total New Connection (/sec)',format2)
        worksheet.write(1,2,'Current',format2)
        worksheet.write(1,3,'30 Days',format2)
        worksheet.write(1,4,'Status',format2)

        #data section newconnectiontable
        count_row_new = 2
        for row in records_new:
            worksheet.write(count_row_new,0,row[0],format4)
            worksheet.write(count_row_new,1,row[1],format4)
            worksheet.write(count_row_new,2,row[2],format4)
            worksheet.write(count_row_new,3,row[3],format4)
            worksheet.write(count_row_new,4,row[4],format4)
            count_row_new += 1

        #sql query httprequeststable
        cursor.execute('''SELECT devicename, httprequests_name, httprequests_current, httprequests_30d, httprequests_status FROM httprequeststable ORDER by devicename ASC''')
        records_http = cursor.fetchall()
        
        #create sheet
        worksheet = workbook.add_worksheet('Connection Summary Table 3')
        
        #header section httprequeststable
        worksheet.merge_range(0, 0, 0, 4, 'HTTP Request (/sec) Table',format1)
        worksheet.write(1,0,'Device Name',format2)
        worksheet.write(1,1,'HTTP Request (/sec)',format2)
        worksheet.write(1,2,'Current',format2)
        worksheet.write(1,3,'30 Days',format2)
        worksheet.write(1,4,'Status',format2)

        #data section httprequeststable
        count_row_http = 2
        for row in records_http:
            worksheet.write(count_row_http,0,row[0],format4)
            worksheet.write(count_row_http,1,row[1],format4)
            worksheet.write(count_row_http,2,row[2],format4)
            worksheet.write(count_row_http,3,row[3],format4)
            worksheet.write(count_row_http,4,row[4],format4)
            count_row_http += 1
        
        #LTM SUMMARY TABLE
        #sql query LTM SUMMARY TABLE
        cursor.execute('''SELECT devicename, ltm_tipe,
                sum(case when availability = 'available' then 1 else 0 end) AS Available,
                sum(case when availability = 'unavailable' then 1 else 0 end) AS Unavailable,
                sum(case when availability = 'offline' then 1 else 0 end) AS Offline,
                sum(case when availability = 'unknown' then 1 else 0 end) AS Unknown,
                sum(case when availability = 'available' and state LIKE 'disabled%' then 1 else 0 end) AS AvDisable,
                sum(case when availability = 'unavailable' and state LIKE 'disabled%' then 1 else 0 end) AS UnaDisable,
                sum(case when availability = 'offline' and state LIKE 'disabled%' then 1 else 0 end) AS OffDisable,
                sum(case when availability = 'unknown' and state LIKE 'disabled%' then 1 else 0 end) AS UnkDisable,
                count(*) AS Total
                FROM ltmtable
                GROUP BY ltm_tipe, devicename
                ORDER BY devicename, ltm_tipe DESC''')
        records = cursor.fetchall()

        #create sheet
        worksheet = workbook.add_worksheet('LTM Summary Table')
        
        #header section LTM SUMMARY TABLE
        worksheet.merge_range(0, 0, 0, 6, 'LTM Summary Table',format1)
        worksheet.write(1,0,'Device Name',format2)
        worksheet.write(1,1,'LTM',format2)
        worksheet.write(1,2,'Total',format2)
        worksheet.write(1,3,'Available',format2)
        worksheet.write(1,4,'Unavailable',format2)
        worksheet.write(1,5,'Offline',format2)
        worksheet.write(1,6,'Unknown',format2)

        #data section LTM SUMMARY TABLE
        count_row = 2
        for row in records:
            worksheet.write(count_row,0,row[0],format4)
            worksheet.write(count_row,1,row[1],format4)
            worksheet.write(count_row,2,str(row[10]),format4)

            if row[2] == 0:
                worksheet.write(count_row,3,row[2],format4)
            elif row[6] == 0:
                worksheet.write(count_row,3,row[2],format4)
            else:
                worksheet.write(count_row,3,str(row[2])+' (' +str(row[6])+ ' Disabled)',format4)

            if row[3] == 0:
                worksheet.write(count_row,4,row[3],format4)
            elif row[7] == 0:
                worksheet.write(count_row,4,row[3],format4)
            else:
                worksheet.write(count_row,4,str(row[3])+' (' +str(row[7])+ ' Disabled)',format4)
            
            if row[4] == 0:
                worksheet.write(count_row,5,row[4],format4)
            elif row[8] == 0:
                worksheet.write(count_row,5,row[4],format4)
            else:
                worksheet.write(count_row,5,str(row[4])+ ' (' +str(row[8])+ ' Disabled)',format4)
            
            if row[5] == 0:
                worksheet.write(count_row,6,row[5],format4)
            elif row[9] == 0:
                worksheet.write(count_row,6,row[5],format4)
            else:
                worksheet.write(count_row,6,str(row[5])+ ' (' +str(row[9])+ ' Disabled)',format4)

            count_row += 1
        
        #sql query Virtual Server Summary Table
        cursor.execute('''SELECT devicename, ltm_name, availability, state FROM ltmtable WHERE ltm_tipe = 'Virtual Server'
                GROUP BY ltm_name
                ORDER BY ltm_name ASC''')
        records = cursor.fetchall()

        #create sheet
        worksheet = workbook.add_worksheet('Virtual Server Summary Table')
        
        #header section Virtual Server Summary Table
        worksheet.merge_range(0, 0, 0, 3, 'Virtual Server Summary Table',format1)
        worksheet.write(1,0,'Device Name',format2)
        worksheet.write(1,1,'Virtual Server Name',format2)
        worksheet.write(1,2,'Availability',format2)
        worksheet.write(1,3,'State',format2)

        #data section Virtual Server Summary Table
        count_row = 2
        for row in records:
            worksheet.write(count_row,0,row[0],format4)
            worksheet.write(count_row,1,row[1],format4)
            worksheet.write(count_row,2,row[2],format4)
            worksheet.write(count_row,3,row[3],format4)

            count_row += 1
        
        #sql query Pool Summary Table
        cursor.execute('''SELECT devicename, ltm_name, availability, state FROM ltmtable WHERE ltm_tipe = 'Pool'
                GROUP BY ltm_name
                ORDER BY ltm_name ASC''')
        records = cursor.fetchall()

        #create sheet
        worksheet = workbook.add_worksheet('Pool Summary Table')
        
        #header section Pool Summary Table
        worksheet.merge_range(0, 0, 0, 3, 'Pool Summary Table',format1)
        worksheet.write(1,0,'Device Name',format2)
        worksheet.write(1,1,'Pool Name',format2)
        worksheet.write(1,2,'Availability',format2)
        worksheet.write(1,3,'State',format2)

        #data section Pool Summary Table
        count_row = 2
        for row in records:
            worksheet.write(count_row,0,row[0],format4)
            worksheet.write(count_row,1,row[1],format4)
            worksheet.write(count_row,2,row[2],format4)
            worksheet.write(count_row,3,row[3],format4)

            count_row += 1
        
        #sql query Node Summary Table
        cursor.execute('''SELECT devicename, ltm_name, availability, state FROM ltmtable WHERE ltm_tipe = 'Node'
                GROUP BY ltm_name
                ORDER BY ltm_name ASC''')
        records = cursor.fetchall()

        #create sheet
        worksheet = workbook.add_worksheet('Node Summary Table')
        
        #header section Node Summary Table
        worksheet.merge_range(0, 0, 0, 3, 'Node Summary Table',format1)
        worksheet.write(1,0,'Device Name',format2)
        worksheet.write(1,1,'Node Name',format2)
        worksheet.write(1,2,'Availability',format2)
        worksheet.write(1,3,'State',format2)

        #data section Node Summary Table
        count_row = 2
        for row in records:
            worksheet.write(count_row,0,row[0],format4)
            worksheet.write(count_row,1,row[1],format4)
            worksheet.write(count_row,2,row[2],format4)
            worksheet.write(count_row,3,row[3],format4)

            count_row += 1
        
        #HIGH AVAILABILITY SUMMARY TABLE
        #sql query hatable
        cursor.execute('''SELECT devicename, device, mgmt_ip, configsync_ip, ha_state, ha_status FROM hatable''')
        records_ha = cursor.fetchall()
        #sql query synchatable
        cursor.execute('''SELECT devicename, sync_status, action FROM synchatable''')
        records_sync = cursor.fetchall()
        #sql query traffichatable
        cursor.execute('''SELECT devicename, traffic_group, device_traffic, traffic_status, next_active, previous_active, active_reason FROM traffichatable''')
        records_traffic = cursor.fetchall()
        #create sheet
        worksheet = workbook.add_worksheet('High Availability Summary Table')
        
        #header section hatable
        worksheet.merge_range(0, 0, 0, 5, 'High Availability Summary Table',format1)
        worksheet.write(1,0,'Device Name',format2)
        worksheet.write(1,1,'HA Hostname',format2)
        worksheet.write(1,2,'Mgmt IP',format2)
        worksheet.write(1,3,'Configsync IP',format2)
        worksheet.write(1,4,'Device HA State',format2)
        worksheet.write(1,5,'Status HA',format2)

        #data section hatable
        count_row_ha = 2
        for row in records_ha:
            worksheet.write(count_row_ha,0,row[0],format4)
            worksheet.write(count_row_ha,1,row[1],format4)
            worksheet.write(count_row_ha,2,row[2],format4)
            worksheet.write(count_row_ha,3,row[3],format4)
            worksheet.write(count_row_ha,4,row[4],format4)
            worksheet.write(count_row_ha,5,row[5],format4)
            count_row_ha += 1
        
        #header section synchatable
        worksheet.write(6,0,'Device Name',format2)
        worksheet.write(6,1,'Sync Status',format2)
        worksheet.write(6,2,'Recommended Action',format2)

        #data section throughputbitstable
        count_row_sync = 7
        for row in records_sync:
            worksheet.write(count_row_sync,0,row[0],format4)
            worksheet.write(count_row_sync,1,row[1],format4)
            worksheet.write(count_row_sync,2,row[2],format4)
            count_row_sync += 1
        
        #header section traffichatable
        worksheet.write(9,0,'Device Name',format2)
        worksheet.write(9,1,'Traffic Group',format2)
        worksheet.write(9,2,'Traffic Hostname',format2)
        worksheet.write(9,3,'Status Traffic',format2)
        worksheet.write(9,4,'Next Active',format2)
        worksheet.write(9,5,'Previous Active',format2)
        worksheet.write(9,6,'Active Reason',format2)

        #data section throughputbitstable
        count_row_traffic = 10
        for row in records_traffic:
            worksheet.write(count_row_traffic,0,row[0],format4)
            worksheet.write(count_row_traffic,1,row[1],format4)
            worksheet.write(count_row_traffic,2,row[2],format4)
            worksheet.write(count_row_traffic,3,row[3],format4)
            worksheet.write(count_row_traffic,4,row[4],format4)
            worksheet.write(count_row_traffic,5,row[5],format4)
            worksheet.write(count_row_traffic,6,row[6],format4)
            count_row_traffic += 1
        
        #CERTIFICATE TABLE
        #sql query
        cursor.execute('''SELECT devicename, cert_name, cn, expired_date, cert_status FROM certtable ORDER by cert_name ASC''')
        records = cursor.fetchall()
        #create sheet
        worksheet = workbook.add_worksheet('Certificate Table')
        
        #header section
        worksheet.merge_range(0, 0, 0, 4, 'Certificate',format1)
        worksheet.write(1,0,'Device Name',format2)
        worksheet.write(1,1,'Certificate Name',format2)
        worksheet.write(1,2,'CN',format2)
        worksheet.write(1,3,'Expired Date',format2)
        worksheet.write(1,4,'Certificate Status',format2)

        #data section
        count_row = 2
        for row in records:
            worksheet.write(count_row,0,row[0],format4)
            worksheet.write(count_row,1,row[1],format4)
            worksheet.write(count_row,2,row[2],format4)
            worksheet.write(count_row,3,row[3],format4)
            worksheet.write(count_row,4,row[4],format4)
            count_row += 1
        
        #LICENSE AND MODULE TABLE
        #sql query
        cursor.execute('''SELECT devicename, lic_version, register_key, active_module, lic_on, svc_date, lic_status FROM lictable''')
        records = cursor.fetchall()
        #create sheet
        worksheet = workbook.add_worksheet('Licenseand Module Summary Table')
        
        #header section
        worksheet.merge_range(0, 0, 0, 6, 'License and Module Summary Table',format1)
        worksheet.write(1,0,'Device Name',format2)
        worksheet.write(1,1,'Licensed Version',format2)
        worksheet.write(1,2,'Registration Key',format2)
        worksheet.write(1,3,'Active Module',format2)
        worksheet.write(1,4,'Licensed On',format2)
        worksheet.write(1,5,'Service Check Date',format2)
        worksheet.write(1,6,'License Status',format2)

        #data section
        count_row = 2
        for row in records:
            worksheet.write(count_row,0,row[0],format4)
            worksheet.write(count_row,1,row[1],format4)
            worksheet.write(count_row,2,row[2],format4)
            worksheet.write(count_row,3,row[3],format4)
            worksheet.write(count_row,4,row[4],format4)
            worksheet.write(count_row,5,row[5],format4)
            worksheet.write(count_row,6,row[6],format4)
            count_row += 1
        
        #CLOSE WORKBOOK
        workbook.close()

        #close database
        db.close()
        
        #save document
        print('')
        print('Saving Document')
        print('Document has been saved to f5.xlsx')