import pandas as pd
from sqlalchemy import create_engine
import sqlite3
import pkg_resources
import os
import re

class insert_cpu_coke:
    @staticmethod
    def insert_cpu_coke():
        capture_path = pkg_resources.resource_filename('netoprmgr', 'static/')
        capture_path = os.path.join(capture_path,'capture/')
        files = os.listdir(capture_path)
        list_name = []
        tnpa_pdf = []
        list_png = []
        for i in files:
            fel = re.findall('.*.xlsx', i)
            for file in fel:
                # print(f'file {file}')
                list_name.append(file)
        try:
            df = pd.read_excel(capture_path+list_name[0])
            db = sqlite3.connect(capture_path+'pmdb_rs')
            df.to_sql('cpucoke', con=db, if_exists='append', index=False)
        except:
            # try:
            print("File xlxs not found")
            db = sqlite3.connect('pmdb_rs')
            cursor = db.cursor()
            # try:
            cursor.execute('''
                CREATE TABLE cpucoke(id INTEGER PRIMARY KEY, Node TEXT, [Average CPU Load] TEXT, [Peak CPU Load] TEXT)
            ''')
            cursor.fetchall()
            
            cursor.execute('''INSERT INTO cpucoke(Node, [Average CPU Load], [Peak CPU Load]) 
                    VALUES(?,?,?)''', ('Error', 'Error', 'Error',))
            db.commit()
            
                
            # except:
            #     pass
            # except:
            #     pass    
            db.close()

# insert_cpu_coke = insert_cpu_coke.insert_cpu_coke()