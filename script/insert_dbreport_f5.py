import sqlite3

class insert_dbreport_f5:
    def __init__(self,
                devicename, model, iosversion, uptime, confreg, version, date_eos, date_last,
                tipe_list, list_serial_number, list_date,
                eos_date, eos_tipe, list_tipe,
                list_psu, list_psu_cond, list_fan, list_fan_cond_cp, list_temp, list_temp_cond,
                cpu_current, cpu_3h, cpu_24h, cpu_7d, cpu_30d, cpu_status,
                memory_name_list, memory_current_list, memory_3h_list, memory_24h_list, memory_7d_list, memory_30d_list, memory_status_list,
                throughput_bits_name_list, throughput_bits_current_list, throughput_bits_3h_list, throughput_bits_24h_list, throughput_bits_7d_list, throughput_bits_30d_list, throughput_bits_status_list,
                throughput_ssl_name_list, throughput_ssl_current_list, throughput_ssl_3h_list, throughput_ssl_24h_list, throughput_ssl_7d_list, throughput_ssl_30d_list, throughput_ssl_status_list,
                throughput_packet_name_list, throughput_packet_current_list, throughput_packet_3h_list, throughput_packet_24h_list, throughput_packet_7d_list, throughput_packet_30d_list, throughput_packet_status_list,
                connection_name_list, connection_current_list, connection_3h_list, connection_24h_list, connection_7d_list, connection_30d_list, connection_status_list,
                newconnection_name_list, newconnection_current_list, newconnection_3h_list, newconnection_24h_list, newconnection_7d_list, newconnection_30d_list, newconnection_status_list,
                httprequests_name_list, httprequests_current_list, httprequests_3h_list, httprequests_24h_list, httprequests_7d_list, httprequests_30d_list, httprequests_status_list,
                ltm_tipe_list, ltm_name_list, availability_list, state_list,
                device_list, mgmt_ip_list, configsync_ip_list, ha_state_list, ha_status_list,
                traffic_group_list, device_traffic_list, traffic_status_list, next_active_list, previous_active_list, active_reason_list,
                sync_status, action,
                cert_name_list, cn_list, expired_date_list, cert_status_list,
                lic_version, register_key, active_module, lic_on, svc_date, lic_status,
                device_class_name,
                file_name
    ):
        self.devicename = devicename
        self.model = model
        self.iosversion = iosversion
        self.uptime = uptime
        self.confreg = confreg
        self.version = version
        self.date_eos = date_eos
        self.date_last = date_last
        self.tipe_list = tipe_list
        self.list_serial_number = list_serial_number
        self.list_date = list_date
        self.eos_date = eos_date
        self.eos_tipe = eos_tipe
        self.list_tipe = list_tipe
        self.list_psu = list_psu
        self.list_psu_cond = list_psu_cond
        self.list_fan = list_fan
        self.list_fan_cond_cp = list_fan_cond_cp
        self.list_temp = list_temp
        self.list_temp_cond = list_temp_cond
        self.cpu_current = cpu_current
        self.cpu_3h = cpu_3h
        self.cpu_24h = cpu_24h
        self.cpu_7d = cpu_7d
        self.cpu_30d = cpu_30d
        self.cpu_status = cpu_status
        self.memory_name_list = memory_name_list
        self.memory_current_list = memory_current_list
        self.memory_3h_list = memory_3h_list
        self.memory_24h_list = memory_24h_list
        self.memory_7d_list = memory_7d_list
        self.memory_30d_list = memory_30d_list
        self.memory_status_list = memory_status_list
        self.throughput_bits_name_list = throughput_bits_name_list
        self.throughput_bits_current_list = throughput_bits_current_list
        self.throughput_bits_3h_list = throughput_bits_3h_list
        self.throughput_bits_24h_list = throughput_bits_24h_list
        self.throughput_bits_7d_list = throughput_bits_7d_list
        self.throughput_bits_30d_list = throughput_bits_30d_list
        self.throughput_bits_status_list = throughput_bits_status_list
        self.throughput_ssl_name_list = throughput_ssl_name_list
        self.throughput_ssl_current_list = throughput_ssl_current_list
        self.throughput_ssl_3h_list = throughput_ssl_3h_list
        self.throughput_ssl_24h_list = throughput_ssl_24h_list
        self.throughput_ssl_7d_list = throughput_ssl_7d_list
        self.throughput_ssl_30d_list = throughput_ssl_30d_list
        self.throughput_ssl_status_list = throughput_ssl_status_list
        self.throughput_packet_name_list = throughput_packet_name_list
        self.throughput_packet_current_list = throughput_packet_current_list
        self.throughput_packet_3h_list = throughput_packet_3h_list
        self.throughput_packet_24h_list = throughput_packet_24h_list
        self.throughput_packet_7d_list = throughput_packet_7d_list
        self.throughput_packet_30d_list = throughput_packet_30d_list
        self.throughput_packet_status_list = throughput_packet_status_list
        self.connection_name_list = connection_name_list
        self.connection_current_list = connection_current_list
        self.connection_3h_list = connection_3h_list
        self.connection_24h_list = connection_24h_list
        self.connection_7d_list = connection_7d_list
        self.connection_30d_list = connection_30d_list
        self.connection_status_list = connection_status_list
        self.newconnection_name_list = newconnection_name_list
        self.newconnection_current_list = newconnection_current_list
        self.newconnection_3h_list = newconnection_3h_list
        self.newconnection_24h_list = newconnection_24h_list
        self.newconnection_7d_list = newconnection_7d_list
        self.newconnection_30d_list = newconnection_30d_list
        self.newconnection_status_list = newconnection_status_list     
        self.httprequests_name_list = httprequests_name_list
        self.httprequests_current_list = httprequests_current_list
        self.httprequests_3h_list = httprequests_3h_list
        self.httprequests_24h_list = httprequests_24h_list
        self.httprequests_7d_list = httprequests_7d_list
        self.httprequests_30d_list = httprequests_30d_list
        self.httprequests_status_list = httprequests_status_list
        self.ltm_tipe_list = ltm_tipe_list
        self.ltm_name_list = ltm_name_list
        self.availability_list = availability_list
        self.state_list = state_list
        self.device_list = device_list
        self.mgmt_ip_list = mgmt_ip_list
        self.configsync_ip_list = configsync_ip_list
        self.ha_state_list = ha_state_list
        self.ha_status_list = ha_status_list
        self.sync_status = sync_status
        self.action = action
        self.traffic_group_list = traffic_group_list
        self.device_traffic_list = device_traffic_list
        self.traffic_status_list = traffic_status_list
        self.next_active_list = next_active_list
        self.previous_active_list = previous_active_list
        self.active_reason_list = active_reason_list
        self.cert_name_list = cert_name_list
        self.cn_list = cn_list = cn_list = cn_list
        self.expired_date_list = expired_date_list
        self.cert_status_list = cert_status_list
        self.lic_version = lic_version
        self.register_key = register_key
        self.active_module = active_module
        self.lic_on = lic_on
        self.svc_date = svc_date
        self.lic_status = lic_status
        self.device_class_name = device_class_name
        self.file_name = file_name

    def insert_dbreport_f5(self):
        #open db connection
        db = sqlite3.connect('pmdb_f5')
        cursor = db.cursor()

        #DB TIPE TABLE
        try:
            for enum, tipe in enumerate(self.tipe_list):
                cursor.execute('''INSERT INTO tipetable(tipe)
                        VALUES(?)''', (tipe,))
        except:
            cursor.execute('''INSERT INTO tipetable(tipe)
                    VALUES(?)''', (self.file_name+'-'+'error',))

        #DB SOFTWARE TABLE
        try:
            cursor.execute('''INSERT INTO swtable(devicename, model, iosversion, uptime, confreg, version, date_eos, date_last)
                    VALUES(?,?,?,?,?,?,?,?)''', (self.devicename, self.model, self.version, self.uptime, self.confreg, self.version, self.date_eos, self.date_last))
        except:
            cursor.execute('''INSERT INTO swtable(devicename, model, iosversion, uptime, confreg, version, date_eos, date_last)
                    VALUES(?,?,?,?,?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))

        try:
            cursor.execute('''INSERT INTO swsumtable(version, date_eos, date_last)
                    VALUES(?,?,?)''', (self.version, self.date_eos, self.date_last))
        except:
            cursor.execute('''INSERT INTO swsumtable(version, date_eos, date_last)
                    VALUES(?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))
        
        #DB HARDWARE TABLE
        try:
            cursor.execute('''INSERT INTO hwsumtable(model)
                    VALUES(?)''', (self.model,))
            count_sql = 0
        except:
            cursor.execute('''INSERT INTO hwsumtable(model)
                    VALUES(?)''', (self.file_name+'-'+'error',))
            count_sql = 0
        try:
            for enum, tipe in enumerate(self.list_tipe):
                cursor.execute('''INSERT INTO hwcardtable(devicename, model, tipe, sn, eosl_date)
                        VALUES(?,?,?,?,?)''', (self.devicename, self.model, tipe, self.list_serial_number[enum], self.list_date[enum]))
        except:
            cursor.execute('''INSERT INTO hwcardtable(devicename, model, tipe, sn, eosl_date)
                    VALUES(?,?,?,?,?)''', (self.file_name+'-'+'error',self.file_name+'-'+'error',self.file_name+'-'+'error',self.file_name+'-'+'error',self.file_name+'-'+'error',))
        
        #DB EOS TABLE
        try:
            for enum, tipe in enumerate(self.eos_tipe):
                cursor.execute('''INSERT INTO eosltable(model, tipe, date)
                        VALUES(?,?,?)''', (self.model, tipe, self.eos_date[enum],))
        except:
            cursor.execute('''INSERT INTO eosltable(model, tipe, date)
                    VALUES(?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))

        #DB ENVIRONMENT
        try:
            count_sql = 0
            for psu in self.list_psu:
                cursor.execute('''INSERT INTO envtable(devicename, system, item, status)
                        VALUES(?,?,?,?)''', (self.devicename,'Power Supply',psu,self.list_psu_cond[count_sql],))
                count_sql+=1
        except:
            count_sql = 0
            for psu in self.list_psu:
                cursor.execute('''INSERT INTO envtable(devicename, system, item, status)
                        VALUES(?,?,?,?)''', (self.file_name+'-'+'error','Power Supply',self.file_name+'-'+'error',self.file_name+'-'+'error',))
                count_sql+=1
        try:
            count_sql = 0
            for fan in self.list_fan:
                cursor.execute('''INSERT INTO envtable(devicename, system, item, status)
                        VALUES(?,?,?,?)''', (self.devicename,'Fan',fan,self.list_fan_cond_cp[count_sql],))
                count_sql+=1
        except:
            count_sql = 0
            for fan in self.list_fan:
                cursor.execute('''INSERT INTO envtable(devicename, system, item, status)
                        VALUES(?,?,?,?)''', (self.file_name+'-'+'error','Fan',self.file_name+'-'+'error',self.file_name+'-'+'error',))
                count_sql+=1
        try:
            count_sql = 0
            for temp in self.list_temp:
                cursor.execute('''INSERT INTO envtable(devicename, system, item, status)
                        VALUES(?,?,?,?)''', (self.devicename,'Temperature',temp,self.list_temp_cond[count_sql],))
                count_sql+=1
        except:
            count_sql = 0
            for temp in self.list_temp:
                cursor.execute('''INSERT INTO envtable(devicename, system, item, status)
                        VALUES(?,?,?,?)''', (self.file_name+'-'+'error','Temperature',self.file_name+'-'+'error',self.file_name+'-'+'error',))
                count_sql+=1

        #DB CPU SUMMARY TABLE
        try:
            cursor.execute('''INSERT INTO cputable(devicename, model, cpu_current, cpu_3h, cpu_24h, cpu_7d, cpu_30d, cpu_status)
                    VALUES(?,?,?,?,?,?,?,?)''', (self.devicename, self.model, self.cpu_current, self.cpu_3h, self.cpu_24h, self.cpu_7d, self.cpu_30d, self.cpu_status,))
        except:
            cursor.execute('''INSERT INTO cputable(devicename, model, cpu_current, cpu_3h, cpu_24h, cpu_7d, cpu_30d, cpu_status)
                    VALUES(?,?,?,?,?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))            
        
        #DB MEMORY SUMMARY TABLE
        for enum, memory in enumerate(self.memory_name_list):
            try:
                cursor.execute('''INSERT INTO memtable(devicename, memory_name, memory_current, memory_3h, memory_24h, memory_7d, memory_30d, memory_status)
                        VALUES(?,?,?,?,?,?,?,?)''', (self.devicename, memory, self.memory_current_list[enum], self.memory_3h_list[enum], self.memory_24h_list[enum], self.memory_7d_list[enum], self.memory_30d_list[enum], self.memory_status_list[enum],))
            except:
                cursor.execute('''INSERT INTO memtable(devicename, memory_name, memory_current, memory_3h, memory_24h, memory_7d, memory_30d, memory_status)
                        VALUES(?,?,?,?,?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))

        #DB THROUGHPUT BITS TABLE
        for enum, throughputbits in enumerate(self.throughput_bits_name_list):
            try:
                cursor.execute('''INSERT INTO throughputbitstable(devicename, throughput_bits_name, throughput_bits_current, throughput_bits_3h, throughput_bits_24h, throughput_bits_7d, throughput_bits_30d, throughput_bits_status)
                        VALUES(?,?,?,?,?,?,?,?)''', (self.devicename, throughputbits, self.throughput_bits_current_list[enum], self.throughput_bits_3h_list[enum], self.throughput_bits_24h_list[enum], self.throughput_bits_7d_list[enum], self.throughput_bits_30d_list[enum], self.throughput_bits_status_list[enum],))
            except:
                cursor.execute('''INSERT INTO throughputbitstable(devicename, throughput_bits_name, throughput_bits_current, throughput_bits_3h, throughput_bits_24h, throughput_bits_7d, throughput_bits_30d, throughput_bits_status)
                        VALUES(?,?,?,?,?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))

        #DB THROUGHPUT SSL TABLE
        for enum, throughputssl in enumerate(self.throughput_ssl_name_list):
            try:
                cursor.execute('''INSERT INTO throughputssltable(devicename, throughput_ssl_name, throughput_ssl_current, throughput_ssl_3h, throughput_ssl_24h, throughput_ssl_7d, throughput_ssl_30d, throughput_ssl_status)
                        VALUES(?,?,?,?,?,?,?,?)''', (self.devicename, throughputssl, self.throughput_ssl_current_list[enum], self.throughput_ssl_3h_list[enum], self.throughput_ssl_24h_list[enum], self.throughput_ssl_7d_list[enum], self.throughput_ssl_30d_list[enum], self.throughput_ssl_status_list[enum],))
            except:
                cursor.execute('''INSERT INTO throughputssltable(devicename, throughput_ssl_name, throughput_ssl_current, throughput_ssl_3h, throughput_ssl_24h, throughput_ssl_7d, throughput_ssl_30d, throughput_ssl_status)
                        VALUES(?,?,?,?,?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))

        #DB THROUGHPUT PACKET TABLE
        for enum, throughputpacket in enumerate(self.throughput_packet_name_list):
            try:
                cursor.execute('''INSERT INTO throughputpackettable(devicename, throughput_packet_name, throughput_packet_current, throughput_packet_3h, throughput_packet_24h, throughput_packet_7d, throughput_packet_30d, throughput_packet_status)
                        VALUES(?,?,?,?,?,?,?,?)''', (self.devicename, throughputpacket, self.throughput_packet_current_list[enum], self.throughput_packet_3h_list[enum], self.throughput_packet_24h_list[enum], self.throughput_packet_7d_list[enum], self.throughput_packet_30d_list[enum], self.throughput_packet_status_list[enum],))
            except:
                cursor.execute('''INSERT INTO throughputpackettable(devicename, throughput_packet_name, throughput_packet_current, throughput_packet_3h, throughput_packet_24h, throughput_packet_7d, throughput_packet_30d, throughput_packet_status)
                        VALUES(?,?,?,?,?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))

        #DB CONNECTION TABLE
        for enum, connection in enumerate(self.connection_name_list):
            try:
                cursor.execute('''INSERT INTO connectiontable(devicename, connection_name, connection_current, connection_3h, connection_24h, connection_7d, connection_30d, connection_status)
                        VALUES(?,?,?,?,?,?,?,?)''', (self.devicename, connection, self.connection_current_list[enum], self.connection_3h_list[enum], self.connection_24h_list[enum], self.connection_7d_list[enum], self.connection_30d_list[enum], self.connection_status_list[enum],))
            except:
                cursor.execute('''INSERT INTO connectiontable(devicename, connection_name, connection_current, connection_3h, connection_24h, connection_7d, connection_30d, connection_status)
                        VALUES(?,?,?,?,?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))

        #DB NEW CONNECTION TABLE
        for enum, newconnection in enumerate(self.newconnection_name_list):
            try:
                cursor.execute('''INSERT INTO newconnectiontable(devicename, newconnection_name, newconnection_current, newconnection_3h, newconnection_24h, newconnection_7d, newconnection_30d, newconnection_status)
                        VALUES(?,?,?,?,?,?,?,?)''', (self.devicename, newconnection, self.newconnection_current_list[enum], self.newconnection_3h_list[enum], self.newconnection_24h_list[enum], self.newconnection_7d_list[enum], self.newconnection_30d_list[enum], self.newconnection_status_list[enum],))
            except:
                cursor.execute('''INSERT INTO newconnectiontable(devicename, newconnection_name, newconnection_current, newconnection_3h, newconnection_24h, newconnection_7d, newconnection_30d, newconnection_status)
                        VALUES(?,?,?,?,?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))

        #DB HTTP REQUEST TABLE
        for enum, httprequests in enumerate(self.httprequests_name_list):
            try:
                cursor.execute('''INSERT INTO httprequeststable(devicename, httprequests_name, httprequests_current, httprequests_3h, httprequests_24h, httprequests_7d, httprequests_30d, httprequests_status)
                        VALUES(?,?,?,?,?,?,?,?)''', (self.devicename, httprequests, self.httprequests_current_list[enum], self.httprequests_3h_list[enum], self.httprequests_24h_list[enum], self.httprequests_7d_list[enum], self.httprequests_30d_list[enum], self.httprequests_status_list[enum],))
            except:
                cursor.execute('''INSERT INTO httprequeststable(devicename, httprequests_name, httprequests_current, httprequests_3h, httprequests_24h, httprequests_7d, httprequests_30d, httprequests_status)
                        VALUES(?,?,?,?,?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))

        #DB LTM TABLE
        for enum, ltm in enumerate(self.ltm_tipe_list):
            try:
                cursor.execute('''INSERT INTO ltmtable(devicename, ltm_tipe, ltm_name, availability, state)
                        VALUES(?,?,?,?,?)''', (self.devicename, ltm,  self.ltm_name_list[enum], self.availability_list[enum], self.state_list[enum],))
            except:
                cursor.execute('''INSERT INTO ltmtable(devicename, ltm_tipe, ltm_name, availability, state)
                        VALUES(?,?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))

        #DB HA TABLE
        for enum, device in enumerate(self.device_list):
            try:
                cursor.execute('''INSERT INTO hatable(devicename, device, mgmt_ip, configsync_ip, ha_state, ha_status)
                        VALUES(?,?,?,?,?,?)''', (self.devicename, device, self.mgmt_ip_list[enum], self.configsync_ip_list[enum], self.ha_state_list[enum], self.ha_status_list[enum],))
            except:
                cursor.execute('''INSERT INTO hatable(devicename, device, mgmt_ip, configsync_ip, ha_state, ha_status)
                        VALUES(?,?,?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))

        #DB SYNC HA
        try:
            cursor.execute('''INSERT INTO synchatable(devicename, sync_status, action)
                    VALUES(?,?,?)''', (self.devicename, self.sync_status, self.action))
        except:
            cursor.execute('''INSERT INTO synchatable(devicename, sync_status, action)
                    VALUES(?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))

        #DB TRAFFIC GROUP HA TABLE
        for enum, device in enumerate(self.device_traffic_list):
            try:
                cursor.execute('''INSERT INTO traffichatable(devicename, traffic_group, device_traffic, traffic_status, next_active, previous_active, active_reason)
                        VALUES(?,?,?,?,?,?,?)''', (self.devicename, self.traffic_group_list[enum], device, self.traffic_status_list[enum], self.next_active_list[enum], self.previous_active_list[enum], self.active_reason_list[enum],))
            except:
                cursor.execute('''INSERT INTO traffichatable(devicename, traffic_group, device_traffic, traffic_status, next_active, previous_active, active_reason)
                        VALUES(?,?,?,?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))

        #DB CERTIFICATE TABLE
        for enum, cert in enumerate(self.cert_name_list):
            try:
                cursor.execute('''INSERT INTO certtable(devicename, cert_name, cn, expired_date, cert_status)
                        VALUES(?,?,?,?,?)''', (self.devicename, cert, self.cn_list[enum], self.expired_date_list[enum], self.cert_status_list[enum],))
            except:
                cursor.execute('''INSERT INTO certtable(devicename, cert_name, cn, expired_date, cert_status)
                        VALUES(?,?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))

        #DB CERTIFICATE TABLE
        try:
            cursor.execute('''INSERT INTO lictable(devicename, lic_version, register_key, active_module, lic_on, svc_date, lic_status)
                    VALUES(?,?,?,?,?,?,?)''', (self.devicename, self.lic_version, self.register_key, self.active_module, self.lic_on, self.svc_date, self.lic_status))
        except:
            cursor.execute('''INSERT INTO lictable(devicename, lic_version, register_key, active_module, lic_on, svc_date, lic_status)
                    VALUES(?,?,?,?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))

        #DB LOG GENERATE REPORT
        try:
            cursor.execute('''INSERT INTO logtable(devicename, model, script)
                    VALUES(?,?,?)''', (self.devicename, self.model, self.device_class_name,))
        except:
            cursor.execute('''INSERT INTO logtable(devicename, model, script)
                    VALUES(?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error',self.file_name+'-'+'error',))
        
        db.commit()             
        db.close()