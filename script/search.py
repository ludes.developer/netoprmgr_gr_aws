import os
import pkg_resources
import xlrd
import xlsxwriter
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt; plt.rcdefaults()
from matplotlib import pyplot as plt
import matplotlib.cm as cm
from matplotlib.colors import Normalize
from numpy.random import rand
import numpy as np

data_path = pkg_resources.resource_filename('netoprmgr', 'static/')
data_path = os.path.join(data_path,'data/')

#Software Summary
chg_dir = os.chdir(data_path)
current_dir=os.getcwd()
support_dir = (data_path+'/template_chart.xlsx')
book = xlrd.open_workbook(support_dir)
first_sheet = book.sheet_by_index(0)
cell = first_sheet.cell(0,0)

version = []
percentage = []
total = []
count_total = 1
for i in range(first_sheet.nrows):
    try:
        total.append(int(first_sheet.row_values(count_total)[1]))
        count +=1
    except:
        pass

subtotal = (sum(total))

count_data = 1
for i in range(first_sheet.nrows):
    try:
        version.append(first_sheet.row_values(count_data)[0])
        percentage.append(int(first_sheet.row_values(count_data)[1])/int(subtotal)*100)
        count_data +=1
    except:
        pass

plt.style.use("fivethirtyeight")
#create chart
fig1, ax1 = plt.subplots()
ax1.pie(percentage, labels=version, autopct='%1.1f%%',
        shadow=True, startangle=90, rotatelabels =True)
ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
#plt.title("Software Summary")
#save image
print('Saving Image')
plt.savefig('Software Summary.png', bbox_inches='tight')
plt.close()
print('Image saved to Software Summary.png')
    
#Hardware Summary
chg_dir = os.chdir(data_path)
current_dir=os.getcwd()
support_dir = (data_path+'/template_chart.xlsx')
book = xlrd.open_workbook(support_dir)
first_sheet = book.sheet_by_index(1)
cell = first_sheet.cell(0,0)

model = []
percentage = []
total = []
count_total = 1
for i in range(first_sheet.nrows):
    try:
        total.append(int(first_sheet.row_values(count_total)[1]))
        count +=1
    except:
        pass

subtotal = (sum(total))

count_data = 1
for i in range(first_sheet.nrows):
    try:
        model.append(first_sheet.row_values(count_data)[0])
        percentage.append(int(first_sheet.row_values(count_data)[1])/int(subtotal)*100)
        count_data +=1
    except:
        pass

plt.style.use("fivethirtyeight")
#create chart
fig1, ax1 = plt.subplots()
ax1.pie(percentage, labels=model, autopct='%1.1f%%',
        shadow=True, startangle=90, rotatelabels =True)
ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
#plt.title("Hardware Summary")
#save image
print('Saving Image')
plt.savefig('Hardware Summary.png', bbox_inches='tight')
plt.close()
print('Image saved to Hardware Summary.png')

#CPU Summary
chg_dir = os.chdir(data_path)
current_dir=os.getcwd()
support_dir = (data_path+'/template_chart.xlsx')
book = xlrd.open_workbook(support_dir)
first_sheet = book.sheet_by_index(1)
cell = first_sheet.cell(0,0)

devicename = []
percentage = []
total = []
count_total = 1
for i in range(first_sheet.nrows):
    try:
        total.append(int(first_sheet.row_values(count_total)[1]))
        count +=1
    except:
        pass

subtotal = (sum(total))

count_data = 1
for i in range(first_sheet.nrows):
    try:
        devicename.append(first_sheet.row_values(count_data)[0])
        percentage.append(float(first_sheet.row_values(count_data)[1])/int(subtotal)*100)
        count_data +=1
    except:
        pass
print (devicename)
print (percentage)
#create chart
data = []
count_color = 1
for i in range(int(subtotal)):
    color_nmb = count_color
    data.append(color_nmb)
    count_color+=1
my_cmap = cm.get_cmap('jet')
my_norm = Normalize(vmin=0, vmax=int(subtotal))
plt.style.use('ggplot')
x_pos = [i for i, _ in enumerate(devicename)]
plt.bar(x_pos, percentage, color=my_cmap(my_norm(data)))
#plt.legend('test') #ini mungkin dipake
#plt.xlabel("Energy Source")
#plt.ylabel("Energy Output (GJ)")
plt.title("Processor CPU Utilization")
plt.xticks(x_pos, devicename, rotation='vertical')
#save image
print('Saving Image')
plt.savefig('CPU Summary.png', bbox_inches='tight')
plt.close()
print('Image saved to CPU Summary.png')

#Memory Summary
chg_dir = os.chdir(data_path)
current_dir=os.getcwd()
support_dir = (data_path+'/template_chart.xlsx')
book = xlrd.open_workbook(support_dir)
first_sheet = book.sheet_by_index(1)
cell = first_sheet.cell(0,0)

devicename = []
percentage = []
total = []
count_total = 1
for i in range(first_sheet.nrows):
    try:
        total.append(int(first_sheet.row_values(count_total)[1]))
        count +=1
    except:
        pass

subtotal = (sum(total))

count_data = 1
for i in range(first_sheet.nrows):
    try:
        devicename.append(first_sheet.row_values(count_data)[0])
        percentage.append(float(first_sheet.row_values(count_data)[1])/int(subtotal)*100)
        count_data +=1
    except:
        pass
print (devicename)
print (percentage)
#create chart
data = []
count_color = 1
for i in range(int(subtotal)):
    color_nmb = count_color
    data.append(color_nmb)
    count_color+=1
my_cmap = cm.get_cmap('jet')
my_norm = Normalize(vmin=0, vmax=int(subtotal))
plt.style.use('ggplot')
x_pos = [i for i, _ in enumerate(devicename)]
plt.bar(x_pos, percentage, color=my_cmap(my_norm(data)))
#plt.legend('test') #ini mungkin dipake
#plt.xlabel("Energy Source")
#plt.ylabel("Energy Output (GJ)")
plt.title("Memory Utilization")
plt.xticks(x_pos, devicename, rotation='vertical')
#save image
print('Saving Image')
plt.savefig('Memory Summary.png', bbox_inches='tight')
plt.close()
print('Image saved to Memory Summary.png')


