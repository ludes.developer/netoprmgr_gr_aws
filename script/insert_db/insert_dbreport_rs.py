import sqlite3
import re
import requests

class insert_dbreport_rs():    
    
    def __init__(self,file_name,devicename, model):
        self.file_name = file_name
        self.devicename = devicename
        self.model = model
        
    
        #open db connection
        self.db = sqlite3.connect('pmdb_rs')
        self.cursor = self.db.cursor()
        
    #close db
    def closeDB(self):
        self.db.commit()             
        self.db.close()

    #insert tipe
    def insert_tipe(self,tipe_list):
        #db tipe
        try:
            for enum, tipe in enumerate(tipe_list):
                self.cursor.execute('''INSERT INTO tipetable(tipe)
                        VALUES(?)''', (tipe,))
        except:
            self.cursor.execute('''INSERT INTO tipetable(tipe)
                    VALUES(?)''', (self.file_name+'-'+'error',))
        
    #insert Software
    def insert_software(self,version, iosversion, uptime, confreg, date_eos, date_last):
        #db swtable
        try:
            self.cursor.execute('''INSERT INTO swtable(devicename, model, iosversion, uptime, confreg, version, date_eos, date_last)
                    VALUES(?,?,?,?,?,?,?,?)''', (self.devicename,self.model,version,uptime,confreg,version,date_eos,date_last))
        except:
            self.cursor.execute('''INSERT INTO swtable(devicename, model, iosversion, uptime, confreg, version, date_eos, date_last)
                    VALUES(?,?,?,?,?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))
            
        #db swsumtable
        try:
            self.cursor.execute('''INSERT INTO swsumtable(version, date_eos, date_last)
                    VALUES(?,?,?)''', (version, date_eos, date_last))
        except:
            self.cursor.execute('''INSERT INTO swsumtable(version, date_eos, date_last)
                    VALUES(?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))
        
    #insert Hardware
    def insert_hardware(self, card, list_serial_number, list_hardware_description, date_list,list_card):
        #db hwsumtable
        try:
            self.cursor.execute('''INSERT INTO hwsumtable(model)
                    VALUES(?)''', (self.model,))

        except:
            self.cursor.execute('''INSERT INTO hwsumtable(model)
                    VALUES(?)''', (self.file_name+'-'+'error',))
        
        #db hwcardtable
        try:
            for enum, card in enumerate(list_card):
                self.cursor.execute('''INSERT INTO hwcardtable(devicename, model, card, sn, hwdscr, eosl_date)
                        VALUES(?,?,?,?,?,?)''', (self.devicename,self.model, card, list_serial_number[enum],list_hardware_description[enum], date_list[enum]))
        except:
            self.cursor.execute('''INSERT INTO hwcardtable(devicename, model, card, sn, hwdscr, eosl_date)
                    VALUES(?,?,?,?,?,?)''', (self.file_name+'-'+'error',self.file_name+'-'+'error',self.file_name+'-'+'error',self.file_name+'-'+'error',self.file_name+'-'+'error',self.file_name+'-'+'error',))
    
    #CPU
    def insert_cpu(self,total, process, interrupt, topcpu, status):
        try:
            status + "String"
            try:
                self.cursor.execute('''INSERT INTO cpusumtable(devicename, model, total, process, interrupt, topcpu, status)
                    VALUES(?,?,?,?,?,?,?)''', (self.devicename, self.model, total, process, interrupt, topcpu, status,))
            except:
                self.cursor.execute('''INSERT INTO cpusumtable(devicename, model, total, process, interrupt, topcpu, status)
                    VALUES(?,?,?,?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))            
            
        except:
            try:
                for enum, card in enumerate(process):
                    self.cursor.execute('''INSERT INTO cpusumtable(devicename, model, total, process, interrupt, topcpu, status)
                        VALUES(?,?,?,?,?,?,?)''', (self.devicename, self.model, total[enum], process[enum], interrupt[enum], topcpu[enum], status[enum],))
            except:
                self.cursor.execute('''INSERT INTO cpusumtable(devicename, model, total, process, interrupt, topcpu, status)
                        VALUES(?,?,?,?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))
    
    #Memory
    def insert_memory(self,memory_status,utils,topproc):
        #memory
        try:
            topproc + "String"
            try:
                self.cursor.execute('''INSERT INTO memsumtable(devicename, model, utils, topproc, status)
                        VALUES(?,?,?,?,?)''', (self.devicename, self.model, utils, topproc, memory_status,))
            except:
                self.cursor.execute('''INSERT INTO memsumtable(devicename, model, utils, topproc, status)
                        VALUES(?,?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))
        except:
            try:
                for enum, card in enumerate(topproc):
                    self.cursor.execute('''INSERT INTO memsumtable(devicename, model, utils, topproc, status)
                            VALUES(?,?,?,?,?)''', (self.devicename, self.model, utils[enum], topproc[enum], memory_status[enum],))
            except:
                self.cursor.execute('''INSERT INTO memsumtable(devicename, model, utils, topproc, status)
                        VALUES(?,?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))

    #db environment Power_Supply
    def insert_power_supply(self, psu, list_psu, list_psu_cond):
        try:
            count_sql = 0
            for psu in list_psu:
                self.cursor.execute('''INSERT INTO envtable(devicename, system, item, status)
                        VALUES(?,?,?,?)''', (self.devicename,'Power Supply',psu,list_psu_cond[count_sql],))
                count_sql+=1
        except:
            count_sql = 0
            for psu in list_psu:
                self.cursor.execute('''INSERT INTO envtable(devicename, system, item, status)
                        VALUES(?,?,?,?)''', (self.file_name+'-'+'error','Power Supply',self.file_name+'-'+'error',self.file_name+'-'+'error',))
                count_sql+=1
                
    #db Fan
    def insert_fan(self,fan, list_fan, list_fan_cond_cp):
        try:
            count_sql = 0
            for fan in list_fan:
                self.cursor.execute('''INSERT INTO envtable(devicename, system, item, status)
                        VALUES(?,?,?,?)''', (self.devicename,'Fan',fan,list_fan_cond_cp[count_sql],))
                count_sql+=1
        except:
            count_sql = 0
            for fan in list_fan:
                self.cursor.execute('''INSERT INTO envtable(devicename, system, item, status)
                        VALUES(?,?,?,?)''', (self.file_name+'-'+'error','Fan',self.file_name+'-'+'error',self.file_name+'-'+'error',))
                count_sql+=1
    
    #db templatur
    def insert_temp(self,list_temp, list_temp_cond):
        try:
            count_sql = 0
            for temp in list_temp:
                self.cursor.execute('''INSERT INTO envtable(devicename, system, item, status)
                        VALUES(?,?,?,?)''', (self.devicename,'Temperature',temp,list_temp_cond[count_sql],))
                count_sql+=1
        except:
            count_sql = 0
            for temp in self.list_temp:
                self.cursor.execute('''INSERT INTO envtable(devicename, system, item, status)
                        VALUES(?,?,?,?)''', (self.file_name+'-'+'error','Temperature',self.file_name+'-'+'error',self.file_name+'-'+'error',))
                count_sql+=1
    
    #db eos
    def insert_eos(self,eos_date, eos_card):
        #db eos
        try:
            for enum, card in enumerate(eos_card):
                self.cursor.execute('''INSERT INTO eosltable(model, card, date)
                        VALUES(?,?,?)''', (self.model, card, eos_date[enum],))
        except:
            self.cursor.execute('''INSERT INTO eosltable(model, card, date)
                    VALUES(?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))
            
    #db log 
    def insert_log(self,device_class_name):
        #LOG Checking
        try:
            self.cursor.execute('''INSERT INTO logtable(devicename, model, script)
                    VALUES(?,?,?)''', (self.devicename, self.model, device_class_name,))
        except:
            self.cursor.execute('''INSERT INTO logtable(devicename, model, script)
                    VALUES(?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error',self.file_name+'-'+'error',))
    
    #db insert_file_raitin
    def insert_file_rating(self,version, devicename, model, iosversion, uptime, confreg, card, list_serial_number, list_hardware_description, date_list,total, process, interrupt, topcpu, status, utils, topproc,memory_status, psu, list_psu, list_psu_cond, fan, list_fan, list_fan_cond_cp,list_temp, list_temp_cond, eos_date, device_class_name, eos_card, file_name, list_card, tipe_list):
        #File Rating
        file_rating = 0
        missing_section = []
        if version == "-":
            file_rating+=1
            missing_section.append("version")
        if devicename == "-":
            file_rating+=1
            missing_section.append("devicename")
        if model == "-":
            file_rating+=1
            missing_section.append("model")
        if iosversion == "-":
            file_rating+=1
            missing_section.append("iosversion")
        if uptime == "-":
            file_rating+=1
            missing_section.append("uptime")
        if confreg == "-":
            file_rating+=1
            missing_section.append("confreg")
        if card == "-":
            file_rating+=1
            missing_section.append("card")
        if list_serial_number == "-":
            file_rating+=1
            missing_section.append("list_serial_number")
        if list_hardware_description == "-":
            file_rating+=1
            missing_section.append("list_hardware_description")
        if date_list == "-":
            file_rating+=1
            missing_section.append("date_list")
        if total == "-":
            file_rating+=1
            missing_section.append("total")
        if process == "-":
            file_rating+=1
            missing_section.append("process")
        if interrupt == "-":
            file_rating+=1
            missing_section.append("interrupt")
        if topcpu == "-":
            file_rating+=1
            missing_section.append("topcpu")
        if status == "-":
            file_rating+=1
            missing_section.append("status")
        if utils == "-":
            file_rating+=1
            missing_section.append("utils")
        if topproc == "-":
            file_rating+=1
            missing_section.append("topproc")
        if memory_status == "-":
            file_rating+=1
            missing_section.append("memory_status")
        if psu == "-":
            file_rating+=1
            missing_section.append("psu")
        if list_psu == "-":
            file_rating+=1
            missing_section.append("list_psu")
        if list_psu_cond == "-":
            file_rating+=1
            missing_section.append("list_psu_cond")
        if fan == "-":
            file_rating+=1
            missing_section.append("fan")
        if list_fan == "-":
            file_rating+=1
            missing_section.append("list_fan")
        if list_fan_cond_cp == "-":
            file_rating+=1
            missing_section.append("list_fan_cond_cp")
        if list_temp == "-":
            file_rating+=1
            missing_section.append("list_temp")
        if list_temp_cond == "-":
            file_rating+=1
            missing_section.append("list_temp_cond")
        if eos_card == "-":
            file_rating+=1
            missing_section.append("eos_card")
        if device_class_name == "-":
            file_rating+=1
            missing_section.append("device_class_name")
        if file_name == "-":
            file_rating+=1
            missing_section.append("file_name")
        if list_card == "-":
            file_rating+=1
            missing_section.append("list_card")
        if eos_date == "-":
            file_rating+=1
            missing_section.append("eos_date")
        if tipe_list == "-":
            file_rating+=1
            missing_section.append("tipe_list")
        join_missing_section = ",".join(missing_section)
        print(file_rating)
        print(join_missing_section)
        print(file_name)
        print(device_class_name)
        try:
            self.cursor.execute('''INSERT INTO filerating(filename, deviceclassname, missingsection, totalrating)
                    VALUES(?,?,?,?)''', (self.file_name, device_class_name, join_missing_section, file_rating,))
        except:
            self.cursor.execute('''INSERT INTO filerating(filename, deviceclassname, missingsection, totalrating)
                    VALUES(?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error',self.file_name+'-'+'error',self.file_name+'-'+'error',))
            