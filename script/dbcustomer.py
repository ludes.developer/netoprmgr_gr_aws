import os
import sqlite3
import pkg_resources
import pymysql
from datetime import datetime
from netoprmgr.script.ip_variables import dbcustomer_ip

capture_path = pkg_resources.resource_filename('netoprmgr', 'static/')
capture_path = os.path.join(capture_path, 'capture/')
BASE_DIR = pkg_resources.resource_filename('netoprmgr', '')
os.chdir(BASE_DIR)
SCRIPT_DIR = os.path.join(BASE_DIR, 'script')


class dbcustomer:
    def __init__(self, customer):
        self.customer = customer

    def dbcustomer(self):
        # get data generate by
        chg_dir = os.chdir(SCRIPT_DIR)
        current_dir = os.getcwd()
        read_file = open('active_login_count.py', 'r')
        read_file_list = read_file.readlines()
        if len(read_file_list) == 0:
            data_generate_by = 'Developer'
        else:
            data_generate_by = read_file_list[0]

        chg_dir = os.chdir(capture_path)

        # SQL QUERY RS
        db = sqlite3.connect('pmdb_rs')
        cursor = db.cursor()
        cursor.execute('''SELECT DISTINCT hwcardtable.devicename, hwcardtable.model, hwcardtable.card, hwcardtable.sn, swtable.version
            FROM hwcardtable
            INNER JOIN swtable
            ON hwcardtable.devicename = swtable.devicename''')
        records = cursor.fetchall()
        # connect to remote db for dbcustomer
        dbcustomer = pymysql.connect(
            db='db_portal', user='root', passwd='N3t0prmgr!', host=dbcustomer_ip, port=3306)
        cursor_customer = dbcustomer.cursor()
        for record in records:
            cursor_check = dbcustomer.cursor()
            cursor_check.execute("""SELECT * from customer_customer where device_name=%s and model=%s and card=%s and sn=%s and version=%s""",
                                 (record[0], record[1], record[2], record[3], record[4]))
            check_records = cursor_check.fetchall()
            if len(check_records) == 0:
                sql = "INSERT INTO customer_customer (customer_name, device_name, model, card, sn, version, date_generated, generated_by) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
                val = (self.customer, record[0], record[1], record[2],
                       record[3], record[4], datetime.now(), data_generate_by)
                cursor_customer.execute(sql, val)
            else:
                print('Data Duplicate, Not Writing to Customer DB')
        dbcustomer.commit()
        dbcustomer.close()

        # CLOSE DB RS
        db.close()

        # SQL QUERY UCM
        dbucm = sqlite3.connect('pmdb_ucm')
        cursor = dbucm.cursor()
        cursor.execute('''SELECT DISTINCT swtable.devicename, swtable.model, swtable.version, hwtable.sn
            FROM swtable
            INNER JOIN hwtable
            ON hwtable.devicename = swtable.devicename''')
        records = cursor.fetchall()
        # connect to remote db for dbcustomer
        dbcustomer = pymysql.connect(
            db='db_portal', user='root', passwd='N3t0prmgr!', host=dbcustomer_ip, port=3306)
        cursor_customer = dbcustomer.cursor()
        for record in records:
            cursor_check = dbcustomer.cursor()
            cursor_check.execute("""SELECT * from customer_customer where device_name=%s and model=%s and card=%s and sn=%s and version=%s""",
                                 (record[0], record[1], '', record[3], record[2]))
            check_records = cursor_check.fetchall()
            if len(check_records) == 0:
                sql = "INSERT INTO customer_customer (customer_name, device_name, model, card, sn, version, date_generated, generated_by) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
                val = (self.customer, record[0], record[1], '',
                       record[3], record[2], datetime.now(), data_generate_by)
                cursor_customer.execute(sql, val)
            else:
                print('Data Duplicate, Not Writing to Customer DB')
        dbcustomer.commit()
        dbcustomer.close()

        # CLOSE DB CUCM
        dbucm.close()

        # SQL QUERY F5
        dbf5 = sqlite3.connect('pmdb_f5')
        cursor = dbf5.cursor()
        cursor.execute('''SELECT DISTINCT swtable.devicename, swtable.model, swtable.version, hwcardtable.sn
            FROM swtable
            INNER JOIN hwcardtable
            ON hwcardtable.devicename = swtable.devicename''')
        records = cursor.fetchall()
        # connect to remote db for dbcustomer
        dbcustomer = pymysql.connect(
            db='db_portal', user='root', passwd='N3t0prmgr!', host=dbcustomer_ip, port=3306)
        cursor_customer = dbcustomer.cursor()
        for record in records:
            cursor_check = dbcustomer.cursor()
            cursor_check.execute("""SELECT * from customer_customer where device_name=%s and model=%s and card=%s and sn=%s and version=%s""",
                                 (record[0], record[1], '', record[3], record[2]))
            check_records = cursor_check.fetchall()
            if len(check_records) == 0:
                sql = "INSERT INTO customer_customer (customer_name, device_name, model, card, sn, version, date_generated, generated_by) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
                val = (self.customer, record[0], record[1], '',
                       record[3], record[2], datetime.now(), data_generate_by)
                cursor_customer.execute(sql, val)
            else:
                print('Data Duplicate, Not Writing to Customer DB')
        dbcustomer.commit()
        dbcustomer.close()

        # CLOSE DB FORTIGATE
        dbf5.close()

        # SQL QUERY FORTIGATE
        dbfortigate = sqlite3.connect('pmdb_fortigate')
        cursor = dbfortigate.cursor()
        cursor.execute('''SELECT DISTINCT devicename, model, card, sn, version FROM portaltable''')
        records = cursor.fetchall()
        # connect to remote db for dbcustomer
        dbcustomer = pymysql.connect(
            db='db_portal', user='root', passwd='N3t0prmgr!', host=dbcustomer_ip, port=3306)
        cursor_customer = dbcustomer.cursor()
        for record in records:
            cursor_check = dbcustomer.cursor()
            cursor_check.execute("""SELECT * from customer_customer where device_name=%s and model=%s and card=%s and sn=%s and version=%s""",
                                 (record[0], record[1], '', record[3], record[2]))
            check_records = cursor_check.fetchall()
            if len(check_records) == 0:
                sql = "INSERT INTO customer_customer (customer_name, device_name, model, card, sn, version, date_generated, generated_by) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
                val = (self.customer, record[0], record[1], '',
                       record[3], record[2], datetime.now(), data_generate_by)
                cursor_customer.execute(sql, val)
            else:
                print('Data Duplicate, Not Writing to Customer DB')
        dbcustomer.commit()
        dbcustomer.close()

        # CLOSE DB FORTIGATE
        dbfortigate.close()
