import sqlite3

class insert_dbreport_ucm:
    def __init__(self,
                devicename, device_type, hardware_type, sn, eos_hardware,
                model, iosversion, uptime, confreg, version, date_eos, date_last,
                cpu, top_cpu, cpu_status,
                memory, top_memory, memory_status,
                disk_name_list, disk_total_list, disk_free_list, disk_used_list, disk_percentage_list, disk_status,
                last_date_backup, type_backup, status_backup, fitur_backup, failed_backup,
                schedule_backup,
                cert_name_list, from_date_list, to_date_list, cert_method_list, cert_status_list,
                node_list, replication_list, rep_status_list,
                alert_log, severity,
                tipe_list,
                list_value_phone, list_firmware, eos_phone,
                list_status_phone, list_desc_phone, list_name_phone,
                list_name_ext, list_ext_phone, list_status_ext,
                lic_name, list_lic_detail, list_lic_count,
                list_app_name, list_app_enable, list_app_ses,
                list_script_name,
                list_promp_name,
                list_triger_no, list_triger_name, list_triger_enable, list_triger_ses,
                list_message_user, list_message, list_message_inbox, list_message_delete,
                list_userid,
                device_class_name,
                file_name
    ):
        self.devicename = devicename
        self.device_type = device_type
        self.hardware_type = hardware_type
        self.sn = sn
        self.eos_hardware = eos_hardware
        self.model = model
        self.iosversion = iosversion
        self.uptime = uptime
        self.confreg = confreg
        self.version = version
        self.date_eos = date_eos
        self.date_last = date_last
        self.cpu = cpu
        self.top_cpu = top_cpu
        self.cpu_status = cpu_status
        self.memory = memory
        self.top_memory = top_memory
        self.memory_status = memory_status
        self.disk_name_list = disk_name_list
        self.disk_total_list = disk_total_list
        self.disk_free_list = disk_free_list
        self.disk_used_list = disk_used_list
        self.disk_percentage_list = disk_percentage_list
        self.disk_status = disk_status
        self.last_date_backup = last_date_backup
        self.type_backup = type_backup
        self.status_backup = status_backup
        self.fitur_backup = fitur_backup
        self.failed_backup = failed_backup
        self.schedule_backup = schedule_backup
        self.cert_name_list = cert_name_list
        self.from_date_list = from_date_list
        self.to_date_list = to_date_list
        self.cert_method_list = cert_method_list
        self.cert_status_list = cert_status_list
        self.node_list = node_list
        self.replication_list = replication_list
        self.rep_status_list = rep_status_list
        self.alert_log = alert_log
        self.severity = severity
        self.tipe_list = tipe_list
        self.list_value_phone = list_value_phone
        self.list_firmware = list_firmware
        self.eos_phone = eos_phone
        self.list_status_phone = list_status_phone
        self.list_desc_phone = list_desc_phone
        self.list_name_phone = list_name_phone
        self.list_name_ext = list_name_ext
        self.list_ext_phone = list_ext_phone
        self.list_status_ext = list_status_ext
        self.lic_name = lic_name
        self.list_lic_detail = list_lic_detail
        self.list_lic_count = list_lic_count
        self.list_app_name = list_app_name
        self.list_app_enable = list_app_enable
        self.list_app_ses = list_app_ses
        self.list_script_name = list_script_name
        self.list_promp_name = list_promp_name
        self.list_triger_no = list_triger_no
        self.list_triger_name = list_triger_name
        self.list_triger_enable = list_triger_enable
        self.list_triger_ses = list_triger_ses
        self.list_message_user = list_message_user
        self.list_message = list_message
        self.list_message_inbox = list_message_inbox
        self.list_message_delete = list_message_delete
        self.list_userid = list_userid
        self.device_class_name = device_class_name
        self.file_name = file_name

    def insert_dbreport_ucm(self):
        #open db connection
        db = sqlite3.connect('pmdb_ucm')
        cursor = db.cursor()

        #DB TIPE TABLE
        try:
            for enum, tipe in enumerate(self.tipe_list):
                cursor.execute('''INSERT INTO tipetable(tipe)
                        VALUES(?)''', (tipe,))
        except:
            cursor.execute('''INSERT INTO tipetable(tipe)
                    VALUES(?)''', (self.file_name+'-'+'error',))

        #DB SOFTWARE TABLE
        try:
            cursor.execute('''INSERT INTO swtable(devicename, model, iosversion, uptime, confreg, version, date_eos, date_last)
                    VALUES(?,?,?,?,?,?,?,?)''', (self.devicename, self.model, self.version, self.uptime, self.confreg, self.version, self.date_eos, self.date_last))
        except:
            cursor.execute('''INSERT INTO swtable(devicename, model, iosversion, uptime, confreg, version, date_eos, date_last)
                    VALUES(?,?,?,?,?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))
        try:
            cursor.execute('''INSERT INTO swsumtable(version, date_eos, date_last)
                    VALUES(?,?,?)''', (self.version, self.date_eos, self.date_last))
        except:
            cursor.execute('''INSERT INTO swsumtable(version, date_eos, date_last)
                    VALUES(?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))
    
        #DB HARDWARE TABLE
        try:
            cursor.execute('''INSERT INTO hwtable(devicename, device_type, hardware_type, sn, eos_hardware)
                    VALUES(?,?,?,?,?)''', (self.devicename, self.device_type, self.hardware_type, self.sn, self.eos_hardware,))
        except:
            cursor.execute('''INSERT INTO hwtable(devicename, device_type, hardware_type, sn, eos_hardware)
                    VALUES(?,?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))
        
        #DB CPU SUMMARY TABLE
        try:
            cursor.execute('''INSERT INTO cputable(devicename, cpu, top_cpu, cpu_status)
                    VALUES(?,?,?,?)''', (self.devicename, self.cpu, self.top_cpu, self.cpu_status,))
        except:
            cursor.execute('''INSERT INTO cputable(devicename, cpu, top_cpu, cpu_status)
                    VALUES(?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))            
        
        #DB MEMORY SUMMARY TABLE
        try:
            cursor.execute('''INSERT INTO memtable(devicename, memory, top_memory, memory_status)
                    VALUES(?,?,?,?)''', (self.devicename, self.memory, self.top_memory, self.memory_status,))
        except:
            cursor.execute('''INSERT INTO memtable(devicename, memory, top_memory, memory_status)
                    VALUES(?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))

        #DB DISK SUMMARY TABLE
        for enum, disk in enumerate(self.disk_name_list):
            try:
                cursor.execute('''INSERT INTO disktable(devicename, disk_name, disk_total, disk_free, disk_used, disk_percentage, disk_status)
                        VALUES(?,?,?,?,?,?,?)''', (self.devicename, disk, self.disk_total_list[enum], self.disk_free_list[enum], self.disk_used_list[enum], self.disk_percentage_list[enum], self.disk_status[enum],))
            except:
                cursor.execute('''INSERT INTO disktable(devicename, disk_name, disk_total, disk_free, disk_used, disk_percentage)
                        VALUES(?,?,?,?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))

        #DB BACKUP SUMMARY TABLE
        try:
            cursor.execute('''INSERT INTO backuptable(devicename, last_date_backup, type_backup, status_backup, schedule_backup, fitur_backup, failed_backup)
                    VALUES(?,?,?,?,?,?,?)''', (self.devicename, self.last_date_backup, self.type_backup, self.status_backup, self.schedule_backup, self.fitur_backup, self.failed_backup,))
        except:
            cursor.execute('''INSERT INTO backuptable(devicename, last_date_backup, type_backup, status_backup, schedule_backup, fitur_backup, failed_backup)
                    VALUES(?,?,?,?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))

        #DB CERTIFICATE SUMMARY TABLE
        for enum, certificate in enumerate(self.cert_name_list):
            try:
                cursor.execute('''INSERT INTO certtable(devicename, certificate_name, from_date, to_date, cert_method, cert_status)
                        VALUES(?,?,?,?,?,?)''', (self.devicename, certificate, self.from_date_list[enum], self.to_date_list[enum], self.cert_method_list[enum], self.cert_status_list[enum],))
            except:
                cursor.execute('''INSERT INTO certtable(devicename, certificate_name, from_date, to_date, cert_method, cert_status)
                        VALUES(?,?,?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))

        #DB REPLICATION STATUS SUMMARY TABLE
        for enum, rep in enumerate(self.replication_list):
            try:
                cursor.execute('''INSERT INTO reptable(devicename, node, replication, rep_status)
                        VALUES(?,?,?,?)''', (self.devicename, self.node_list[enum], rep, self.rep_status_list[enum],))
            except:
                cursor.execute('''INSERT INTO reptable(devicename, node, replication, rep_status)
                        VALUES(?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))

        #DB ALERT AND LOG SUMMARY TABLE
        try:
            cursor.execute('''INSERT INTO alerttable(devicename, alert_log, severity)
                    VALUES(?,?,?)''', (self.devicename, self.alert_log, self.severity,))
        except:
            cursor.execute('''INSERT INTO alerttable(devicename, alert_log, severity)
                    VALUES(?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))
        
        #DB LOG GENERATE REPORT
        try:
            cursor.execute('''INSERT INTO logtable(devicename, model, script)
                    VALUES(?,?,?)''', (self.devicename, self.model, self.device_class_name,))
        except:
            cursor.execute('''INSERT INTO logtable(devicename, model, script)
                    VALUES(?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error',self.file_name+'-'+'error',))
        
        #DB FIRMWARE PHONE TABLE
        for enum, devicetype in enumerate(self.list_value_phone):
            try:
                cursor.execute('''INSERT INTO firmwaretable(devicetype, firmware, latest, eos_phone)
                        VALUES(?,?,?,?)''', (devicetype, self.list_firmware[enum], '', '',))
            except:
                cursor.execute('''INSERT INTO firmwaretable(devicetype, firmware, latest, eos_phone)
                        VALUES(?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))
        
        #DB STATUS PHONE TABLE
        for enum, devicetype in enumerate(self.list_value_phone):
            try:
                cursor.execute('''INSERT INTO phonetable(devicename, devicetype, status_phone, desc_phone)
                        VALUES(?,?,?,?)''', (self.list_name_phone[enum], devicetype, self.list_status_phone[enum], self.list_desc_phone[enum],))
            except:
                cursor.execute('''INSERT INTO phonetable(devicename, devicetype, status_phone, desc_phone)
                        VALUES(?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))
        
        #DB STATUS EXT TABLE
        for enum, ext in enumerate(self.list_ext_phone):
            try:
                cursor.execute('''INSERT INTO exttable(devicename, ext_phone, status_ext)
                        VALUES(?,?,?)''', (self.list_name_ext[enum], ext, self.list_status_ext[enum],))
            except:
                cursor.execute('''INSERT INTO exttable(devicename, ext_phone, status_ext)
                        VALUES(?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))
        
        #DB LICENSE TABLE
        for enum, lic in enumerate(self.list_lic_detail):
            try:
                cursor.execute('''INSERT INTO lictable(devicename, lic_name, lic_detail, lic_count)
                        VALUES(?,?,?,?)''', (self.devicename, self.lic_name, lic, self.list_lic_count[enum],))
            except:
                cursor.execute('''INSERT INTO lictable(devicename, lic_name, lic_detail, lic_count)
                        VALUES(?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))
        
        #DB CCX APLICATION NAME TABLE
        for enum, app in enumerate(self.list_app_name):
            try:
                cursor.execute('''INSERT INTO ccxapptable(devicename, app_name, app_enable, app_ses)
                        VALUES(?,?,?,?)''', (self.devicename, app, self.list_app_enable[enum], self.list_app_ses[enum],))
            except:
                cursor.execute('''INSERT INTO ccxapptable(devicename, app_name, app_enable, app_ses)
                        VALUES(?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))
        
        #DB CCX SCRIPT NAME TABLE
        for enum, script in enumerate(self.list_script_name):
            try:
                cursor.execute('''INSERT INTO ccxscripttable(devicename, script_name)
                        VALUES(?,?)''', (self.devicename, script,))
            except:
                cursor.execute('''INSERT INTO ccxscripttable(devicename, script_name)
                        VALUES(?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error',))
        
        #DB CCX PROMPT NAME TABLE
        for enum, promp in enumerate(self.list_promp_name):
            try:
                cursor.execute('''INSERT INTO ccxpromptable(devicename, promp_name)
                        VALUES(?,?)''', (self.devicename, promp,))
            except:
                cursor.execute('''INSERT INTO ccxpromptable(devicename, promp_name)
                        VALUES(?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error',))
        
        #DB CCX TRIGER NUMBER TABLE
        for enum, triger in enumerate(self.list_triger_name):
            try:
                cursor.execute('''INSERT INTO ccxtrigertable(devicename, triger_no, triger_name, triger_enable, triger_ses)
                        VALUES(?,?,?,?,?)''', (self.devicename, self.list_triger_no[enum], triger, self.list_triger_enable[enum], self.list_triger_ses[enum],))
            except:
                cursor.execute('''INSERT INTO ccxtrigertable(devicename, triger_no, triger_name, triger_enable, triger_ses)
                        VALUES(?,?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))
        
        #DB CUCN MESSAGE TABLE
        for enum, user in enumerate(self.list_message_user):
            try:
                cursor.execute('''INSERT INTO cucnmessagetable(devicename, message_user, message, message_inbox, message_delete)
                        VALUES(?,?,?,?,?)''', (self.devicename, user, self.list_message[enum], self.list_message_inbox[enum], self.list_message_delete[enum],))
            except:
                cursor.execute('''INSERT INTO cucnmessagetable(devicename, message_user, message, message_inbox, message_delete)
                        VALUES(?,?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))
        
        #DB CUCN USERID TABLE
        for enum, userid in enumerate(self.list_userid):
            try:
                cursor.execute('''INSERT INTO cucnuseridtable(devicename, userid)
                        VALUES(?,?)''', (self.devicename, userid,))
            except:
                cursor.execute('''INSERT INTO cucnuseridtable(devicename, userid)
                        VALUES(?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error',))
        
        db.commit()             
        db.close()