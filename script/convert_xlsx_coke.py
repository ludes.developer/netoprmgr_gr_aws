import sqlite3
import re
import pkg_resources
import os
import xlsxwriter

class convert_xlsx_coke:
    @staticmethod
    def convert_xlsx_coke():
        result_path = pkg_resources.resource_filename('netoprmgr', 'static/')
        result_path = os.path.join(result_path,'result/')

        print('')
        print('Processing Document')

        #open db connection
        db = sqlite3.connect('pmdb_rs')
        cursor = db.cursor()

        #BUG SUMMARY
        #sql query
        cursor.execute('''SELECT model, version FROM swtable ORDER BY model, version ASC''')
        records = cursor.fetchall()
        #define workbook
        workbook = xlsxwriter.Workbook('routing_switching.xlsx')

        #format title
        format1 = workbook.add_format({
                                'font_name' : 'Arial',
                                'font_size' : 12,
                                'bold' : 1,
                                'text_wrap': True
                                })
        #format header table
        format2 = workbook.add_format({
                                'font_name' : 'Arial',
                                'font_size' : 11,
                                'bold' : 1,
                                'border': 1,
                                'align' : 'center',
                                'valign' : 'vcenter',
                                'fg_color' : '#00659C',
                                'font_color': 'white',
                                'text_wrap': True
                                })
        #format number header
        format3 = workbook.add_format({
                                'font_name' : 'Arial',
                                'font_size' : 10,
                                'bold' : 1,
                                'border': 1,
                                'align' : 'center',
                                'valign' : 'vcenter',
                                'fg_color' : '#00659C',
                                'font_color': 'white',
                                'text_wrap': True
                                })
        #format data table
        format4 = workbook.add_format({
                                'font_name' : 'Arial',
                                'font_size' : 10,
                                'border': 1,
                                'align' : 'center',
                                'valign' : 'vcenter',
                                'text_wrap': True
                                })
        #format for merge
        merge_format = workbook.add_format({
                                'font_name' : 'Arial',
                                'font_size' : 11,
                                'bold': 1,
                                'border': 1,
                                'align': 'center',
                                'valign': 'vcenter',
                                'fg_color': '#00659C',
                                'font_color': 'white',
                                'text_wrap': True
                                })
        #create sheet
        worksheet = workbook.add_worksheet('BUG SUMMARY')
        #header section
        worksheet.merge_range(0, 0, 0, 10, 'BUG SUMMARY',format1)
        worksheet.merge_range(1, 0, 2, 0, 'Device', merge_format)
        worksheet.merge_range(1, 1, 2, 1, 'Version', merge_format)
        worksheet.merge_range(1, 2, 1, 7, 'Count of Bug Severity', merge_format)
        worksheet.merge_range(1, 8, 2, 8, 'Grand Total', merge_format)
        worksheet.merge_range(1, 9, 2, 9, 'Suggested Release Version', merge_format)
        worksheet.merge_range(1, 10, 2, 10, 'Suggestion', merge_format)

        worksheet.write(2,2,'1',format3)
        worksheet.write(2,3,'2',format3)
        worksheet.write(2,4,'3',format3)
        worksheet.write(2,5,'4',format3)
        worksheet.write(2,6,'5',format3)
        worksheet.write(2,7,'6',format3)

        #data section
        row_check = 'ludesdeveloper'
        row_version = 'ludesdeveloper'
        iteration_check = False
        count_row = 3
        for row in records:
            if row_check not in row[0] and iteration_check == False:

                row_check = row[0]
                row_version = row[1]
                iteration_check = False
                start_row = count_row

                worksheet.write(count_row,0,row[0],format4)
                worksheet.write(count_row,1,row[1],format4)
                worksheet.write(count_row,2,'',format4)
                worksheet.write(count_row,3,'',format4)
                worksheet.write(count_row,4,'',format4)
                worksheet.write(count_row,5,'',format4)
                worksheet.write(count_row,6,'',format4)
                worksheet.write(count_row,7,'',format4)
                worksheet.write(count_row,8,'',format4)
                worksheet.write(count_row,9,'',format4)
                worksheet.write(count_row,10,'',format4)
                iteration_check == True
                count_row+=1

            elif row_check in row[0] and row_version not in row[1] and iteration_check == False:
                row_check = row[0]
                row_version = row[1]

                worksheet.write(count_row,1,row[1],format4)
                worksheet.write(count_row,2,'',format4)
                worksheet.write(count_row,3,'',format4)
                worksheet.write(count_row,4,'',format4)
                worksheet.write(count_row,5,'',format4)
                worksheet.write(count_row,6,'',format4)
                worksheet.write(count_row,7,'',format4)
                worksheet.write(count_row,8,'',format4)
                worksheet.write(count_row,9,'',format4)
                worksheet.write(count_row,10,'',format4)
                end_row = count_row
                worksheet.merge_range(start_row, 0, end_row, 0, row[0], format4)
                iteration_check == True
                count_row+=1

        #SOFTWARE SUMMARY TABLE
        #sql query
        cursor.execute('''SELECT version, COUNT(*) FROM swsumtable GROUP BY version''')
        records = cursor.fetchall()
        cursor.execute('''SELECT COUNT(version) FROM swsumtable''')
        total = cursor.fetchall()
        total=(str(total))
        total=re.sub("\D", "", total)
        total=int(total)
        #create sheet
        worksheet = workbook.add_worksheet('SOFTWARE TABLE SUMMARY')
        
        #header section
        worksheet.merge_range(0, 0, 0, 4, 'SOFTWARE TABLE SUMMARY',format1)
        worksheet.write(1,0,'Version',format2)
        worksheet.write(1,1,'Total',format2)
        worksheet.write(1,2,'Percentage',format2)
        worksheet.write(1,3,'End of Software Maintenance',format2)
        worksheet.write(1,4,'Last Date of Support',format2)
        #header for chart
        worksheet.merge_range(0, 6, 0, 7, 'For Chart',format1)
        worksheet.write(1,6,'Version',format4)
        worksheet.write(1,7,'Percentage',format4)

        #data section
        count_row = 2
        for row in records:
            worksheet.write(count_row,0,row[0],format4)
            worksheet.write(count_row,1,str(row[1]),format4)
            percentage = (str((row[1]/total)*100))
            percentage = re.findall('\d+[.]\d',percentage)
            percentage = percentage[0]
            worksheet.write(count_row,2,(percentage+'%'),format4)
            worksheet.write(count_row,3,'',format4)
            worksheet.write(count_row,4,'',format4)
            worksheet.write(count_row,6,row[0],format4)
            try:
                worksheet.write(count_row,7,float(percentage),format4)
            except:
                worksheet.write(count_row,7,"-",format4)
            count_row += 1

        #chart section
        software_chart = workbook.add_chart({'type': 'pie'})
        software_chart.add_series({
            "categories": ["SOFTWARE TABLE SUMMARY", 2, 6, count_row-1, 6],
            "values":     ["SOFTWARE TABLE SUMMARY", 2, 7, count_row-1, 7],
            "data_labels": {"value": True},
        })
        worksheet.insert_chart('J2', software_chart)

        #SOFTWARE TABLE
        #sql query
        cursor.execute('''SELECT devicename, model, iosversion, uptime, confreg FROM swtable''')
        records = cursor.fetchall()
        #create sheet
        worksheet = workbook.add_worksheet('SOFTWARE TABLE')
        
        #header section
        worksheet.merge_range(0, 0, 0, 4, 'SOFTWARE TABLE',format1)
        worksheet.write(1,0,'Device Name',format2)
        worksheet.write(1,1,'Model',format2)
        worksheet.write(1,2,'IOS Version',format2)
        worksheet.write(1,3,'Uptime',format2)
        worksheet.write(1,4,'Config Register',format2)

        #data section
        count_row = 2
        for row in records:
            worksheet.write(count_row,0,row[0],format4)
            worksheet.write(count_row,1,row[1],format4)
            worksheet.write(count_row,2,row[2],format4)
            worksheet.write(count_row,3,row[3],format4)
            worksheet.write(count_row,4,row[4],format4)
            count_row += 1

        #HARDWARE SUMMARY TABLE
        #sql query
        cursor.execute('''SELECT card, date, COUNT(*) FROM eosltable GROUP BY card''')
        records = cursor.fetchall()
        cursor.execute('''SELECT COUNT(card) FROM eosltable''')
        total = cursor.fetchall()
        total=(str(total))
        total=re.sub("\D", "", total)
        total=int(total)
        #create sheet
        worksheet = workbook.add_worksheet('HARDWARE SUMMARY TABLE')
        
        #header section
        worksheet.merge_range(0, 0, 0, 3, 'HARDWARE SUMMARY TABLE',format1)
        worksheet.write(1,0,'Model',format2)
        worksheet.write(1,1,'Total',format2)
        worksheet.write(1,2,'Percentage',format2)
        worksheet.write(1,3,'End of Support',format2)
        #header for chart
        worksheet.merge_range(0, 5, 0, 6, 'For Chart',format1)
        worksheet.write(1,5,'Model',format4)
        worksheet.write(1,6,'Percentage',format4)

        #data section
        count_row = 2
        for row in records:
            worksheet.write(count_row,0,row[0],format4)
            worksheet.write(count_row,1,str(row[2]),format4)
            percentage = (str((row[2]/total)*100))
            percentage = re.findall('\d+[.]\d',percentage)
            percentage = percentage[0]
            worksheet.write(count_row,2,(percentage+'%'),format4)
            worksheet.write(count_row,3,row[1],format4)
            worksheet.write(count_row,5,row[0],format4)
            try:
                worksheet.write(count_row,6,float(percentage),format4)
            except:
                worksheet.write(count_row,6,"-",format4)
            count_row += 1

        #chart section
        hardware_chart = workbook.add_chart({'type': 'pie'})
        hardware_chart.add_series({
            "categories": ["HARDWARE SUMMARY TABLE", 2, 5, count_row-1, 5],
            "values":     ["HARDWARE SUMMARY TABLE", 2, 6, count_row-1, 6],
            "data_labels": {"value": True},
        })
        worksheet.insert_chart('I2', hardware_chart)

        #HARDWARE CARD TABLE
        #sql query
        cursor.execute('''SELECT devicename, model, card, sn, hwdscr, eosl_date FROM hwcardtable''')
        records = cursor.fetchall()
        #create sheet
        worksheet = workbook.add_worksheet('HARDWARE CARD TABLE')
        
        #header section
        worksheet.merge_range(0, 0, 0, 5, 'HARDWARE CARD TABLE',format1)
        worksheet.write(1,0,'Device Name',format2)
        worksheet.write(1,1,'Model',format2)
        worksheet.write(1,2,'Card',format2)
        worksheet.write(1,3,'Description',format2)
        worksheet.write(1,4,'Serial Number',format2)
        worksheet.write(1,5,'End of Support',format2)

        #data section
        iteration_check = False
        row_check = 'ludesdeveloper'
        count_row = 2
        for row in records:
            if row_check not in row[0]:
                row_check = row[0]
                iteration_check = False
                start_row = count_row
            else:
                pass
            if row_check == row[0] and iteration_check == False:
                worksheet.write(count_row,0,row[0],format4)
                worksheet.write(count_row,1,row[1],format4)
                worksheet.write(count_row,2,row[2],format4)
                worksheet.write(count_row,3,row[4],format4)
                worksheet.write(count_row,4,row[3],format4)
                worksheet.write(count_row,5,row[5],format4)
                iteration_check = True
            else:
                worksheet.write(count_row,0,'',format4)
                worksheet.write(count_row,1,row[1],format4)
                worksheet.write(count_row,2,row[2],format4)
                worksheet.write(count_row,3,row[4],format4)
                worksheet.write(count_row,4,row[3],format4)
                worksheet.write(count_row,5,row[5],format4)
                iteration_check = True
                end_row = count_row
                worksheet.merge_range(start_row, 0, end_row, 0, row[0], format4)
            count_row+=1

        #CPU SUMMARY TABLE
        #sql query
        cursor.execute('''SELECT devicename, model, total, process, interrupt, topcpu, status FROM cpusumtable''')
        records = cursor.fetchall()
        cursor.execute('''SELECT Node, [Average CPU Load], [Peak CPU Load] from cpucoke''')
        cpusum = cursor.fetchall()
        #create sheet
        worksheet = workbook.add_worksheet('CPU SUMMARY TABLE')
        
        #header section
        worksheet.merge_range(0, 0, 0, 5, 'CPU SUMMARY TABLE',format1)
        worksheet.write(1,0,'Device Name',format2)
        worksheet.write(1,1,'Model',format2)
        worksheet.write(1,2,'Average CPU',format2)
        worksheet.write(1,3,'Peak CPU',format2)
        worksheet.write(1,4,'Top Process',format2)
        worksheet.write(1,5,'Status',format2)
        
        #header for chart
        worksheet.merge_range(0, 7, 0, 8, 'For Chart',format1)
        worksheet.write(1,7,'Device Name',format4)
        worksheet.write(1,8,'Average CPU',format4)

        #data section
        count_row = 2
        for row in records:
            worksheet.write(count_row,0,row[0],format4)
            worksheet.write(count_row,1,row[1],format4)
            for row2 in cpusum:
                if row[0] in row2[0]:
                    worksheet.write(count_row,2,(row2[1]),format4)
                    worksheet.write(count_row,3,(row2[2]),format4)
                    x = row2[1].split(" ")
                    try:
                        worksheet.write(count_row,8,float(x[0]),format4)
                    except:
                        worksheet.write(count_row,8,float(0.0),format4)
                    
                    if float(x[0]) < 21:
                        worksheet.write(count_row,5,"Low",format4)
                        
                    elif float(x[0]) < 81:
                        worksheet.write(count_row,5,"Medium",format4)
                        
                    elif float(x[0]) > 81:
                        worksheet.write(count_row,5,"High",format4)
                    break
                elif row2[0] == 'Error':
                    worksheet.write(count_row,2,"Please upload file Report_CPU_Load.xlsx",format4)
                    worksheet.write(count_row,3,"Please upload file Report_CPU_Load.xlsx",format4)
                    worksheet.write(count_row,5,"Please upload file Report_CPU_Load.xlsx",format4)
                else:
                    worksheet.write(count_row,2,"Hostname Not Found in Report_CPU_Load.xlsx",format4)
                    worksheet.write(count_row,3,"Hostname Not Found in Report_CPU_Load.xlsx",format4)
                    worksheet.write(count_row,5,"Hostname Not Found in Report_CPU_Load.xlsx",format4)
            worksheet.write(count_row,4,row[4],format4)
            
            
            worksheet.write(count_row,7,row[0],format4)
            
            count_row += 1

        #chart section
        cpu_chart = workbook.add_chart({'type': 'column'})
        for i in range(3,count_row+1):
            cpu_chart.add_series(
                {
                    "name" : "='CPU SUMMARY TABLE'!H"+str(i),
                    "values": "='CPU SUMMARY TABLE'!I"+str(i),
                    "data_labels": {"value": True},
                    }
                )
        worksheet.insert_chart('K2', cpu_chart)

        #MEMORY SUMMARY TABLE
        #sql query
        cursor.execute('''SELECT devicename, model, utils, topproc, status FROM memsumtable''')
        records = cursor.fetchall()
        #create sheet
        worksheet = workbook.add_worksheet('MEMORY SUMMARY TABLE')
        
        #header section
        worksheet.merge_range(0, 0, 0, 4, 'MEMORY SUMMARY TABLE',format1)
        worksheet.write(1,0,'Device Name',format2)
        worksheet.write(1,1,'Model',format2)
        worksheet.write(1,2,'Processor Memory Utilization',format2)
        worksheet.write(1,3,'Top Process',format2)
        worksheet.write(1,4,'Status',format2)
        #header for chart
        worksheet.merge_range(0, 6, 0, 7, 'For Chart',format1)
        worksheet.write(1,6,'Device Name',format4)
        worksheet.write(1,7,'Processor Memory Utilization',format4)

        #data section
        count_row = 2
        for row in records:
            worksheet.write(count_row,0,row[0],format4)
            worksheet.write(count_row,1,row[1],format4)
            worksheet.write(count_row,2,(row[2]+"%"),format4)
            worksheet.write(count_row,3,row[3],format4)
            worksheet.write(count_row,4,row[4],format4)
            worksheet.write(count_row,6,row[0],format4)
            #solving if data is not float issue
            try:
                worksheet.write(count_row,7,float(row[2]),format4)
            except:
                worksheet.write(count_row,7,"-",format4)
            count_row += 1

        #chart section
        memory_chart = workbook.add_chart({'type': 'column'})
        for i in range(3,count_row+1):
            memory_chart.add_series(
                {
                    "name" : "='MEMORY SUMMARY TABLE'!G"+str(i),
                    "values": "='MEMORY SUMMARY TABLE'!H"+str(i),
                    "data_labels": {"value": True},
                    }
                )
        worksheet.insert_chart('J2', memory_chart)

        #HARDWARE CONDITION ANALYSIS
        #sql query
        cursor.execute('''SELECT * FROM envtable''')
        records = cursor.fetchall()
        #create sheet
        worksheet = workbook.add_worksheet('HARDWARE CONDITION ANALYSIS')
        
        #header section
        worksheet.merge_range(0, 0, 0, 3, 'HARDWARE CONDITION ANALYSIS',format1)
        worksheet.write(1,0,'Device Name',format2)
        worksheet.write(1,1,'System',format2)
        worksheet.write(1,2,'Item',format2)
        worksheet.write(1,3,'Status',format2)

        #data section
        iteration_check = False
        row_check = 'ludesdeveloper'
        count_row = 2
        for row in records:
            if row_check not in row[1]:
                row_check = row[1]
                iteration_check = False
                start_row = count_row
            else:
                pass
            if row_check == row[1] and iteration_check == False:
                worksheet.write(count_row,0,row[1],format4)
                worksheet.write(count_row,1,row[2],format4)
                worksheet.write(count_row,2,row[3],format4)
                worksheet.write(count_row,3,row[4],format4)
                iteration_check = True
            else:
                worksheet.write(count_row,0,'',format4)
                worksheet.write(count_row,1,row[2],format4)
                worksheet.write(count_row,2,row[3],format4)
                worksheet.write(count_row,3,row[4],format4)
                iteration_check = True
                end_row = count_row
                worksheet.merge_range(start_row, 0, end_row, 0, row[1], format4)
            count_row+=1

        #CLOSE WORKBOOK
        workbook.close()

        #close database
        db.close()
        
        #save document
        print('')
        print('Saving Document')
        print('Document has been saved to routing_switching.xlsx')