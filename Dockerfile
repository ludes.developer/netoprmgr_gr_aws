FROM python:3.8
ENV TZ="Asia/Jakarta"
ADD requirements.txt /
RUN pip install -r requirements.txt
RUN apt-get update
RUN apt-get install poppler-utils -y
ADD . /usr/local/lib/python3.8/site-packages/netoprmgr
WORKDIR /usr/local/lib/python3.8/site-packages/netoprmgr
CMD ["python", "main.py"]
